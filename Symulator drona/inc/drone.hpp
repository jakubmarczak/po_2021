#ifndef DRONE_HPP
#define DRONE_HPP

#include <array>
#include <string>
#include <vector>

#include "Dr3D_gnuplot_api.hpp"
#include "coordinateSystem.hpp"
#include "cuboid.hpp"
#include "hexagonalPrism.hpp"

/**
  @brief Klasa modelujaca pojecie drona zlozonego z korpusu i czterech wirnikow.
*/
class drone : public coordinateSystem {
  uint id;                                  // ID drona.
  std::string color;                        // Kolor drona.
  cuboid body;                              // Prostopadloscian reprezentujacy korpus drona.
  std::array<hexagonalPrism, 4> propellers; // Array przechowujacy graniastoslupy prawidlowe szesciokatne reprezentujace wirniki drona.

public:
  /**
		@brief Konstruktor inicjalizujacy uklad wspolrzednych, poszczegolne wymiary i kolor drona.
		@param centroid Wektor trojwymiarowy reprezentujacy srodek drona (srodek korpusu).
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje drona.
		@param width Szerokosc korpusu drona (w osi X).
		@param height Wysokosc korpusu drona (w osi Y).
		@param length Dlugosc korpusu drona (w osi Z)
    @param radius Promien okregu wpisanego w graniastoslup prawidlowy szesciokatny reprezentujacy wirnik drona.
    @param color Kolor drona.
	*/
  drone(vectorMD<3> centeroid, rotationMatrixMD<3> orientation, double width, double height, double length, double radius, std::string color = "black");

  /**
    @brief Metoda realizujaca przemieszczenie drona w kierunku poziomym (dla zerowej orientacji w osi Z).
    @param distance Dystans, o jaki zostanie przemieszczony dron.
  */
  void move_horizontally(const double &distance);

  /**
    @brief Metoda realizujaca przemieszczenie drona w kierunku pionowym (w osi Y).
    @param height Wysokosc, o jaka zostanie przemieszczony dron.
  */
  void move_vertically(const double &height);

  /**
    @brief Metoda realizujaca rotacje drona wokol osi Y.
    @param angle Kat, o jaki zostanie obrocony dron.
  */
  void spin(const double &angle);

  /**
    @brief Metoda realizujaca rotacje wirnikow drona wokol osi Y.
    @param angle Kat, o jaki zostana obrocone wirniki.
  */
  void spin_propellers(const double &angle);

  /**
    @brief Metoda realizujaca wykonanie przemieszczen i rotacji drona wraz z ich wizualizacja.
    @param distance Dystans, o jaki zostanie przemieszczony dron.
    @param height Wysokosc, o jaka zostanie przemieszczony dron.
    @param angle Kat, o jaki zostanie obrocony dron.
    @param speed Wyrazona w procentach predkosc, z jaka bedzie sie poruszal dron.
  */
  void animate(std::shared_ptr<drawNS::Draw3DAPI> api, const double &distance, const double &height, const double &angle, uint speed);

  /**
    @brief Metoda realizujaca beczke i jej wizualizacje.
    @param distance Dystans, o jaki zostanie przemieszczony dron.
    @param height Wysokosc, o jaka zostanie przemieszczony dron.
    @param speed Wyrazona w procentach predkosc, z jaka dron zrobi beczke.
  */
  void doABarellRoll(std::shared_ptr<drawNS::Draw3DAPI> api, const double &distance, const double &height, uint speed);

  /**
    @brief Metoda realizujaca rysowanie drona w programie gnuplot za pomoca dostarczonego api.
    @param api wskaznik na obslugiwane api do gnuplota.
	*/
  std::vector<uint> draw(std::shared_ptr<drawNS::Draw3DAPI> api);
};

#endif

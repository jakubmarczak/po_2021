#ifndef VECTORMD_HPP
#define VECTORMD_HPP

#include <cmath>
#include <array>

/**
  @brief Szablon klasy modelujacej pojecie wektora wielowymiarowego (multidimensional vector).
*/
template <uint8_t dimension>
class vectorMD {
  /**
		@brief Array przechowujacy wspolrzedne wektora wielowymiarowego.
		@param dimension wymiar wektora.
	*/
  std::array<double, dimension> tab;

 public:
  /**
		@brief Konstruktor domyslny wektora wielowymiarowego.
	*/
  vectorMD() { tab.fill(0); }

  /**
		@brief Konstruktor przypisujacy wspolrzednym wektora wielowymiarowego wartosci przekazane jako argument.
		@param arg array z wartosciami kolejnych wspolrzednych.
	*/
  vectorMD(std::array<double, dimension> arg) : tab(arg) {}

  /**
		@brief Operator dostepowy dla wspolrzednych wektora wielowymiarowego.
		@param n numer wspolrzednej wektora (n>=0, 0=x, 1=y, ...).
		@return Modyfikowalna wartosc n-tej wspolrzednej wektora.
	*/
  double &operator[](uint8_t n);

  /**
		@brief Operator dostepowy dla wspolrzednych wektora wielowymiarowego.
		@param n numer wspolrzednej wektora (n>=0, 0=x, 1=y, ...).
		@return Niemodyfikowalna wartosc n-tej wspolrzednej wektora.
	*/
  const double &operator[](uint8_t n) const;

  /**
		@brief Operator dodawania wektorow wielowymiarowych.
		@param arg wektor wielowymiarowy, ktory chcemy dodac.
		@return Suma dwoch wektorow (nowy wektor).
	*/
  vectorMD<dimension> operator+(const vectorMD<dimension> &arg) const;

  /**
		@brief Operator odejmowania wektorow wielowymiarowych.
		@param arg wektor wielowymiarowy, ktory chcemy odjac.
		@return Roznica dwoch wektorow (nowy wektor).
	*/
  vectorMD<dimension> operator-(const vectorMD<dimension> &arg) const;

  /**
		@brief Operator iloczynu wektora wielowymiarowego i liczby rzeczywistej.
		@param arg liczba rzeczywista, przez ktora chcemy przemnozyc wektor.
		@return Przemnozony wektor.
	*/
  vectorMD<dimension> operator*(double arg) const;

  /**
		@brief Operator iloczynu skalarnego dwoch wektorow wielowymiarowych.
		@param arg wektor wielowymiarowy bedacy drugim skladnikiem iloczynu skalarnego.
		@return Liczba rzeczywista - wartosc iloczynu skalarnego.
	*/
  const double operator*(const vectorMD<dimension> &arg) const;

  /**
		@brief Metoda realizujaca obliczenie dlugosci wektora.
		@return Liczba rzeczywista - dlugosc wektora.
	*/
  const double length() const;

  /**
		@brief Metoda realizujaca wyznaczenie wektora znormalizowanego (wektora jednostkowego/wersora).
		@return Znormalizowany wektor.
	*/
  vectorMD<dimension> normalized() const;
};

/**
	@brief Operator przesuniecia bitowego w lewo (wypisanie) dla wspolrzednych wektora wielowymiarowego.
	@param stream obslugiwany strumien.
	@param arg wektor wielowymiarowy, ktorego wspolrzedne chcemy wypisac.
	@return Obslugiwany strumien.
*/
template <uint8_t dimension>
std::ostream &operator<<(std::ostream &stream, const vectorMD<dimension> &arg);

template <uint8_t dimension>
double &vectorMD<dimension>::operator[](uint8_t n) {
  if (n > dimension - 1)
    throw std::logic_error("proba dostepu do nieistniejacego elementu wektora/macierzy");
  return tab[n];
}

template <uint8_t dimension>
const double &vectorMD<dimension>::operator[](uint8_t n) const {
  if (n > dimension - 1)
    throw std::logic_error("proba dostepu do nieistniejacego elementu wektora/macierzy");
  return tab[n];
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::operator+(const vectorMD &arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result.tab[i] = tab[i] + arg.tab[i];
  return result;
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::operator-(const vectorMD &arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result.tab[i] = tab[i] - arg.tab[i];
  return result;
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::operator*(double arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result.tab[i] = tab[i] * arg;
  return result;
}

template <uint8_t dimension>
const double vectorMD<dimension>::operator*(const vectorMD<dimension> &arg) const {
  double result = 0;
  for (uint8_t i = 0; i < dimension; ++i)
    result += tab[i] * arg.tab[i];
  return result;
}

template <uint8_t dimension>
const double vectorMD<dimension>::length() const {
  double result = 0;
  for (uint8_t i = 0; i < dimension; ++i)
    result += std::pow(tab[i], 2);
  return std::sqrt(result);
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::normalized() const {
  return (*this) * (1 / (*this).length());
}

template <uint8_t dimension>
std::ostream &operator<<(std::ostream &stream, const vectorMD<dimension> &arg) {
  for (uint16_t i = 0; i < dimension; ++i)
    stream << "| [" << i + 1 << "] = " << arg[i] << std::endl;
  return stream;
}

#endif

#ifndef COORDINATESYSTEM_HPP
#define COORDINATESYSTEM_HPP

#include "Dr3D_gnuplot_api.hpp"
#include "rotationMatrixMD.hpp"

/**
  @brief Klasa modelujaca pojecie trojwymiarowego ukladu wspolrzednych.
*/
class coordinateSystem {
protected:
  coordinateSystem *parent;        // Wskaznik na poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
  vectorMD<3> centeroid;           // Wektor trojwymiarowy reprezentujacy srodek ukladu wspolrzednych.
  rotationMatrixMD<3> orientation; // Trojwymiarowa macierz rotacji reprezentujaca orientacje ukladu wspolrzednych.

public:
  /**
		@brief Konstruktor domyslny trojwymiarowego ukladu wspolrzednych.
	*/
  coordinateSystem() : parent(nullptr), centeroid(), orientation(){};

  /**
		@brief Konstruktor przypisujacy poprzedni uklad wspolrzednych oraz srodek i orientacje ukladu lokalnego.
		@param centroid Wektor trojwymiarowy reprezentujacy srodek ukladu wspolrzednych.
		@param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje ukladu wspolrzednych.
    @param parent Wskaznik na poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
	*/
  coordinateSystem(vectorMD<3> centeroid, rotationMatrixMD<3> orientation, coordinateSystem *parent = nullptr)
      : parent(parent), centeroid(centeroid), orientation(orientation) {}

  /**
		@brief Metoda realizujaca translacje trojwymiarowego ukladu wspolrzednych.
		@param translation Trojwymiarowy wektor translacji.
	*/
  void translate(const vectorMD<3> &translation);

  /**
		@brief Metoda realizujaca rotacje trojwymiarowego ukladu wspolrzednych.
		@param rotation Trojwymiarowa macierz rotacji.
	*/
  void rotate(const rotationMatrixMD<3> &rotation);

  /**
		@brief Metoda realizujaca przeliczenie lokalnego ukladu wspolrzednych do "rodzica".
		@return Przeliczony uklad wspolrzednych.
	*/
  coordinateSystem convertToParent();

  /**
		@brief Metoda realizujaca przeliczenie lokalnego ukladu wspolrzednych do globalnego.
    @return Przeliczony uklad wspolrzednych.
	*/
  coordinateSystem convertToGlobal();

  /**
		@brief Metoda realizujaca przeliczenie wektora trojwymiarowego z lokalnego ukladu wspolrzednych na Point3D w ukladzie globalnym.
    @return Point3D w globalnym ukladzie wspolrzednych o identycznych parametrach, co wektor trojwymiarowy przekazany w argumencie.
	*/
  drawNS::Point3D convertToGlobalPoint3D(const vectorMD<3> &arg);
};

#endif
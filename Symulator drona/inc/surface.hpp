#ifndef SURFACE_HPP
#define SURFACE_HPP

#include "Dr3D_gnuplot_api.hpp"

/**
  @brief Klasa modelujaca pojecie powierzchni o jednostkowej odleglosci pomiedzy jej punktami.
*/
class surface {
  uint id;       // ID powierzchni.
  double width;  // Szerokosc powierzchni.
  double height; // Wysokosc wzgledem poziomu zerowego.
  double length; // Dlugosc powierzchni.

public:
  /**
    @brief Konstruktor inicjalizujacy wymiary powierzchni.
    @param width Szerokosc powierzchni.
    @param height Wysokosc wzgledem poziomu zerowego.
    @param length Dlugosc powierzchni.
	*/
  surface(double width, double height, double length)
      : width(width), height(height), length(length) {}

  /**
    @brief Metoda realizujaca rysowanie powierzchni plaskiej w programie gnuplot za pomoca dostarczonego api.
    @param api wskaznik na obslugiwane api do gnuplota.
    @return ID powierzchni.
	*/
  uint draw(std::shared_ptr<drawNS::Draw3DAPI> api);
};

#endif
#ifndef CUBOID_HPP
#define CUBOID_HPP

#include <string>

#include "coordinateSystem.hpp"
#include "Dr3D_gnuplot_api.hpp"
#include "rotationMatrixMD.hpp"

/**
  @brief Klasa modelujaca pojecie prostopadloscianu.
*/
class cuboid : public coordinateSystem {
  uint id;           // ID prostopadloscianu.
  std::string color; // Kolor prostopadloscianu.
  double width;      // Szerokosc prostopadloscianu (w osi X).
  double height;     // Wysokosc prostopadloscianu (w osi Y).
  double length;     // Dlugosc prostopadloscianu (w osi Z).

public:
  /**
		@brief Konstruktor inicjalizujacy uklad wspolrzednych, poszczegolne wymiary i kolor prostopadloscianu.
		@param centroid Wektor trojwymiarowy reprezentujacy srodek prostopadloscianu.
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje prostopadloscianu.
    @param parent Wskaznik na poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
		@param width Szerokosc prostopadloscianu (w osi X).
		@param height Wysokosc prostopadloscianu (w osi Y).
		@param length Dlugosc prostopadloscianu (w osi Z)
    @param color Kolor prostopadloscianu.
	*/
  cuboid(vectorMD<3> centroid, rotationMatrixMD<3> orientation, coordinateSystem *parent, double width, double height, double length, std::string color="black")
      : coordinateSystem(centroid, orientation, parent), color(color), width(width), height(height), length(length){};

  /**
    @brief Metoda realizujaca rysowanie prostopadloscianu w programie gnuplot za pomoca dostarczonego api.
    @param api wskaznik na obslugiwane api do gnuplota.
    @return ID prostopadloscianu.
	*/
  uint draw(std::shared_ptr<drawNS::Draw3DAPI> api);
};

#endif

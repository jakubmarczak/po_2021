#ifndef SCENE_HPP
#define SCENE_HPP

#include "drawingInterface.hpp"
#include "droneInterface.hpp"
#include "landmarkInterface.hpp"

/**
  @brief Klasa modelujaca pojecie sceny w programie gnuplot.
*/
class scene {

protected:
  std::vector<droneInterface *> drones;       // Kolekcja dronow.
  std::vector<landmarkInterface *> landmarks; // Kolekcja elementow krajobrazu.
  std::vector<drawingInterface *> drawables;  // Kolekcja elementow rysowalnych.
  droneInterface *selected_drone;

public:
  scene() = default;
  void add_drone(droneInterface *drone) { drones.push_back(drone); };
  void add_landmark(landmarkInterface *landmark) { landmarks.push_back(landmark); };
  void remove_drone(const uint &id) { drones.erase(drones.begin() + id); }
  void remove_landmark(const uint &id) { landmarks.erase(landmarks.begin() + id ); }
  void remove_all();
  void highlight();
  void select_drone(const uint8_t &n);
  void check_collision(droneInterface *drone);


  /**
    @brief Metoda realizujaca animacje (wizualizacje) przemieszczen i rotacji drona z zadana predkoscia.
    @param horizontal_distance Dystans, o jaki zostanie przemieszczony animowany dron w kierunku poziomym/horyzontalnym.
    @param height Dystans, o jaki zostanie przemieszczony animowany dron w kierunku pionowym/wertykalnym.
    @param angle Kat, o jaki zostanie obrocony animowany dron.
    @param speed Wyrazona w procentach predkosc, z jaka bedzie sie poruszal animowany dron.
  */
  void animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, int speed);
};

#endif

#ifndef HILL_HPP
#define HILL_HPP

#include "coordinateSystem.hpp"
#include "drawingInterface.hpp"
#include "landmarkInterface.hpp"

/**
  @brief Klasa modelujaca pojecie wzgorza.
*/
class hill : public coordinateSystem, public drawingInterface, public landmarkInterface{
  
protected:
  std::vector<vectorMD<3>> vertices; // Wierzcholki podstawy wzgorza.
  double min; // Minimalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
  double max; // Maksymalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
  double height; // Wysokosc wzgledem poziomu zerowego (w osi Z).

public:
  /**
    @brief Konstruktor inicjalizujacy podstawowe parametry losowo wygenerowanego wzgorza.
    @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
    @param centroid Wektor trojwymiarowy reprezentujacy srodek wzgorza.
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje wzgorza.
    @param min Minimalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
    @param max Maksymalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
    @param height Wysokosc wzgledem poziomu zerowego (w osi Z).
    @param color Kolor wzgorza.
	*/
  hill(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double min,
      double max,
      double height,
      std::string color = "black");

  /**
    @brief Metoda realizujaca rysowanie powierzchni plaskiej w programie gnuplot za pomoca dostarczonego api.
	*/
  void draw() override;
  
  /**
    @brief Metoda realizujaca zmazywanie powierzchni plaskiej w programie gnuplot za pomoca dostarczonego api.
	*/
  void erase() override { api->erase_shape(id); }

  /**
    @brief Metoda realizujaca zmiane skali wzgorza.
    @param scale Skala, z jaka zostana powiekszone (scale>1) / pomniejszone (scale<1) poszczegolne wymiary wzgorza.
  */
  void rescale(const double &scale) override {}

  /**
    @brief Metoda sprawdzajaca, czy dron znajduje sie nad wzgorzem.
    @param drone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron znajduje lub nie znajduje sie nad wzgorzem.
  */
  bool is_above(droneInterface *drone) override;

  /**
    @brief Metoda sprawdzajaca, czy dron moze wyladowac/opasc na dana wysokosc wzgledem poziomu zerowego.
    @param drone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @param altitude Wysokosc, dla ktorej zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron moze lub nie moze opasc na dana wysokosc.
  */
  bool can_land(droneInterface *drone, const double &altitude) override;
};

#endif
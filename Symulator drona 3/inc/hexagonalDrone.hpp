#ifndef HEXAGONALDRONE_HPP
#define HEXAGONALDRONE_HPP

#include "coordinateSystem.hpp"
#include "cuboid.hpp"
#include "drawingInterface.hpp"
#include "droneInterface.hpp"
#include "hexagonalPrism.hpp"
#include "landmarkInterface.hpp"

/**
  @brief Klasa modelujaca pojecie drona szesciokatnego - zlozonego z korpusu i szesciu wirnikow w postaci graniastoslupow prawidlowych szesciokatnych.
*/
class hexagonalDrone : public coordinateSystem, public drawingInterface, public droneInterface, public landmarkInterface {
protected:
  hexagonalPrism body;                      // Graniastoslup prawidlowy szesciokatny reprezentujacy korpus drona.
  std::array<hexagonalPrism, 6> propellers; // Array przechowujacy graniastoslupy prawidlowe szesciokatne reprezentujace wirniki drona.

public:
  /**
		@brief Konstruktor inicjalizujacy interfejs rysowania, uklad wspolrzednych i poszczegolne wymiary drona.
    @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
		@param centroid Wektor trojwymiarowy reprezentujacy srodek drona (srodek korpusu).
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje drona.
    @param height Wysokosc korpusu drona (w osi Z).
    @param body_radius Promien okregu wpisanego w graniastoslup prawidlowy szesciokatny reprezentujacy korpus drona.
    @param propeller_radius Promien okregu wpisanego w graniastoslup prawidlowy szesciokatny reprezentujacy wirnik drona.
    @param color Kolor drona.
	*/
  hexagonalDrone(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centeroid,
      rotationMatrixMD<3> orientation,
      double height,
      double body_radius,
      double propeller_radius,
      std::string color = "black");

  /**
    @brief Metoda realizujaca przemieszczenie drona w kierunku poziomym/horyzontalnym (domyslnie w osi Y).
    @param distance Dystans, o jaki zostanie przemieszczony dron.
  */
  void move_horizontally(const double &distance) override { translate(orientation * vectorMD<3>({0, distance, 0})); }

  /**
    @brief Metoda realizujaca przemieszczenie drona w kierunku pionowym/wertykalnym (domyslnie w osi Z).
    @param distance Dystans, o jaki zostanie przemieszczony dron.
  */
  void move_vertically(const double &distance) override { translate(orientation * vectorMD<3>({0, 0, distance})); }

  /**
    @brief Metoda realizujaca pochylenie drona (rotacje wokol osi X).
    @param angle Kat, o jaki zostanie pochylony dron.
  */
  void tilt(const double &angle) override { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({1, 0, 0}))); }

  /**
    @brief Metoda realizujaca rotacje drona wokol osi Z.
    @param angle Kat, o jaki zostanie obrocony dron.
  */
  void spin(const double &angle) override { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({0, 0, 1}))); }

  /**
    @brief Metoda realizujaca rotacje wirnikow drona wokol osi Z.
    @param angle Kat, o jaki zostana obrocone wirniki.
  */
  void spin_propellers(const double &angle) override;

  /**
    @brief Metoda realizujaca animacje (wizualizacje) przemieszczen i rotacji drona z zadana predkoscia.
    @param horizontal_distance Dystans, o jaki zostanie przemieszczony animowany dron w kierunku poziomym/horyzontalnym.
    @param height Dystans, o jaki zostanie przemieszczony animowany dron w kierunku pionowym/wertykalnym.
    @param angle Kat, o jaki zostanie obrocony animowany dron.
    @param speed Wyrazona w procentach predkosc, z jaka bedzie sie poruszal animowany dron.
  */
  void animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, int speed) override;

  /**
    @brief Metoda realizujaca rysowanie drona w programie gnuplot za pomoca dostarczonego api.
	*/
  void draw() override;

  /**
    @brief Metoda realizujaca zmazywanie drona w programie gnuplot za pomoca dostarczonego api.
	*/
  void erase() override;

  /**
    @brief Metoda realizujaca zmiane skali drona.
    @param scale Skala, z jaka zostanie powiekszony (scale>1) / pomniejszony (scale<1) dron.
  */
  void rescale(const double &scale) override;

  /**
    @brief Metoda sprawdzajaca, czy dron znajduje sie nad innym dronem.
    @param hexagonalDrone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron znajduje lub nie znajduje sie nad innym dronem.
  */
  bool is_above(droneInterface *hexagonalDrone) override;

  /**
    @brief Metoda sprawdzajaca, czy dron moze wyladowac/opasc na dana wysokosc wzgledem poziomu zerowego.
    @param hexagonalDrone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @param altitude Wysokosc, dla ktorej zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron moze lub nie moze opasc na dana wysokosc.
  */
  bool can_land(droneInterface *hexagonalDrone, const double &altitude) override;
};

#endif

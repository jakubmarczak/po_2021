#ifndef LANDMARKINTERFACE_HPP
#define LANDMARKINTERFACE_HPP

#include "droneInterface.hpp"

/**
  @brief Klasa modelujaca interfejs elementu krajobrazu.
*/
class landmarkInterface {

public:
  /**
    @brief Metoda sprawdzajaca, czy dron znajduje sie nad elementem krajobrazu.
    @param drone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron znajduje lub nie znajduje sie nad elementem krajobrazu.
  */
  virtual bool is_above(droneInterface *drone) = 0;

  /**
    @brief Metoda sprawdzajaca, czy dron moze wyladowac/opasc na dana wysokosc wzgledem poziomu zerowego.
    @param drone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @param altitude Wysokosc, dla ktorej zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron moze lub nie moze opasc na dana wysokosc.
  */
  virtual bool can_land(droneInterface *drone, const double &altitude) = 0;
};

#endif

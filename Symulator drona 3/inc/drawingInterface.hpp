#ifndef DRAWINGINTERFACE_HPP
#define DRAWINGINTERFACE_HPP

#include "Dr3D_gnuplot_api.hpp"

/**
  @brief Klasa modelujaca interfejs rysowania w programie gnuplot za pomoca dostarczonego api.
*/
class drawingInterface {

protected:
  std::shared_ptr<drawNS::Draw3DAPI> api; // Wskaznik wspoldzielony na obslugiwane api do gnuplota.
  uint id;                                // ID rysowanego obiektu.
  std::string color;                      // Kolor rysowanego obiektu.

public:
  /**
    @brief Konstruktor inicjalizujacy wskaznik na api do gnuplota i kolor rysowanego obiektu.
  */
  drawingInterface(std::shared_ptr<drawNS::Draw3DAPI> api, std::string color) : api(api), color(color) {}

  /**
    @brief Metoda wirtualna realizujaca rysowanie obiektu.
  */
  virtual void draw() = 0;

  /**
    @brief Metoda wirtualna realizujaca zmazywanie obiektu.
  */
  virtual void erase() = 0;

  /**
    @brief Metoda realizujaca zmiane skali obiektu.
    @param scale Skala, z jaka zostana powiekszony (scale>1) / pomniejszony (scale<1) dany obiekt.
  */
  virtual void rescale(const double &scale) = 0;
};

#endif
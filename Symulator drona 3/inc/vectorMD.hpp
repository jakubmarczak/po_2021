#ifndef VECTORMD_HPP
#define VECTORMD_HPP

#include <array>
#include <cmath>

/**
  @brief Szablon klasy modelujacej pojecie wektora wielowymiarowego (multidimensional vector).
*/
template <uint8_t dimension>
class vectorMD {
  /**
		@brief Array przechowujacy wspolrzedne wektora wielowymiarowego.
		@param dimension wymiar wektora.
	*/
  std::array<double, dimension> tab;
  inline static uint _created = 0;  // Ilosc stworzonych wektorow wielowymiarowych.
  inline static uint _existing = 0; // Ilosc istniejacych wektorow wielowymiarowych.

public:
  /**
		@brief Konstruktor domyslny wektora wielowymiarowego.
	*/
  vectorMD() { tab.fill(0); _created++; _existing++; }

  /**
		@brief Konstruktor kopiujacy wektora wielowymiarowego.
		@param arg array z wartosciami kolejnych wspolrzednych.
	*/
  vectorMD(const vectorMD<dimension> &arg) { tab = arg.tab; _created++; _existing++; }

  /**
		@brief Konstruktor przypisujacy wspolrzednym wektora wielowymiarowego wartosci przekazane jako argument.
		@param arg array z wartosciami kolejnych wspolrzednych.
	*/
  vectorMD(const std::array<double, dimension> &arg) : tab(arg) { _created++; _existing++; }
  
  /**
		@brief Destruktor wektora wielowymiarowego.
	*/
  ~vectorMD() { _existing--; }

  /**
    @brief Metoda dostepowa dla ilosci stworzonych wektorow wielowymiarowych.
    @return Ilosc stworzonych wektorow wielowymiarowych.
  */
  static uint created() { return _created; }

  /**
    @brief Metoda dostepowa dla ilosci istniejacych wektorow wielowymiarowych.
    @return Ilosc istniejacych wektorow wielowymiarowych.
  */
  static uint existing() { return _existing; }

  /**
		@brief Operator dostepowy dla wspolrzednych wektora wielowymiarowego.
		@param n numer wspolrzednej wektora (n>=0, 0=x, 1=y, ...).
		@return Modyfikowalna wartosc n-tej wspolrzednej wektora.
	*/
  double &operator[](const uint8_t &n);

  /**
		@brief Operator dostepowy dla wspolrzednych wektora wielowymiarowego.
		@param n numer wspolrzednej wektora (n>=0, 0=x, 1=y, ...).
		@return Niemodyfikowalna wartosc n-tej wspolrzednej wektora.
	*/
  const double &operator[](const uint8_t &n) const;

  /**
		@brief Operator dodawania wektorow wielowymiarowych.
		@param arg wektor wielowymiarowy, ktory chcemy dodac.
		@return Suma dwoch wektorow (nowy wektor).
	*/
  vectorMD<dimension> operator+(const vectorMD<dimension> &arg) const;

  /**
		@brief Operator odejmowania wektorow wielowymiarowych.
		@param arg wektor wielowymiarowy, ktory chcemy odjac.
		@return Roznica dwoch wektorow (nowy wektor).
	*/
  vectorMD<dimension> operator-(const vectorMD<dimension> &arg) const;

  /**
		@brief Operator iloczynu wektora wielowymiarowego i liczby rzeczywistej.
		@param arg liczba rzeczywista, przez ktora chcemy przemnozyc wektor.
		@return Przemnozony wektor.
	*/
  vectorMD<dimension> operator*(double arg) const;

  /**
		@brief Operator iloczynu skalarnego dwoch wektorow wielowymiarowych.
		@param arg wektor wielowymiarowy bedacy drugim skladnikiem iloczynu skalarnego.
		@return Liczba rzeczywista - wartosc iloczynu skalarnego.
	*/
  const double operator*(const vectorMD<dimension> &arg) const;

  /**
		@brief Metoda realizujaca obliczenie dlugosci wektora.
		@return Liczba rzeczywista - dlugosc wektora.
	*/
  const double length() const;

  /**
		@brief Metoda realizujaca wyznaczenie wektora znormalizowanego (wektora jednostkowego/wersora).
		@return Znormalizowany wektor.
	*/
  vectorMD<dimension> normalized() const;
};

/**
	@brief Operator przesuniecia bitowego w lewo (wypisanie) dla wspolrzednych wektora wielowymiarowego.
	@param stream obslugiwany strumien.
	@param arg wektor wielowymiarowy, ktorego wspolrzedne chcemy wypisac.
	@return Obslugiwany strumien.
*/
template <uint8_t dimension>
std::ostream &operator<<(std::ostream &stream, const vectorMD<dimension> &arg);

template <uint8_t dimension>
double &vectorMD<dimension>::operator[](const uint8_t &n) {
  if (n > dimension - 1)
    throw std::logic_error("proba dostepu do nieistniejacego elementu wektora/macierzy");
  return tab[n];
}

template <uint8_t dimension>
const double &vectorMD<dimension>::operator[](const uint8_t &n) const {
  if (n > dimension - 1)
    throw std::logic_error("proba dostepu do nieistniejacego elementu wektora/macierzy");
  return tab[n];
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::operator+(const vectorMD &arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result.tab[i] = tab[i] + arg.tab[i];
  return result;
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::operator-(const vectorMD &arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result.tab[i] = tab[i] - arg.tab[i];
  return result;
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::operator*(double arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result.tab[i] = tab[i] * arg;
  return result;
}

template <uint8_t dimension>
const double vectorMD<dimension>::operator*(const vectorMD<dimension> &arg) const {
  double result = 0;
  for (uint8_t i = 0; i < dimension; ++i)
    result += tab[i] * arg.tab[i];
  return result;
}

template <uint8_t dimension>
const double vectorMD<dimension>::length() const {
  double result = 0;
  for (uint8_t i = 0; i < dimension; ++i)
    result += std::pow(tab[i], 2);
  return std::sqrt(result);
}

template <uint8_t dimension>
vectorMD<dimension> vectorMD<dimension>::normalized() const {
  return (*this) * (1 / (*this).length());
}

template <uint8_t dimension>
std::ostream &operator<<(std::ostream &stream, const vectorMD<dimension> &arg) {
  for (uint16_t i = 0; i < dimension; ++i)
    stream << "| [" << i + 1 << "] = " << arg[i] << std::endl;
  return stream;
}

#endif

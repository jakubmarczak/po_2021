#ifndef HEXAGONALPRISM_HPP
#define HEXAGONALPRISM_HPP

#include "coordinateSystem.hpp"
#include "drawingInterface.hpp"

/**
  @brief Klasa modelujaca pojecie graniastoslupa prawidlowego szesciokatnego.
*/
class hexagonalPrism : public coordinateSystem, public drawingInterface {
protected:
  double radius; // Promien okregu opisanego na podstawie graniastoslupa - szesciokacie foremnym.
  double height; // Wysokosc graniastoslupa (w osi Z).

public:
  /**
    @brief Konstruktor inicjalizujacy interfejs rysowania, uklad wspolrzednych i poszczegolne wymiary graniastoslupa prawidlowego szesciokatnego.
    @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
    @param centroid Wektor trojwymiarowy reprezentujacy srodek graniastoslupa.
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje graniastoslupa.
    @param parent Poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
    @param radius Promien okregu opisanego na podstawie graniastoslupa - szesciokacie foremnym.
    @param height Wysokosc graniastoslupa (w osi Z).
	*/
  hexagonalPrism(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      coordinateSystem *parent,
      double radius,
      double height,
      std::string color = "black")
      : coordinateSystem(centroid, orientation, parent),
        drawingInterface(api, color),
        radius(radius), height(height){};

  /**
    @brief Metoda realizujaca rysowanie graniastoslupa prawidlowego szesciokatnego w programie gnuplot za pomoca dostarczonego api.
	*/
  void draw() override;

  /**
    @brief Metoda realizujaca zmazywanie graniastoslupa prawidlowego szesciokatnego w programie gnuplot za pomoca dostarczonego api.
	*/
  void erase() override { api->erase_shape(id); }

  /**
    @brief Metoda realizujaca zmiane skali graniastoslupa prawidlowego szesciokatnego.
    @param scale Skala, z jaka zostanie powiekszony (scale>1) / pomniejszony (scale<1) graniastoslup prawidlowy szesciokatny.
  */
  void rescale(const double &scale) override { centeroid = centeroid * scale; radius *= scale; height *= scale; }  
};

#endif

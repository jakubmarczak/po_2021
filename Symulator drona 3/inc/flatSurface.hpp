#ifndef FLATSURFACE_HPP
#define FLATSURFACE_HPP

#include "drawingInterface.hpp"

/**
  @brief Klasa modelujaca pojecie powierzchni plaskiej o jednostkowej odleglosci pomiedzy jej punktami.
*/
class flatSurface : public drawingInterface {
  
protected:
  double scale;  // Skala powierzchni plaskiej (odleglosc pomiedzy sasiednimi punktami)
  double width;  // Szerokosc powierzchni plaskiej (w osi X).
  double length; // Dlugosc powierzchni plaskiej (w osi Y).
  double height; // Wysokosc wzgledem poziomu zerowego (w osi Z).

public:
  /**
    @brief Konstruktor inicjalizujacy interfejs rysowania, poszczegolne wymiary i kolor powierzchni plaskiej.
    @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
    @param width Szerokosc powierzchni plaskiej (w osi X).
    @param length Dlugosc powierzchni plaskiej (w osi Y).
    @param height Wysokosc wzgledem poziomu zerowego (w osi Z).
    @param color Kolor powierzchni plaskiej.
	*/
  flatSurface(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      double scale,
      double width,
      double length,
      double height,
      std::string color = "black")
      : drawingInterface(api, color),
        scale(scale), width(width), length(length), height(height) { draw(); }

  /**
    @brief Metoda realizujaca rysowanie powierzchni plaskiej w programie gnuplot za pomoca dostarczonego api.
	*/
  void draw() override;

  /**
    @brief Metoda realizujaca zmazywanie powierzchni plaskiej w programie gnuplot za pomoca dostarczonego api.
	*/
  void erase() override { api->erase_shape(id); }

  /**
    @brief Metoda realizujaca zmiane skali (odleglosci pomiedzy sasiednimi punktami) powierzchni plaskiej.
    @param scale Skala, z jaka zostanie powiekszona (scale>1) / pomniejszona (scale<1) odleglosc pomiedzy sasiednimi punktami powierzchni plaskiej.
  */
  void rescale(const double &_scale) override { erase(); scale = _scale; draw(); }
};

#endif
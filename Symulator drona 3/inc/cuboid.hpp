#ifndef CUBOID_HPP
#define CUBOID_HPP

#include "coordinateSystem.hpp"
#include "drawingInterface.hpp"

/**
  @brief Klasa modelujaca pojecie prostopadloscianu.
*/
class cuboid : public coordinateSystem, public drawingInterface {

protected:
  double width;  // Szerokosc prostopadloscianu (w osi X).
  double length; // Dlugosc prostopadloscianu (w osi Y).
  double height; // Wysokosc prostopadloscianu (w osi Z).

public:
  /**
		@brief Konstruktor domyslny prostopadloscianu.
	*/
  cuboid();

  /**
		@brief Konstruktor inicjalizujacy interfejs rysowania, uklad wspolrzednych i poszczegolne wymiary prostopadloscianu.
    @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
		@param centroid Wektor trojwymiarowy reprezentujacy srodek prostopadloscianu.
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje prostopadloscianu.
    @param parent Wskaznik na poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
		@param width Szerokosc prostopadloscianu (w osi X).
    @param length Dlugosc prostopadloscianu (w osi Y).c
    @param height Wysokosc prostopadloscianu (w osi Z).
    @param color Kolor prostopadloscianu.
	*/
  cuboid(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      coordinateSystem *parent,
      double width,
      double length,
      double height,
      std::string color = "black")
      : coordinateSystem(centroid, orientation, parent),
        drawingInterface(api, color),
        width(width), length(length), height(height){};

  /**
    @brief Metoda realizujaca rysowanie prostopadloscianu w programie gnuplot za pomoca dostarczonego api.
	*/
  void draw() override;

  /**
    @brief Metoda realizujaca zmazywanie prostopadloscianu w programie gnuplot za pomoca dostarczonego api.
	*/
  void erase() override { api->erase_shape(id); };

  /**
    @brief Metoda realizujaca zmiane skali prostopadloscianu.
    @param scale Skala, z jaka zostanie powiekszony (scale>1) / pomniejszony (scale<1) prostopadloscian
  */
  void rescale(const double &scale) override {
    centeroid = centeroid * scale;
    width *= scale;
    length *= scale;
    height *= scale;
  }
};

#endif

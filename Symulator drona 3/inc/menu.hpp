#ifndef MENU_HPP
#define MENU_HPP

#include <ncurses.h>
#include <string>
#include <vector>

/**
  @brief Klasa modelujaca menu "okienkowe" programu.
*/
class menu {

  WINDOW *border;                      // Okno zawierajace ramke menu.
  WINDOW *content;                     // Okno zawierajace tresc uzyteczna menu.
  std::vector<std::string> options;    // Wektor zawierajacy pozycje menu.
  std::vector<std::string> borderText; // Wektor zawierajacy tekst ramki menu.
  attr_t attr;                         // Atrybut biblioteki ncurses, ktory zostanie przypisany zaznaczonej pozycji.
  uint16_t height;                     // Wysokosc terminala, na ktorym zostanie wyswietlone menu.
  uint16_t width;                      // Szerokosc terminala, na ktorym zostanie wyswietlone menu.
  uint8_t selected;                    // Numer pozycji wybranej przyciskiem ENTER.
  int keyPressed;                      // Zmienna przechowujaca kod ostatnio wcisnietego klawisza.

  /**
		@brief Metoda realizujaca inicjacje menu.
	*/
  void init();

  /**
    @brief Metoda realizujaca wypisanie pozycji menu i przypisanie podanego w argumencie atrybutu zaznaczonej pozycji.
    @param attr atrybut biblioteki ncurses, ktory zostanie przypisany zaznaczonej pozycji.
  */
  void printOptions(attr_t attr);

  /**
		@brief Metoda realizujaca zaznaczenie poprzedniej pozycji menu.
	*/
  void scrollUp();

  /**
		@brief Metoda realizujaca zaznaczenie nastepnej pozycji menu.
	*/
  void scrollDown();

public:
  /**
		@brief Konstruktor ustawiajacy wybrana pozycje na pierwsza i atrybut ncurses na przekazany oraz przekazujacy wektory z pozycjami i tekstem ramki menu.
    @param _options wektor zawierajacy pozycje menu.
    @param _borderText wektor zawierajacy tekst ramki menu.
	*/
  menu(std::vector<std::string> _options, std::vector<std::string> _borderText, attr_t _attr) : options{_options}, borderText{_borderText}, attr{_attr}, selected{1} {};

  /**
    @brief Metoda "przerywajaca" dzialanie programu do momentu wykrycia znaku nowej linii.
  */
  void pressEnter();

  /**
		@brief Metoda realizujaca pelna funkcjonalnosc menu.
    @return Numer pozycji wybranej przyciskiem ENTER.
	*/
  uint8_t run();
};
#endif

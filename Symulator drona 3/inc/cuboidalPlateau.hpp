#ifndef CUBOIDALPLATEAU_HPP
#define CUBOIDALPLATEAU_HPP

#include "cuboid.hpp"
#include "landmarkInterface.hpp"

/**
  @brief Klasa modelujaca pojecie plaskowyzu prostopadlosciennego.
*/
class cuboidalPlateau : public cuboid, public landmarkInterface {

protected:
  double min;     // Minimalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
  double max;     // Maksymalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
  double _height; // Wysokosc wzgledem poziomu zerowego (w osi Z).

public:
  /**
    @brief Konstruktor inicjalizujacy podstawowe parametry losowo wygenerowanego plaskowyzu prostopadlosciennego.
    @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
    @param centroid Wektor trojwymiarowy reprezentujacy srodek plaskowyzu prostopadlosciennego.
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje plaskowyzu prostopadlosciennego.
    @param min Minimalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
    @param max Maksymalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
    @param height Wysokosc wzgledem poziomu zerowego (w osi Z).
    @param color Kolor plaskowyzu prostopadlosciennego.
	*/
  cuboidalPlateau(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double min,
      double max,
      double height,
      std::string color = "black");

  /**
    @brief Metoda sprawdzajaca, czy dron znajduje sie nad plaskowyzem prostopadlosciennym.
    @param drone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron znajduje lub nie znajduje sie nad plaskowyzem prostopadlosciennym.
  */
  bool is_above(droneInterface *drone) override;

  /**
    @brief Metoda sprawdzajaca, czy dron moze wyladowac/opasc na dana wysokosc wzgledem poziomu zerowego.
    @param drone Interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
    @param altitude Wysokosc, dla ktorej zostanie wykonane sprawdzenie.
    @return Wartosci 1 lub 0 odpowiednio, gdy dron moze lub nie moze opasc na dana wysokosc.
  */
  bool can_land(droneInterface *drone, const double &altitude) override;
};

#endif
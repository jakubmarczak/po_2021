#include "cuboidalPlateau.hpp"
#include <random>

cuboidalPlateau::cuboidalPlateau(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double min,
    double max,
    double height,
    std::string color)
    : cuboid(api, centroid, orientation, nullptr, width, length, height, color),
      min(min), max(max), _height(height){
  // do randomizacji szerokosci i dlugosci prostopadloscianu
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_real_distribution<double> width_distribution(min, max);
  std::uniform_real_distribution<double> length_distribution(min, max);
  width = width_distribution(gen);
  length = length_distribution(gen);
  draw();
  api->redraw();
}

bool cuboidalPlateau::is_above(droneInterface *dronse) { 
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}

bool cuboidalPlateau::can_land(droneInterface *drone, const double &altitude) { 
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}

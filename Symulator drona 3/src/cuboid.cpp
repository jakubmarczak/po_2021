#include "cuboid.hpp"

void cuboid::draw() {
  
  std::vector<drawNS::Point3D> base1, base2; // podstawy

  // 1 podstawa
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, -0.5 * length, -0.5 * height})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, -0.5 * length, 0.5 * height})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, -0.5 * length, 0.5 * height})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, -0.5 * length, -0.5 * height})));

  // 2 podstawa
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, 0.5 * length, -0.5 * height})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, 0.5 * length, 0.5 * height})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, 0.5 * length, 0.5 * height})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, 0.5 * length, -0.5 * height})));

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base1, base2}, color);
}
#include <iostream>
#include <ncurses.h> // do menu
#include <string>
#include <vector>

#include "Dr3D_gnuplot_api.hpp"
#include "basicDrone.hpp"
#include "cuboidalPlateau.hpp"
#include "droneInterface.hpp"
#include "flatSurface.hpp"
#include "hexagonalDrone.hpp"
#include "hill.hpp"
#include "menu.hpp"
#include "plateau.hpp"
#include "scene.hpp"

using drawNS::APIGnuPlot3D;
using std::cin;
using std::cout;
using std::endl;

int main() {
  // menu
  std::vector<std::string> menu_options, menu_border;                         // vectory przechowujace: nazwy pozycji i tekst ramki menu
  attr_t menu_attr = A_REVERSE;                                               // atrybut ncurses przypisywany do zaznaczonej pozycji
  uint8_t menu_choice;                                                        // numer pozycji wybranej przyciskiem enter
  menu_options.push_back("Test animacji dronow");                             // pozycja 1
  menu_options.push_back("Test generacji elementow krajobrazu");              // pozycja 2
  menu_options.push_back("Zakoncz");                                          // pozycja 3
  menu_border.push_back(" Symulator drona ");                                 // tytul projektu
  menu_border.push_back(" Jakub Marczak 259319 ");                            // autor
  menu_border.push_back(" sterowanie: strzalki / klawisze W,A,S,D,Q,ENTER "); // sterowanie

  std::vector<droneInterface *> drones;       // kolekcja dronow
  std::vector<landmarkInterface *> landmarks; // kolekcja elementow krajobrazu

  try {
    // inicjalizacja menu
    menu menu(menu_options, menu_border, menu_attr);

    // scena X[-10,10] x Y[-10,10] x Z[0,10] odswiezana za pomoca metody redraw
    std::shared_ptr<drawNS::Draw3DAPI> api(new APIGnuPlot3D(-10, 10, -10, 10, 0, 10, -1));

    // scena z dronami i elementami krajobrazu
    scene scene; 

    // definicja powierzchni plaskiej - "podlogi"
    flatSurface floor(api, 4, 5, 5, 0);

    while (menu_choice != menu_options.size()) // ostatnia pozycja - zakoncz
    {
      menu_choice = menu.run();

      switch (menu_choice) {
        case 1: { // test animacji dronow       

          // definicje dronow (szczerze mowiac to nie wiem, jak to prosciej zapisac)
          hexagonalDrone drone1(api, vectorMD<3>({-3, 3, 0}), rotationMatrixMD<3>(), 0.7, 1, 0.4, "red");
          basicDrone drone2(api, vectorMD<3>({-3, -3, 0}), rotationMatrixMD<3>(), 1.5, 1.5, 1, 0.5, "orange");
          hexagonalDrone drone3(api, vectorMD<3>({3, 3, 0}), rotationMatrixMD<3>(), 0.8, 1.4, 0.5, "green");
          basicDrone drone4(api, vectorMD<3>({3, -3, 0}), rotationMatrixMD<3>(), 0.8, 3, 0.5, 0.3, "blue");
          drones.push_back(&drone1);
          drones.push_back(&drone2);
          drones.push_back(&drone3);
          drones.push_back(&drone4);

          // dodanie dronow do sceny
          for (droneInterface *i : drones)
            scene.add_drone(i);

          //animacja ruchow drona 1
          scene.select_drone(1);
          scene.animate(0, 1, 0, 85);
          scene.animate(-5, 0, 0, 85);
          scene.animate(0, 5, 180, 60);
          scene.animate(0, -5, 0, 60);
          scene.animate(-5, 0, 0, 85);
          scene.animate(0, -1, 0, 85);

          // animacja ruchow drona 2
          scene.select_drone(2);
          scene.animate(0, 5, 180, 70);
          scene.animate(0, -5, 180, 85);

          // animacja ruchow drona 3
          scene.select_drone(3);
          scene.animate(0, 5, 90, 80);
          scene.animate(0, 0, 90, 80);
          scene.animate(3, 0, 0, 80);
          scene.animate(0, 0, 180, 80);
          scene.animate(3, 0, 0, 80);
          scene.animate(0, -5, -360, 80);

          // animacja ruchow drona 4
          scene.select_drone(4);
          scene.animate(4, 4, 90, 80);
          scene.animate(4, -4, 90, 65);
          scene.animate(4, 4, 90, 80);
          scene.animate(4, -4, 90, 65);

          menu.pressEnter();
          break;
        }
        case 2: // test generacji elementow krajobrazu
        { 
          // definicje elementow krajobrazu
          plateau plateau(api, vectorMD<3>({-5, 5, 0}), rotationMatrixMD<3>(), 3, 6, 3, "black");
          hill hill(api, vectorMD<3>({-5, -5, 0}), rotationMatrixMD<3>(), 3, 6, 4, "black");
          cuboidalPlateau cuboidalPlateau(api, vectorMD<3>({5, 0, 1}), rotationMatrixMD<3>(), 2, 4, 2, "black");
          landmarks.push_back(&plateau);
          landmarks.push_back(&hill);
          landmarks.push_back(&cuboidalPlateau);

          // dodanie elementow krajobrazu do sceny
          for (landmarkInterface *i : landmarks)
            scene.add_landmark(i);

          menu.pressEnter();
          break;
        }
      }
    }
  } catch (std::logic_error &error) {
    cout << "# Blad: " << error.what() << "." << endl;
  }
}
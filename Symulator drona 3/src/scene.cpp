#include "scene.hpp"

void scene::select_drone(const uint8_t &n) {

  if (n > drones.size())
    throw std::logic_error("proba dostepu do nieistniejacego drona");
  selected_drone = drones[n-1];
}

void scene::animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, int speed) {
  // tymczasowe rozwiazanie, nie wiem jak rozwiazac kwestie dostepu do metod drawingInterface z poziomu droneInterface i problem z dostepem do api w drone->api->redraw();
  selected_drone->animate(horizontal_distance, vertical_distance, angle, speed);
}


// void scene::remove_all() {
//   // segfaulty
//   for (size_t i=0;i<drones.size();++i)
//     remove_drone(i);

//   for (size_t i=0;i<landmarks.size();++i)
//     remove_landmark(i);
// }
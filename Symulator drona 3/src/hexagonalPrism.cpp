#include "hexagonalPrism.hpp"

void hexagonalPrism::draw() {

  std::vector<drawNS::Point3D> base1, base2;     // podstawy
  vectorMD<3> first({radius, 0, -0.5 * height}); // wektor bazowy

  // 1 podstawa
  for (uint8_t i = 0; i < 7; ++i)
    base1.push_back(convertToGlobalPoint3D(rotationMatrixMD<3>(60 * i, vectorMD<3>({0, 0, 1})) * first));
  for (uint8_t i = 0; i < 7; ++i) {
    base1.push_back(convertToGlobalPoint3D(rotationMatrixMD<3>(60 * i, vectorMD<3>({0, 0, 1})) * first));
    base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0, 0, -0.5 * height})));
  }

  // 2 podstawa
  first = first + vectorMD<3>({0, 0, height});
  for (uint8_t i = 0; i < 7; ++i)
    base2.push_back(convertToGlobalPoint3D(rotationMatrixMD<3>(60 * i, vectorMD<3>({0, 0, 1})) * first));
  for (uint8_t i = 0; i < 7; ++i) {
    base2.push_back(convertToGlobalPoint3D(rotationMatrixMD<3>(60 * i, vectorMD<3>({0, 0, 1})) * first));
    base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0, 0, 0.5 * height})));
  }

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base1, base2}, color);
}
#ifndef CUBOID_HPP
#define CUBOID_HPP

#include <iostream>

#include "Dr3D_gnuplot_api.hpp"
#include "rotationMatrixMD.hpp"
#include "vectorMD.hpp"

/**
  @brief Klasa modelujaca pojecie prostopadloscianu.
*/
class cuboid {
  std::array<vectorMD<3>, 8> vertices; // Array przechowujacy wektory wielowymiarowe reprezentujace wierzcholki prostopadloscianu.
  rotationMatrixMD<3> orientation;     // Macierz rotacji reprezentujaca orientacje prostopadloscianu.
  vectorMD<3> centroid;                // Wektor trojwymiarowy reprezentujacy srodek prostopadloscianu.

  double width;  // Szerokosc prostopadloscianu (os X).
  double height; // Wysokosc prostopadloscianu (os Y).
  double length; // Dlugosc prostopadloscianu (os Z).

 public:

  /**
		@brief Konstruktor inicjalizujacy srodek i poszczegolne wymiary prostopadloscianu.
		@param _centroid Wektor trojwymiarowy reprezentujacy srodek prostopadloscianu.
		@param _width Szerokosc prostopadloscianu (os X).
		@param _height Wysokosc prostopadloscianu (os Y).
		@param _length Dlugosc prostopadloscianu (os Z)
	*/
  cuboid(vectorMD<3> _centroid, double _width, double _height, double _length);
  
  /**
		@brief Operator dostepowy dla wierzcholkow prostopadloscianu.
		@param n numer wierzcholka prostopadloscianu (n nalezy do przedzialu [0,7]).
		@return Niemodyfikowalny wierzcholek prostopadloscianu.
	*/
  const vectorMD<3> &operator[](uint8_t n) const;

  /**
    @brief Metoda dostepowa dla srodka prostopadloscianu.
    @return Wektor trojwymiarowy reprezentujacy srodek prostopadloscianu.
  */
  const vectorMD<3> &get_centroid() const { return centroid; }

  /**
	  @brief Metoda dostepowa dla macierzy rotacji reprezentujacej orientacje prostopadloscianu.
	  @return Macierz rotacji reprezentujaca orientacje prostopadloscianu.
	*/
  const rotationMatrixMD<3> &get_orientation() const { return orientation; }

  /**
		@brief Metoda modyfikujaca orientacje prostopadloscianu (posrednia rotacja).
    @param angle kat obrotu podany w stopniach.
		@param axis wektor trojwymiarowy reprezentujacy os obrotu.
	*/
  void rotate(double angle, vectorMD<3> axis);

  /**
		@brief Metoda modyfikujaca wspolrzedne srodka prostopadloscianu (posrednia translacja).
    @param arg wektor trojwymiarowy, o ktory zostanie przesuniety prostopadloscian.
	*/
  void translate(vectorMD<3> arg);

  /**
    @brief Metoda realizujaca przygotowanie prostopadloscianu do rysowania i jego posrednie rysowanie w programie gnuplot za pomoca dostarczonego api.
    @param api wskaznik na obslugiwane api do gnuplota.
    @return ID ksztaltu wymagane w razie potrzeby pozniejszego usuniecia prostopadloscianu.
	*/
  int draw(std::shared_ptr<drawNS::Draw3DAPI> api);
};

/**
	@brief Funkcja realizujaca konwersje wektora trojwymiarowego na Point3D.
  @param arg konwertowany wektor trojwymiarowy.
	@return Point3D o identycznych parametrach, co wektor trojwymiarowy.
*/
drawNS::Point3D convert(vectorMD<3> arg);

#endif

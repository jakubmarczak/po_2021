var searchData=
[
  ['redraw_135',['redraw',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#a27a110521a511f0c75e5c867f247a3f6',1,'drawNS::APIGnuPlot3D::redraw()'],['../classdraw_n_s_1_1_draw3_d_a_p_i.html#ae88121104d2eeb8936f8dc1b68fc3bbf',1,'drawNS::Draw3DAPI::redraw()']]],
  ['replot_5floop_136',['replot_loop',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#afa364d7d8a0a74d51b680a9560e1f1ed',1,'drawNS::APIGnuPlot3D']]],
  ['rotate_137',['rotate',['../classcuboid.html#a10e7ad5ca76b37b94b13862d4cb7c32a',1,'cuboid']]],
  ['rotationmatrixmd_138',['rotationMatrixMD',['../classrotation_matrix_m_d.html#ae6542a39add90c162c0f03a155009b1a',1,'rotationMatrixMD::rotationMatrixMD()'],['../classrotation_matrix_m_d.html#add5eb8450790910c8c24da50cb675e18',1,'rotationMatrixMD::rotationMatrixMD(double angle, vectorMD&lt; dimension &gt; axis)']]],
  ['run_139',['run',['../classmenu.html#a8d1ce798dd304e75c24b3c48ac76d2f4',1,'menu']]]
];

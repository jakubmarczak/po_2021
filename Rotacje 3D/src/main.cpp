// do menu
#include <ncurses.h>

#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>
#include <array>
#include <vector>
#include <string>

// do funkcji pokroju sleep()
#include <chrono>
#include <thread>

#include "Dr3D_gnuplot_api.hpp"
#include "cuboid.hpp"
#include "menu.hpp"
#include "rotationMatrixMD.hpp"
#include "vectorMD.hpp"

using drawNS::APIGnuPlot3D;
using drawNS::Point3D;
using std::cin;
using std::cout;
using std::endl;
using std::numeric_limits;
using std::streamsize;

/**
  @brief Funkcja realizujaca czyszczenie sceny gnuplota z ksztaltow zapisanych w
  wektorze.
  @param api api do gnuplota.
  @param shapes wektor przechowujacy id ksztaltow do usuniecia.
*/
void clean(std::shared_ptr<drawNS::Draw3DAPI> api, std::vector<int> shapes) {
  for (int &x : shapes)
    api->erase_shape(x);
}

/**
  @brief Funkcja realizujaca "zatrzymanie" programu na okres czasu przekazany w
  argumencie.
  @param period okres czasu, podany w milisekundach, przez ktory "zatrzymywany"
  jest program.
*/
void wait(int period) { std::this_thread::sleep_for(std::chrono::milliseconds(period)); }

int main() {
  // menu
  std::vector<std::string> menu_options, menu_border; // vectory przechowujace: nazwy pozycji i tekst ramki menu
  attr_t menu_attr = A_REVERSE;                       // atrybut ncurses przypisywany do zaznaczonej pozycji
  uint8_t menu_choice;                                // numer pozycji wybranej przyciskiem enter
  // pozycje menu
  menu_options.push_back("Sekwencja obrotow");                                // 1
  menu_options.push_back("Powtorzenie ostatniej sekwencji obrotow");          // 2
  menu_options.push_back("Translacja o zadany wektor");                       // 3
  menu_options.push_back("Wyswietlenie macierzy rotacji");                    // 4
  menu_options.push_back("Wyswietlenie wspolrzednych wierzcholkow");          // 5
  menu_options.push_back("Sprawdzenie dlugosci przeciwleglych bokow");        // 6
  menu_options.push_back("Dodanie prostopadloscianu");                        // 7
  menu_options.push_back("Usuniecie prostopadloscianu");                      // 8
  menu_options.push_back("(do poprzednich zajec) Test gnuplota - obroty 3D"); // 9
  menu_options.push_back("Zakoncz");                                          // 10
  // tekst ramki
  menu_border.push_back(" Rotacje 3D ");                                      // tytul projektu
  menu_border.push_back(" Jakub Marczak 259319 ");                            // autor
  menu_border.push_back(" sterowanie: strzalki / klawisze W,A,S,D,Q,ENTER "); // sterowanie

  // gnuplot
  std::vector<int> shapes; // wektor przechowujacy id ksztaltow do usuniecia
  uint id;                 // id ksztaltu do usuniecia

  // prostopadloscian
  int n;                                                   // ilosc powtorzen obrotu
  double angle;                                            // kat obrotu
  double width, height, length;                            // wymiary dodawanego prostopadloscianu
  double epsilon = std::numeric_limits<double>::epsilon(); // epsilon do porownywania
                                                           // dlugosci bokow
  uint16_t selected;                                       // wybor prostopadloscianu
  vectorMD<3> axis, translation, centeroid;                // os obrotu, wektor translacji, srodek prostopadloscianu

  // definicje domyslnych prostopadloscianow
  std::vector<cuboid> cuboids{cuboid(vectorMD<3>({-12.5, -8, 10}), 5, 3, 3),
                              cuboid(vectorMD<3>({-7.5, 6, -10}), 5, 5, 5),
                              cuboid(vectorMD<3>({0, -4, -5}), 8, 3, 3),
                              cuboid(vectorMD<3>({7.5, 3, 7}), 6, 3, 6),
                              cuboid(vectorMD<3>({12.5, -10, -8}), 6, 7, 8)};

  cuboid animation(vectorMD<3>({0, 0, 0}), 0, 0, 0); // prostopadloscian do animacji obrotow

  std::vector<double> angles, sides; // vector do przechowywania katow obrotu i dlugosci bokow prostopadloscianu
  std::vector<vectorMD<3>> axes;     // vectory do przechowywania osi obrotu

  char input; // zmienna pomocnicza do czytania sekwencji obrotow

  try {
    cuboid c1(vectorMD<3>({-12.5, -8, 10}), 5, 3, 3);
    // inicjalizacja menu
    menu _menu(menu_options, menu_border, menu_attr);

    // scena [-20,20]x[-20,20]x[-20,20] odswiezana za pomoca metody redraw
    std::shared_ptr<drawNS::Draw3DAPI> api(new APIGnuPlot3D(-20, 20, -20, 20, -20, 20, -1));

    // ustawienie precyzji wyswietlania
    cout << std::fixed << std::setprecision(30) << endl;

    while (menu_choice != menu_options.size()) // ostatnia pozycja - zakoncz
    {
      menu_choice = _menu.run();

      switch (menu_choice) {
        case 1: { // sekwencja obrotow

          // wyczyszczenie vectorow
          angles.clear();
          axes.clear();
          sides.clear();

          // wybor prostopadloscianu
          selected = 0;
          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do wykonania sekwencji obrotow." << endl;
            _menu.pressEnter();
            break;
          }
          do {
            cout << "| Wybierz prostopadloscian (1-" << cuboids.size() << ") > ";
            cin >> selected;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (selected < 1 || selected > cuboids.size());
          --selected;

          animation = cuboids[selected];                // dopasowanie prostopadloscianu do animacji
          centeroid = cuboids[selected].get_centroid(); // pobranie srodka prostopadloscianu

          cout << endl
               << "| Podaj sekwencje obrotow: os obrotu [X,Y,Z], kat obrotu R, ilosc powtorzen, ..." << endl
               << "|" << endl
               << "| Przyklad sekwencji: [1 0 0] 45 3 [2 3 5] 0.1 900" << endl
               << "|" << endl
               << "| > ";

          // czytanie sekwencji obrotow
          do {
            cin >> std::ws >> input;
            if (input != '[') {
              std::cerr << endl
                        << "# Blad: brak nawiasu otwierajacego w zapisie osi obrotu." << endl;
              cin.ignore(numeric_limits<streamsize>::max(), '\n');
              break;
            }

            // pobranie wspolrzednych osi obrotu
            for (uint8_t i = 0; i < 3; ++i)
              cin >> std::ws >> axis[i];

            if (cin.fail()) {
              std::cerr << endl
                        << "# Blad: blad zapisu wspolrzednych osi obrotu." << endl;
              cin.clear();
              cin.ignore(numeric_limits<streamsize>::max(), '\n');
              break;
            }

            if (axis[0] == 0 && axis[1] == 0 && axis[2] == 0) {
              std::cerr << endl
                        << "# Blad: os obrotu nie moze byc wektorem zerowym." << endl;
              cin.clear();
              cin.ignore(numeric_limits<streamsize>::max(), '\n');
              break;
            }

            cin >> std::ws >> input;
            if (input != ']') {
              std::cerr << endl
                        << "# Blad: brak nawiasu zamykajacego w zapisie osi obrotu." << endl;
              cin.clear();
              cin.ignore(numeric_limits<streamsize>::max(), '\n');
              break;
            }

            // pobranie kata obrotu
            cin >> std::ws >> angle;
            if (fabs(angle) <= epsilon || cin.fail()) {
              std::cerr << endl
                        << "# Blad: proba obrotu o zerowy kat." << endl;
              cin.clear();
              cin.ignore(numeric_limits<streamsize>::max(), '\n');
              break;
            }

            // pobranie ilosci powtorzen
            cin >> std::ws >> n;
            if (n <= 0 || cin.fail()) {
              std::cerr << endl
                        << "# Blad: proba obrotu z zerowa lub ujemna iloscia powtorzen." << endl;
              cin.clear();
              cin.ignore(numeric_limits<streamsize>::max(), '\n');
              break;
            }

            angle *= n;

            // zapisanie kata i osi obrotu do animacji
            angles.push_back(angle);
            axes.push_back(axis);

            cuboids[selected].rotate(angle, axis); // wlasciwa rotacja
          } while (cin.peek() != '\n');

          clean(api, shapes); // wyczyszczenie sceny

          for (size_t i = 0; i < angles.size(); ++i) {
            // os obrotu
            id = api->draw_line(convert(centeroid - axes[i].normalized() * 8),
                                convert(centeroid + axes[i].normalized() * 8), "red");

            // animacja obrotu
            for (uint8_t j = 0; j < 40; ++j) {
              clean(api, shapes);
              animation.rotate(0.025 * angles[i], axes[i]);
              shapes.push_back(animation.draw(api));
              api->redraw();
              wait(50);
            }
            shapes.push_back(id);

            // informacje o obrocie
            cout << endl
                 << "@ Wykonano obrot prostopadloscianu numer " << selected + 1 << "." << endl
                 << endl
                 << "| R = " << angles[i] << endl
                 << "| X = " << axes[i][0] << endl
                 << "| Y = " << axes[i][1] << endl
                 << "| Z = " << axes[i][2] << endl;
          }

          clean(api, shapes);

          // rysowanie prostopadloscianow
          for (cuboid &i : cuboids)
            shapes.push_back(i.draw(api));
          api->redraw();

          // definicje bokow
          // X: 1-0, 3-2, 5-4, 7-6
          // Y: 4-0, 5-1, 6-2, 7-3
          // Z: 3-0, 2-1
          // Z: 7-4, 6-5

          for (uint8_t i = 0; i < 4; ++i)
            sides.push_back((cuboids[selected][2 * i + 1] - cuboids[selected][2 * i]).length());

          for (uint8_t i = 0; i < 4; ++i)
            sides.push_back((cuboids[selected][i + 4] - cuboids[selected][i]).length());

          for (uint8_t i = 0; i < 2; ++i)
            sides.push_back((cuboids[selected][3 - i] - cuboids[selected][i]).length());

          for (uint8_t i = 0; i < 2; ++i)
            sides.push_back((cuboids[selected][7 - i] - cuboids[selected][i + 4]).length());

          cout << endl;

          // X
          if (fabs(sides[3] - sides[2]) > epsilon || fabs(sides[2] - sides[1]) > epsilon ||
              fabs(sides[1] - sides[0]) > epsilon || fabs(sides[0] - sides[3]) > epsilon)
            cout << endl
                 << "# Pierwszy zestaw przeciwleglych bokow nie jest rowny!" << endl
                 << endl;
          else
            cout << endl
                 << "| Pierwszy zestaw przeciwleglych bokow jest rowny." << endl
                 << endl;
          for (uint16_t i = 0; i < 4; ++i)
            cout << "| Dlugosc " << i + 1 << " boku: " << sides[i] << endl;

          // Y
          if (fabs(sides[7] - sides[6]) > epsilon || fabs(sides[6] - sides[5]) > epsilon ||
              fabs(sides[5] - sides[4]) > epsilon || fabs(sides[4] - sides[7]) > epsilon)
            cout << endl
                 << "# Drugi zestaw przeciwleglych bokow nie jest rowny!" << endl
                 << endl;
          else
            cout << endl
                 << "| Drugi zestaw przeciwleglych bokow jest rowny." << endl
                 << endl;
          for (int i = 4; i < 8; ++i)
            cout << "| Dlugosc " << i + 1 << " boku: " << sides[i] << endl;

          // Z
          if (fabs(sides[11] - sides[10]) > epsilon || fabs(sides[10] - sides[9]) > epsilon ||
              fabs(sides[9] - sides[8]) > epsilon || fabs(sides[8] - sides[11]) > epsilon)
            cout << endl
                 << "# Trzeci zestaw przeciwleglych bokow nie jest rowny!" << endl
                 << endl;
          else
            cout << endl
                 << "| Trzeci zestaw przeciwleglych bokow jest rowny." << endl
                 << endl;
          for (uint16_t i = 8; i < 12; ++i)
            cout << "| Dlugosc " << i + 1 << " boku: " << sides[i] << endl;

          _menu.pressEnter();
          break;
        }
        case 2: { // powtorzenie ostatniej sekwencji obrotow

          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do powtorzenia sekwencji obrotow." << endl;
            _menu.pressEnter();
            break;
          }

          if (selected == 0) {
            std::cerr << endl
                      << "# Blad: nie wybrano zadnego prostopadloscianu." << endl;
            _menu.pressEnter();
            break;
          }

          if (axis[0] == 0 && axis[1] == 0 && axis[2] == 0) {
            std::cerr << endl
                      << "# Blad: os obrotu nie moze byc wektorem zerowym." << endl;
            _menu.pressEnter();
            break;
          }

          if (fabs(angle) <= epsilon) {
            std::cerr << endl
                      << "# Blad: proba obrotu o zerowy kat." << endl;
            _menu.pressEnter();
            break;
          }

          if (n == 0) {
            std::cerr << endl
                      << "# Blad: proba obrotu z zerowa iloscia powtorzen" << endl;
            _menu.pressEnter();
            break;
          }

          animation = cuboids[selected];                // dopasowanie prostopadloscianu do animacji
          centeroid = cuboids[selected].get_centroid(); // pobranie srodka prostopadloscianu

          clean(api, shapes);

          for (size_t i = 0; i < angles.size(); ++i) {
            cuboids[selected].rotate(angles[i], axes[i]); // wlasciwa rotacja

            // os obrotu
            id = api->draw_line(convert(centeroid - axes[i].normalized() * 8),
                                convert(centeroid + axes[i].normalized() * 8), "blue");

            // animacja obrotu
            for (uint8_t j = 0; j < 40; ++j) {
              clean(api, shapes);
              animation.rotate(0.025 * angles[i], axes[i]);
              shapes.push_back(animation.draw(api));
              api->redraw();
              wait(50);
            }
            shapes.push_back(id);

            // informacje o obrocie
            cout << endl
                 << "@ Powtorzono obrot prostopadloscianu numer " << selected + 1 << "." << endl
                 << endl
                 << "| R = " << angles[i] << endl
                 << "| X = " << axes[i][0] << endl
                 << "| Y = " << axes[i][1] << endl
                 << "| Z = " << axes[i][2] << endl;
          }

          clean(api, shapes);

          // rysowanie prostopadloscianow
          for (cuboid &i : cuboids)
            shapes.push_back(i.draw(api));
          api->redraw();

          _menu.pressEnter();
          break;
        }
        case 3: { // translacja o zadany wektor

          // wybor prostopadloscianu
          selected = 0;
          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do wykonania translacji." << endl;
            _menu.pressEnter();
            break;
          }
          do {
            cout << "| Wybierz prostopadloscian (1-" << cuboids.size() << ") > ";
            cin >> selected;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (selected < 1 || selected > cuboids.size());
          --selected;

          cout << endl
               << "| Podaj wektor translacji [x,y,z]." << endl
               << "|" << endl
               << "| Przyklad: -13 4 2.43" << endl
               << "|" << endl
               << "| > ";

          cin >> translation[0];
          cin >> translation[1];
          cin >> translation[2];

          centeroid = cuboids[selected].get_centroid();

          animation = cuboids[selected]; // dopasowanie prostopadloscianu do animacji

          cuboids[selected].translate(translation); // wlasciwa translacja

          // trajektoria
          id = api->draw_line(convert(centeroid), convert(centeroid + translation), "green");

          // animacja translacji
          for (uint8_t j = 0; j < 40; ++j) {
            clean(api, shapes);
            animation.translate(translation * 0.025);
            shapes.push_back(animation.draw(api));
            api->redraw();
            wait(50);
          }

          clean(api, shapes);

          // informacje o translacji
          cout << endl
               << "@ Wykonano translacje prostopadloscianu numer " << selected + 1 << "." << endl
               << endl
               << "| X = " << translation[0] << endl
               << "| Y = " << translation[1] << endl
               << "| Z = " << translation[2] << endl;

          // do usuniecia trajektorii
          shapes.push_back(id);

          // rysowanie prostopadloscianow
          for (cuboid &i : cuboids)
            shapes.push_back(i.draw(api));
          api->redraw();

          _menu.pressEnter();
          break;
        }
        case 4: { // wyswietlenie macierzy rotacji

          // wybor prostopadloscianu
          selected = 0;
          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do wyswietlenia macierzy rotacji." << endl;
            _menu.pressEnter();
            break;
          }
          do {
            cout << "| Wybierz prostopadloscian (1-" << cuboids.size() << ") > ";
            cin >> selected;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (selected < 1 || selected > cuboids.size());
          --selected;

          cout << endl
               << "@ Kolejne wiersze macierzy rotacji prostopadloscianu numer " << selected + 1 << ":" << endl
               << endl
               << cuboids[selected].get_orientation();

          _menu.pressEnter();
          break;
        }
        case 5: { // wyswietlenie wspolrzednych wierzcholkow

          // wybor prostopadloscianu
          selected = 0;
          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do wyswietlenia wspolrzednych wierzcholkow." << endl;
            _menu.pressEnter();
            break;
          }
          do {
            cout << "| Wybierz prostopadloscian (1-" << cuboids.size() << ") > ";
            cin >> selected;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (selected < 1 || selected > cuboids.size());
          --selected;

          cout << endl
               << "@ Kolejne wierzcholki prostopadloscianu numer " << selected + 1 << ":" << endl;

          for (uint16_t i = 0; i < 8; ++i)
            cout << "| Wierzcholek " << i + 1 << ":" << endl
                 << cuboids[selected][i] << endl;

          _menu.pressEnter();
          break;
        }
        case 6: { // sprawdzenie dlugosci przeciwleglych bokow

          // wybor prostopadloscianu
          selected = 0;
          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do sprawdzenia dlugosci przeciwleglych bokow." << endl;
            _menu.pressEnter();
            break;
          }
          do {
            cout << "| Wybierz prostopadloscian (1-" << cuboids.size() << ") > ";
            cin >> selected;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (selected < 1 || selected > cuboids.size());
          --selected;

          sides.clear();
          
          // definicje bokow
          // X: 1-0, 3-2, 5-4, 7-6
          // Y: 4-0, 5-1, 6-2, 7-3
          // Z: 3-0, 2-1
          // Z: 7-4, 6-5
          
          // X
          for (uint8_t i = 0; i < 4; ++i)
            sides.push_back((cuboids[selected][2 * i + 1] - cuboids[selected][2 * i]).length());

          // Y
          for (uint8_t i = 0; i < 4; ++i)
            sides.push_back((cuboids[selected][i + 4] - cuboids[selected][i]).length());

          // Z
          for (uint8_t i = 0; i < 2; ++i)
            sides.push_back((cuboids[selected][3 - i] - cuboids[selected][i]).length());

          // Z
          for (uint8_t i = 0; i < 2; ++i)
            sides.push_back((cuboids[selected][7 - i] - cuboids[selected][i + 4]).length());

          // X
          if (fabs(sides[3] - sides[2]) > epsilon || fabs(sides[2] - sides[1]) > epsilon ||
              fabs(sides[1] - sides[0]) > epsilon || fabs(sides[0] - sides[3]) > epsilon)
            cout << endl
                 << "# Pierwszy zestaw przeciwleglych bokow nie jest rowny!" << endl
                 << endl;
          else
            cout << endl
                 << "| Pierwszy zestaw przeciwleglych bokow jest rowny." << endl
                 << endl;
          for (uint16_t i = 0; i < 4; ++i)
            cout << "| Dlugosc " << i + 1 << " boku: " << sides[i] << endl;

          // Y
          if (fabs(sides[7] - sides[6]) > epsilon || fabs(sides[6] - sides[5]) > epsilon ||
              fabs(sides[5] - sides[4]) > epsilon || fabs(sides[4] - sides[7]) > epsilon)
            cout << endl
                 << "# Drugi zestaw przeciwleglych bokow nie jest rowny!" << endl
                 << endl;
          else
            cout << endl
                 << "| Drugi zestaw przeciwleglych bokow jest rowny." << endl
                 << endl;
          for (int i = 4; i < 8; ++i)
            cout << "| Dlugosc " << i + 1 << " boku: " << sides[i] << endl;

          // Z
          if (fabs(sides[11] - sides[10]) > epsilon || fabs(sides[10] - sides[9]) > epsilon ||
              fabs(sides[9] - sides[8]) > epsilon || fabs(sides[8] - sides[11]) > epsilon)
            cout << endl
                 << "# Trzeci zestaw przeciwleglych bokow nie jest rowny!" << endl
                 << endl;
          else
            cout << endl
                 << "| Trzeci zestaw przeciwleglych bokow jest rowny." << endl
                 << endl;
          for (uint16_t i = 8; i < 12; ++i)
            cout << "| Dlugosc " << i + 1 << " boku: " << sides[i] << endl;

          _menu.pressEnter();
          break;
        }
        case 7: { // dodanie prostopadloscianu

          cout << endl
               << "| Podaj wspolrzedne wektora srodka prostopadloscianu [x,y,z], szerokosc (x), wysokosc (y) oraz dlugosc (z)." << endl
               << "|" << endl
               << "| Przyklad: 1 0 0 5 3 10" << endl
               << "|" << endl
               << "| > ";

          cin >> centeroid[0];
          cin >> centeroid[1];
          cin >> centeroid[2];
          cin >> width;
          cin >> height;
          cin >> length;

          if (cin.fail() || width <= 0 || height <= 0 || length <= 0) {
            std::cerr << endl
                      << "# Blad: blad zapisu (uwaga: wymiary prostopadloscianu musza byc liczbami dodatnimi)." << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            _menu.pressEnter();
            break;
          }

          cuboids.push_back(cuboid(centeroid, width, height, length));

          // informacje o dodanym prostopadloscianie
          cout << endl
               << "@ Dodano prostopadloscian numer " << cuboids.size() << "." << endl
               << endl;

          clean(api, shapes);

          for (cuboid &i : cuboids)
            shapes.push_back(i.draw(api));
          api->redraw();

          _menu.pressEnter();
          break;
        }
        case 8: { // usuwanie prostopadloscianu

          // wybor prostopadloscianu
          selected = 0;
          if (cuboids.size() == 0) {
            std::cerr << endl
                      << "# Blad: brak prostopadloscianow do usuniecia." << endl;
            _menu.pressEnter();
            break;
          }
          do {
            cout << "| Wybierz prostopadloscian (1-" << cuboids.size() << ") > ";
            cin >> selected;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (selected < 1 || selected > cuboids.size());
          --selected;

          cuboids.erase(cuboids.begin() + selected);
          cout << endl
               << "@ Usunieto prostopadloscian numer " << selected + 1 << "." << endl;

          if (cuboids.size() > 0) {
            clean(api, shapes);

            for (cuboid &i : cuboids)
              shapes.push_back(i.draw(api));
            api->redraw();
          }

          _menu.pressEnter();
          break;
        }
        case 9: { // test gnuplota - obroty 3D

          // definicje osi obrotu i prostopadloscianow do testu
          vectorMD<3> axis_aX({1, 0, 0});
          vectorMD<3> axis_aY({0, 1, 0});
          vectorMD<3> axis_aZ({0, 0, 1});
          vectorMD<3> axis_b({3, -3, 0});
          vectorMD<3> axis_c({2, 6, 4});
          vectorMD<3> centeroid_a({-12.5, 0, 0});
          vectorMD<3> centeroid_b({0, 0, 0});
          vectorMD<3> centeroid_c({12.5, 0, 0});
          cuboid cuboid_a(centeroid_a, 5, 3, 3);
          cuboid cuboid_b(centeroid_b, 6, 6, 6);
          cuboid cuboid_c(centeroid_c, 3, 10, 3);

          wait(1000); // zatrzymanie procesu na 1s

          // os obrotu
          id = api->draw_line(convert(centeroid_a - axis_aX.normalized() * 5),
                              convert(centeroid_a + axis_aX.normalized() * 5), "red");

          // obrot wokol osi X (oryginal animowany)
          for (uint8_t i = 0; i < 36; ++i) {
            clean(api, shapes);
            cuboid_a.rotate(10, axis_aX);         // obrot
            shapes.push_back(cuboid_a.draw(api)); // rysowanie prostopadloscianu
            api->redraw();
            wait(50); // zatrzymanie procesu na 50ms w celu obserwacji obrotu
          }

          wait(1000);
          shapes.push_back(id); // do usuniecia osi obrotu

          id = api->draw_line(convert(centeroid_a - axis_aY.normalized() * 5),
                              convert(centeroid_a + axis_aY.normalized() * 5), "blue");

          // obrot wokol osi Y
          for (uint8_t i = 0; i < 36; ++i) {
            clean(api, shapes);
            cuboid_a.rotate(10, axis_aY);
            shapes.push_back(cuboid_a.draw(api));
            api->redraw();
            wait(50);
          }

          wait(1000);
          shapes.push_back(id);

          id = api->draw_line(convert(centeroid_a - axis_aZ.normalized() * 5),
                              convert(centeroid_a + axis_aZ.normalized() * 5), "green");

          // obrot wokol osi Z
          for (uint8_t i = 0; i < 36; ++i) {
            clean(api, shapes);
            cuboid_a.rotate(10, axis_aZ);
            shapes.push_back(cuboid_a.draw(api));
            api->redraw();
            wait(50);
          }

          wait(1000);
          shapes.push_back(id);

          id = api->draw_line(convert(centeroid_b - axis_b.normalized() * 8),
                              convert(centeroid_b + axis_b.normalized() * 8), "orange");

          for (uint8_t i = 0; i < 36; ++i) {
            clean(api, shapes);
            cuboid_b.rotate(10, axis_b);
            shapes.push_back(cuboid_b.draw(api));
            api->redraw();
            wait(50);
          }

          wait(1000);
          shapes.push_back(id);

          id = api->draw_line(convert(centeroid_c - axis_c.normalized() * 8),
                              convert(centeroid_c + axis_c.normalized() * 8), "yellow");

          for (uint8_t i = 0; i < 36; ++i) {
            clean(api, shapes);
            cuboid_c.rotate(10, axis_c);
            shapes.push_back(cuboid_c.draw(api));
            api->redraw();
            wait(50);
          }

          for (uint8_t i = 0; i < 36; ++i) {
            clean(api, shapes);
            cuboid_c.rotate(-10, axis_c);
            shapes.push_back(cuboid_c.draw(api));
            api->redraw();
            wait(50);
          }

          shapes.push_back(id);
          clean(api, shapes);

          shapes.push_back(cuboid_a.draw(api));
          shapes.push_back(cuboid_b.draw(api));
          shapes.push_back(cuboid_c.draw(api));
          api->redraw();

          _menu.pressEnter();
          break;
        }
        default:
          break;
      }
    }
  } catch (std::logic_error &error) {
    cout << "Blad: " << error.what() << "." << endl;
  }
}
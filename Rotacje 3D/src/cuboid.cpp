#include <iostream>
#include <cmath>
#include <vector>

#include "cuboid.hpp"
#include "rotationMatrixMD.hpp"

cuboid::cuboid(vectorMD<3> _centroid, double _width, double _height, double _length) : centroid(_centroid), width(_width), height(_height), length(_length) {
  // definicja 1 podstawy
  vertices[0] = vectorMD<3>({width, height, length}) * (-0.5);
  vertices[1] = vertices[0] + vectorMD<3>({width, 0, 0});
  vertices[2] = vertices[1] + vectorMD<3>({0, 0, length});
  vertices[3] = vertices[0] + vectorMD<3>({0, 0, length});
  // definicja 2 podstawy
  for (uint8_t i = 0; i < 4; ++i)
    vertices[i + 4] = vertices[i] + vectorMD<3>({0, height, 0});
}

const vectorMD<3> &cuboid::operator[](uint8_t n) const {
  if (n > vertices.size() - 1)
    throw std::logic_error("proba dostepu do nieistniejacego wierzcholka prostopadloscianu");
  return vertices[n];
}

void cuboid::rotate(double angle, vectorMD<3> axis) {
  rotationMatrixMD<3> rotation(angle, axis);
  orientation = rotation * orientation;
}

void cuboid::translate(vectorMD<3> arg) {
  // przesuniecie srodka prostopadloscianu
  centroid = centroid + arg;
}

int cuboid::draw(std::shared_ptr<drawNS::Draw3DAPI> api) {
  // podstawy prostopadloscianu
  std::vector<drawNS::Point3D> base1, base2;

  // aktualizacja 1 podstawy
  vertices[0] = vectorMD<3>({width, height, length}) * (-0.5);
  vertices[1] = vertices[0] + vectorMD<3>({width, 0, 0});
  vertices[2] = vertices[1] + vectorMD<3>({0, 0, length});
  vertices[3] = vertices[0] + vectorMD<3>({0, 0, length});

  // aktualizacja 2 podstawy
  for (uint8_t i = 0; i < 4; ++i)
    vertices[i + 4] = vertices[i] + vectorMD<3>({0, height, 0});

  // obrot wierzcholka + przesuniecie o wektor srodka
  for (vectorMD<3> &i : vertices)
    i = (orientation * i) + centroid;

  for (uint8_t i = 0; i < 4; ++i) {
    base1.push_back(convert(vertices[i]));
    base2.push_back(convert(vertices[i + 4]));
  }

  return api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base1, base2});
}

drawNS::Point3D convert(vectorMD<3> arg) {
  return drawNS::Point3D(arg[0], arg[1], arg[2]);
}
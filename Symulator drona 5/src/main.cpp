#include <algorithm>
#include <iostream>
#include <limits>
#include <memory>
#include <ncurses.h> // do menu
#include <string>

#include "Dr3D_gnuplot_api.hpp"
#include "basicDrone.hpp"
#include "cuboidalPlateau.hpp"
#include "droneInterface.hpp"
#include "flatSurface.hpp"
#include "hexagonalDrone.hpp"
#include "hill.hpp"
#include "menu.hpp"
#include "plateau.hpp"
#include "scene.hpp"

using drawNS::APIGnuPlot3D;
using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::numeric_limits;
using std::streamsize;

bool check(std::istream &stream) {
  // sprawdzanie bledu i 'reset'
  if (stream.fail()) {
    stream.clear();
    stream.ignore(numeric_limits<streamsize>::max(), '\n');
    return 1;
  }
  return 0;
}

int main() {

  // definicja menu
  std::vector<std::string> menu_options, menu_border;                     // vectory przechowujace: nazwy pozycji i tekst ramki menu
  attr_t menu_attr = A_REVERSE;                                           // atrybut ncurses przypisywany do zaznaczonej pozycji
  uint8_t menu_choice;                                                    // numer pozycji wybranej przyciskiem enter
  menu_options.push_back("Generate a simple scene");                      // pozycja 1 - wygeneruj prosta scene
  menu_options.push_back("Maneuver the selected drone");                  // pozycja 2 - manewruj wybranym dronem
  menu_options.push_back("Repeat the last manouver");                     // pozycja 3 - powtorz ostatni manewr
  menu_options.push_back("Land the selected drone");                      // pozycja 4 - wyladuj dronem
  menu_options.push_back("Select a drone");                               // pozycja 5 - wybierz drona
  menu_options.push_back("Highlight an object");                          // pozycja 6 - "podswietl" wybrany obiekt
  menu_options.push_back("Add a drone to the scene");                     // pozycja 7 - dodaj drona do sceny
  menu_options.push_back("Add a landmark to the scene");                  // pozycja 8 - dodaj element krajobrazu do sceny
  menu_options.push_back("Remove an object from the scene");              // pozycja 9 - usun obiekt ze sceny
  menu_options.push_back("Remove all objects from the scene");            // pozycja 10 - usun wszystkie obiekty ze sceny
  menu_options.push_back("Show short description of the selected drone"); // pozycja 11 - wypisz krotki opis wybranego drona
  menu_options.push_back("Show full description of the selected drone");  // pozycja 12 - wypisz pelny opis wybranego drona
  menu_options.push_back("Show current 3D vector statistics");            // pozycja 13 - wyswietl obecne statystyki wektorow trojwymiarowych
  menu_options.push_back("Exit");                                         // pozycja 14 - zakoncz
  menu_border.push_back(" Drone simulator ");                             // tytul projektu
  menu_border.push_back(" Jakub Marczak (259319) ");                      // autor
  menu_border.push_back(" controls: arrows / W,A,S,D,Q,ENTER ");          // sterowanie

  // dostepne kolory, w ktorych rysuje gnuplot
  std::vector<std::string> colors = {"red", "orange", "yellow", "green", "blue", "light-blue", "purple", "white", "grey", "black"};

  // zmienne pomocnicze
  uint16_t id, n;
  uint16_t speed;
  std::string type, color;
  bool selected = false, manouvre = false;
  double horizontal_distance, vertical_distance, angle;
  double x, y, width, length, height, body_radius, propeller_radius;
  double rotation, min, max;
  vectorMD<3> base({0, 0, 0}); // wektor do statystyk

  try {

    menu menu(menu_options, menu_border, menu_attr); // inicjalizacja menu

    // scena X[-20,20] x Y[-20,20] x Z[-5,35] odswiezana za pomoca metody redraw
    std::shared_ptr<drawNS::Draw3DAPI> api(new APIGnuPlot3D(-20, 20, -20, 20, -5, 35, -1));

    scene scene;                        // scena z dronami i elementami krajobrazu
    flatSurface floor(api, 8, 5, 5, 0); // powierzchnia plaska, "podloga"

    while (menu_choice != menu_options.size()) // ostatnia pozycja - zakoncz
    {
      menu_choice = menu.run();

      switch (menu_choice) {
        case 1: { // pozycja 1 - wygeneruj prosta scene

          scene.remove_all();

          // dodanie elementow krajobrazu do sceny
          scene.add_landmark(std::shared_ptr<landmarkInterface>(new hill(api, vectorMD<3>({-10, -10, 0}), rotationMatrixMD<3>(), 6, 10, 7, "light-blue")));
          scene.add_landmark(std::shared_ptr<landmarkInterface>(new plateau(api, vectorMD<3>({-10, 10, 0}), rotationMatrixMD<3>(), 6, 7, 8, "black")));
          scene.add_landmark(std::shared_ptr<landmarkInterface>(new cuboidalPlateau(api, vectorMD<3>({7.5, 0, 2}), rotationMatrixMD<3>(), 4, 9, 4, "grey")));

          // dodanie dronow do sceny
          scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({12.5, -12.5, 0}), rotationMatrixMD<3>(), 1.5, 2.5, 1, "red")));
          scene.add_drone(std::shared_ptr<droneInterface>(new basicDrone(api, vectorMD<3>({12.5, 12.5, 0}), rotationMatrixMD<3>(), 3, 7, 1.2, 0.9, "green")));
          scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({-10, 10, 8}), rotationMatrixMD<3>(), 3, 2, 0.8, "blue")));

          cout << "@ Successfully generated a basic scene with 3 landmarks and 3 drones." << endl;

          menu.pressEnter();
          break;
        }
        case 2: { // pozycja 2 - manewruj wybranym dronem

          if (!selected) {
            cout << "# No drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          // dystans horyzontalny
          cout << "| Enter horizontal distance > ";
          cin.ignore(numeric_limits<streamsize>::max(), '\n');
          cin >> horizontal_distance;
          //blad
          while (check(cin)) {
            cout << "| > ";
            cin >> horizontal_distance;
          }

          // dystans wertykalny
          cout << "| Enter vertical distance > ";
          cin >> vertical_distance;
          //blad
          while (check(cin)) {
            cout << "| > ";
            cin >> vertical_distance;
          }

          // rotacja
          cout << "| Enter rotation angle > ";
          cin >> angle;
          //blad
          while (check(cin)) {
            cout << "| > ";
            cin >> angle;
          }

          // predkosc
          cout << "| Enter speed value > ";
          cin >> speed;
          //blad
          while (check(cin)) {
            cout << "| > ";
            cin >> speed;
          }

          scene.animate(horizontal_distance, vertical_distance, angle, speed);
          cout << endl;
          scene.describe_drone_short(cout);

          cout << endl
               << "@ Successfully manoeuvred the selected drone." << endl;

          manouvre = true;

          menu.pressEnter();
          break;
        }
        case 3: { // pozycja 3 - powtorz ostatni manewr

          if (!selected) {
            cout << "# No drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          if (!manouvre) {
            cout << "# No maneuvers have been completed." << endl;
            menu.pressEnter();
            break;
          }

          scene.animate(horizontal_distance, vertical_distance, angle, speed);
          cout << endl;
          scene.describe_drone_short(cout);

          cout << endl
               << "@ Successfully repeated the last maneuver." << endl;

          menu.pressEnter();
          break;
        }
        case 4: { // pozycja 4 - wyladuj dronem

          scene.land_drone();
          cout << "@ Landing attempt completed." << endl;

          menu.pressEnter();
          break;
        }
        case 5: { // pozycja 5 - wybierz drona

          // aktualizacja ilosci obiektow sceny
          id = 0;
          n = scene.quantity();

          // sprawdzenie, czy scena jest pusta
          if (n == 0) {
            cerr << endl
                 << "# The scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          // wprowadzenie ID
          cout << "| Enter object ID (1-" << n << ") > ";
          cin >> id;
          //blad
          while (check(cin) || (id < 1 || id > n)) {
            cout << "| > ";
            cin >> id;
          }

          // wybor, "podswietlenie" drona i potwierdzenie
          if (scene.select_drone(id)) {
            cout << "@ Successfully selected a drone with ID (" << id << ")." << endl;
            scene.highlight(id);
            selected = true;
          }
          // nieprawidlowy wybor
          else {
            cout << "# Selected object is not of a type drone." << endl;
            selected = false;
          }

          menu.pressEnter();
          break;
        }
        case 6: { // pozycja 6 - "podswietl" wybrany obiekt

          // aktualizacja ilosci obiektow sceny
          id = 0;
          n = scene.quantity();

          // sprawdzenie, czy scena jest pusta
          if (n == 0) {
            cerr << endl
                 << "# The scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          // ID
          cout << "| Enter object ID (1-" << n << ") > ";
          cin >> id;
          while (id < 1 || id > n) {
            cout << "| > ";
            cin >> id;
            check(cin);
          }

          // "podswietlenie" wybranego obiektu i potwierdzenie
          scene.highlight(id);
          cout << "@ Successfully highlighted a selected object." << endl;

          menu.pressEnter();
          break;
        }
        case 7: { // pozycja 7 - dodaj drona do sceny

          // typ drona
          cout << "| Enter drone type > ";
          cin >> type;
          //blad
          while (check(cin) || (type != "basic" && type != "hexagonal")) {
            cout << "# Available drone types: basic, hexagonal." << endl
                 << "| > ";
            cin >> type;
          }

          // wspolrzedne srodka
          cout << "| Enter base coordinates > ";
          cin >> x;
          cin >> y;
          // blad
          while (check(cin)) {
            cout << "# Enter the coordinates in 'X Y' format, e.g. 6 -4" << endl
                 << "| > ";
            cin >> x;
            cin >> y;
          }

          // dla drona bazowego
          if (type == "basic") {
            // szerokosc korpusu
            cout << "| Enter body width (X axis) > ";
            cin >> width;
            // blad
            while (check(cin) || width == 0) {
              cout << "| > ";
              cin >> width;
            }
            // dlugosc korpusu
            cout << "| Enter body length (Y axis) > ";
            cin >> length;
            // blad
            while (check(cin) || length == 0) {
              cout << "| > ";
              cin >> length;
            }
          }

          // wysokosc korpusu
          cout << "| Enter body height (Z axis) > ";
          cin >> height;
          // blad
          while (check(cin) || height == 0) {
            cout << "| > ";
            cin >> height;
          }

          // dla drona heksagonalnego
          if (type == "hexagonal") {
            // promien korpusu
            cout << "| Enter body radius (XY plane) > ";
            cin >> body_radius;
            // blad
            while (check(cin) || body_radius == 0) {
              cout << "| > ";
              cin >> body_radius;
            }
          }

          // promien wirnikow
          cout << "| Enter propeller radius (XY plane) > ";
          cin >> propeller_radius;
          // blad
          while (check(cin) || propeller_radius == 0) {
            cout << "| > ";
            cin >> propeller_radius;
          }

          // standaryzacja wirnikow
          if (type == "basic" && (propeller_radius > 0.5 * width || propeller_radius > 0.5 * length)) {
            if (width >= length)
              propeller_radius = 0.4 * length;
            else
              propeller_radius = 0.4 * width;
            std::cerr << "# Warning: propeller radius has been changed to " << propeller_radius << " in order to create a valid basicDrone." << endl;
          } else if (type == "hexagonal" && (propeller_radius > 0.5 * body_radius)) {
            propeller_radius = 0.4 * body_radius;
            std::cerr << "# Warning: propeller radius has been changed to " << propeller_radius << " in order to create a valid hexagonalDrone." << endl;
          }

          // kolor
          cout << "| Enter color > ";
          cin >> color;
          // blad
          while (std::find(std::begin(colors), std::end(colors), color) == colors.end()) {
            cout << "# Available colors: ";
            for (std::string i : colors) {
              cout << i;
              if (i != colors.back())
                cout << ", ";
            }
            cout << "." << endl;
            cout << "| > ";
            cin >> color;
          }

          // dodanie dronow do sceny i potwierdzenie
          if (type == "basic") {
            scene.add_drone(std::shared_ptr<droneInterface>(new basicDrone(api, vectorMD<3>({x, y, 0}), rotationMatrixMD<3>(), width, length, height, propeller_radius, color)));
            cout << "@ Successfully added a basic drone with ID (" << scene.quantity() << ") to the scene." << endl;
          } else if (type == "hexagonal") {
            scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({x, y, 0}), rotationMatrixMD<3>(), height, body_radius, propeller_radius, color)));
            cout << "@ Successfully added a hexagonal drone with ID (" << scene.quantity() << ") to the scene." << endl;
          }

          menu.pressEnter();
          break;
        }
        case 8: { // pozycja 8 - dodaj element krajobrazu do sceny

          // typ elementu krajobrazu
          cout << "| Enter landmark type > ";
          cin >> type;
          // blad
          while (check(cin) || (type != "hill" && type != "plateau" && type != "cuboidalplateau")) {
            cout << "# Available landmark types: hill, plateau, cuboidalplateau" << endl
                 << "| > ";
            cin >> type;
          }

          // wspolrzedne srodka
          cout << "| Enter base coordinates > ";
          cin >> x;
          cin >> y;
          // blad
          while (check(cin)) {
            cout << "# Enter the coordinates in 'X Y' format, e.g. 6 -4" << endl
                 << "| > ";
            cin >> x;
            cin >> y;
          }

          // kat obrotu
          cout << "| Enter rotation angle (Z axis) > ";
          cin >> rotation;
          // blad
          while (check(cin)) {
            cout << "| > ";
            cin >> rotation;
          }

          // najmniejsza wartosc generowanej szerokosci i dlugosci
          cout << "| Enter the lower bound > ";
          cin >> min;
          // blad
          while (check(cin)) {
            cout << "# Whereas the lower bound is the lowest possible value of generation for vertices & radius / width & length (cuboidalPlateau)." << endl
                 << "| > ";
            cin >> min;
          }

          // najwieksza wartosc generowanej szerokosci i dlugosci
          cout << "| Enter the upper bound > ";
          cin >> max;
          // blad
          while (check(cin)) {
            cout << "# Whereas the upper bound is the highest possible value of generation for vertices & radius / width & length (cuboidalPlateau)." << endl
                 << "| > ";
            cin >> max;
          }

          // wysokosc
          cout << "| Enter height > ";
          cin >> height;
          // blad
          while (check(cin) || height == 0) {
            cout << "# Height cannot be of zero value." << endl
                 << "| > ";
            cin >> height;
          }

          // kolor
          cout << "| Enter color > ";
          cin >> color;
          // blad
          while (std::find(std::begin(colors), std::end(colors), color) == colors.end()) {
            cout << "# Available colors: ";
            for (std::string i : colors) {
              cout << i;
              if (i != colors.back())
                cout << ", ";
            }
            cout << "." << endl;
            cout << "| > ";
            cin >> color;
          }

          // dodanie do sceny i potwierdzenie
          if (type == "hill") {
            scene.add_landmark(std::shared_ptr<landmarkInterface>(new hill(api, vectorMD<3>({x, y, 0}), rotationMatrixMD<3>(rotation, vectorMD<3>({0, 0, 1})), min, max, height, color)));
            cout << "@ Successfully added a hill with ID (" << scene.quantity() << ") to the scene." << endl;
          } else if (type == "plateau") {
            scene.add_landmark(std::shared_ptr<landmarkInterface>(new plateau(api, vectorMD<3>({x, y, 0}), rotationMatrixMD<3>(rotation, vectorMD<3>({0, 0, 1})), min, max, height, color)));
            cout << "@ Successfully added a plateau with ID (" << scene.quantity() << ") to the scene." << endl;
          } else if (type == "cuboidalplateau") {
            scene.add_landmark(std::shared_ptr<landmarkInterface>(new cuboidalPlateau(api, vectorMD<3>({x, y, height / 2}), rotationMatrixMD<3>(rotation, vectorMD<3>({0, 0, 1})), min, max, height, color)));
            cout << "@ Successfully added a cuboidal plateau with ID (" << scene.quantity() << ") to the scene." << endl;
          }

          menu.pressEnter();
          break;
        }
        case 9: { // pozycja 9 - usun obiekt ze sceny

          // aktualizacja ilosci obiektow sceny
          id = 0;
          n = scene.quantity();

          // sprawdzenie, czy scena jest pusta
          if (n == 0) {
            cerr << endl
                 << "# The scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          // ID
          do {
            cout << "| Enter object ID (1-" << n << ") > ";
            cin >> id;
            check(cin);
          } while (id < 1 || id > n);

          // usuniecie obiektu ze sceny i potwierdzenie
          scene.remove(id);
          cout << "@ Successfully removed an object with ID (" << id << ") from the scene." << endl;

          selected = false;

          menu.pressEnter();
          break;
        }
        case 10: { // pozycja 10 - usun wszystkie obiekty ze sceny

          // aktualizacja ilosci obiektow sceny
          id = 0;
          n = scene.quantity();

          // sprawdzenie, czy scena jest pusta
          if (n == 0) {
            cerr << endl
                 << "# The scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          // usuniecie wszystkich obiektow ze sceny i potwierdzenie
          scene.remove_all();
          cout << "@ Successfully removed all objects from the scene." << endl;

          selected = false;

          menu.pressEnter();
          break;
        }
        case 11: { // pozycja 11 - wypisz krotki opis wybranego drona

          // sprawdzenie, czy wybrano drona
          if (!selected) {
            cout << "# No drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          // wypisanie krotkiego opisu wybranego drona
          scene.describe_drone_short(cout);

          menu.pressEnter();
          break;
        }
        case 12: { // pozycja 12 - wypisz pelny opis wybranego drona

          // sprawdzenie, czy wybrano drona
          if (!selected) {
            cout << "# No drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          // wypisanie pelnego opisu wybranego drona
          scene.describe_drone_full(cout);

          menu.pressEnter();
          break;
        }
        case 13: { // pozycja 13 - wyswietl obecne statystyki wektorow trojwymiarowych

          // wyswietlenie obecnych statystyk wektorow trojwymiarowych
          cout << "| Number of created 3-dimensional vectorMDs: " << base.created() << endl;
          cout << "| Number of existing 3-dimensional vectorMDs: " << base.existing() << endl;

          menu.pressEnter();
          break;
        }
      }
    }
  } catch (std::logic_error &error) {

    cout << "# Error: " << error.what() << "." << endl;
  }
}
#include "hill.hpp"

#include <algorithm>
#include <random>

hill::hill(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double _min,
    double _max,
    double _height,
    std::string color)
    : coordinateSystem(centroid, orientation, nullptr),
      drawingInterface(api, color) {
  // standaryzacja
  min = fabs(_min);
  max = fabs(_max);
  height = fabs(_height);
  if (min < 3) {
    min = 3;
    std::cerr << "# Warning: the lower bound has been changed to 3 in order to create a valid hill." << std::endl;
  }
  if (max < min) {
    max = min;
    std::cerr << "# Warning: the upper bound has been changed to " << min << " in order to create a valid hill." << std::endl;
  }
  // randomizacja
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_int_distribution<int> vertices_distribution(min, max);
  std::uniform_real_distribution<double> radius_distribution(0.5 * min, 0.5 * max);
  int vertices_number = vertices_distribution(gen); // randomizacja ilosci wierzcholkow
  for (int i = 0; i < vertices_number; ++i)
    vertices.push_back(rotationMatrixMD<3>(360 * i / vertices_number, vectorMD<3>({0, 0, 1})) * vectorMD<3>({1, 1, 0}) * radius_distribution(gen));
  // rysowanie
  draw();
  api->redraw();
}

void hill::draw() {

  std::vector<drawNS::Point3D> base, summit; // podstawa i gorny wierzcholek

  for (vectorMD<3> i : vertices) {
    base.push_back(convertToGlobalPoint3D(i));
    summit.push_back(convertToGlobalPoint3D(vectorMD<3>({0, 0, height})));
  }

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base, summit}, color);
}

bool hill::is_above(std::shared_ptr<droneInterface> drone) {
  
  // APROKSYMACJA: 
  // podstawa wzgorza -> okrąg o promieniu wyznaczonym na podstawie wiadomosci o generacji wierzcholkow
  // dron -> okrag opisany na podstawie korpusu
  
  double radius = vertices.size();

  double drone_width = 0.5 * drone->get_width();
  double drone_length = 0.5 * drone->get_length();
  double drone_radius = sqrt(pow(drone_width,2) + pow(drone_length,2));

  vectorMD<3> vdistance = centroid - std::dynamic_pointer_cast<coordinateSystem>(drone)->get_centroid();
  double distance = sqrt(pow(vdistance[0],2) + pow(vdistance[1],2));
  
  if ((distance < radius + drone_radius) && (std::dynamic_pointer_cast<coordinateSystem>(drone)->get_centroid()[2]- 0.5 * drone->get_height() >= height))
    return true;
  return false;
}

bool hill::can_land(std::shared_ptr<droneInterface> drone) {
  // nigdy nie mozna ladowac na wzgorzu
  return false;
}
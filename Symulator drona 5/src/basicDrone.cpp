#include <chrono>
#include <cmath>
#include <thread>

#include "Dr3D_gnuplot_api.hpp"
#include "basicDrone.hpp"

basicDrone::basicDrone(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double width,
    double length,
    double height,
    double radius,
    std::string color)
    : coordinateSystem(centroid, orientation, nullptr),
      drawingInterface(api, color),
      body(api, vectorMD<3>({0, 0, 0.5 * height}), rotationMatrixMD<3>(), this, width, length, height, color),
      propellers{hexagonalPrism(api, vectorMD<3>({0.5 * width, -0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
                 hexagonalPrism(api, vectorMD<3>({0.5 * width, 0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
                 hexagonalPrism(api, vectorMD<3>({-0.5 * width, 0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
                 hexagonalPrism(api, vectorMD<3>({-0.5 * width, -0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color)} {
  // NIEROZWIĄZANE: bez powyższych 4 linijek tak czy inaczej zmazuje sie podloga i pojawiaja sie 4 komunikaty z api
  // // standaryzacja wirnikow
  // if (radius > 0.5 * width || radius > 0.5 * length) {
  //   if (width >= length)
  //     radius = 0.4 * length;
  //   else
  //     radius = 0.4 * width;
  //   std::cerr << "# Warning: propeller radius has been changed to " << radius << " in order to create a valid drone." << std::endl;
  // }
  // propellers[0] = hexagonalPrism(api, vectorMD<3>({0.5 * width, -0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color);
  // propellers[1] = hexagonalPrism(api, vectorMD<3>({0.5 * width, 0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color);
  // propellers[2] = hexagonalPrism(api, vectorMD<3>({-0.5 * width, 0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color);
  // propellers[3] = hexagonalPrism(api, vectorMD<3>({-0.5 * width, -0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color);
  // rysowanie
  draw();
  api->redraw();
}

void basicDrone::shortDescription(std::ostream &stream) {
  stream << "@ SHORT DRONE DESCRIPTION" << std::endl
         << "|" << std::endl
         << "| Type: basicDrone" << std::endl
         << "| Color: " << color << std::endl
         << "|" << std::endl
         << "| Current position:" << std::endl
         << centroid;
}

void basicDrone::fullDescription(std::ostream &stream) {
  stream << "@ FULL DRONE DESCRIPTION" << std::endl
         << "|" << std::endl
         << "| Type: basicDrone" << std::endl
         << "| Color: " << color << std::endl
         << "|" << std::endl
         << "| Current position:" << std::endl
         << centroid
         << "|" << std::endl
         << "> Body's description" << std::endl
         << "|" << std::endl
         << "| Type: cuboidal" << std::endl;

  body.describe(stream);

  stream << "|" << std::endl
         << "> Propellers' description" << std::endl
         << "|" << std::endl
         << "| Type: 4 x hexagonal" << std::endl
         << "|" << std::endl;

  propellers[0].describe(stream);
}

void basicDrone::spin_propellers(const double &angle) {

  rotationMatrixMD<3> right(angle, vectorMD<3>({0, 0, 1}));
  rotationMatrixMD<3> left(angle, vectorMD<3>({0, 0, -1}));

  propellers[0].rotate(right); // obrot w prawo
  propellers[1].rotate(left);  // obrot w lewo
  propellers[2].rotate(right);
  propellers[3].rotate(left);
}

void basicDrone::animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed) {

  int8_t direction = 0; // kierunek przesuniecia poziomego (+/-)

  if (horizontal_distance != 0)
    direction = std::copysign(1, horizontal_distance);

  // normalizacja predkosci
  if (speed >= 100)
    speed = 99;

  // zmazywanie "domyslnego" drona (z konstruktora)
  erase();

  for (uint8_t i = 0; i < 100 - speed; ++i) {

    // przesuniecia drona w pionie i poziomie
    move_vertically(vertical_distance / (100 - speed));
    move_horizontally(horizontal_distance / (100 - speed));

    // rotacja drona i wirnikow wokol osi Z
    spin(angle / (100 - speed));
    spin_propellers(360 / (100 - speed));

    // przechylenie drona
    if (i <= 0.5 * (100 - speed))
      tilt(-direction * speed / (i + 4)); // do przodu
    else
      tilt(direction * speed / (i + 4)); // do tylu

    // rysowanie drona i odswiezenie sceny
    draw();
    api->redraw();

    // poprawka dla kolejnych przesuniec
    if (i <= 0.5 * (100 - speed))
      tilt(direction * speed / (i + 4));
    else
      tilt(-direction * speed / (i + 4));

    // zmazywanie drona (pojedynczej "klatki")
    erase();

    // przestoj 50ms przed kolejna klatka
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }

  // rysowanie "domyslnego" drona
  draw();
  api->redraw();
}

void basicDrone::draw() {
  body.draw();
  for (hexagonalPrism &i : propellers)
    i.draw();
}

void basicDrone::erase() {
  body.erase();
  for (hexagonalPrism &i : propellers)
    i.erase();
}

void basicDrone::rescale(const double &scale) {
  body.rescale(scale);
  for (hexagonalPrism &i : propellers)
    i.rescale(scale);
}

bool basicDrone::is_above(std::shared_ptr<droneInterface> drone) {

  // APROKSYMACJA: SZESCIOKAT FOREMNY -> KWADRAT

  // a - dron 'stacjonarny', b - dron, dla ktorego wykonywane jest sprawdzenie

  //          H --------- G
  //          |     b     |
  //          |           |
  //   D -----|----- C    |
  //   |      E --------- F
  //   |   a         |
  //   A ----------- B

  double axmin = centroid[0] - 0.5 * get_width();  // A
  double axmax = centroid[0] + 0.5 * get_width();  // B
  double aymin = centroid[1] - 0.5 * get_length(); // A
  double aymax = centroid[1] + 0.5 * get_length(); // D

  vectorMD<3> drone_centroid = std::dynamic_pointer_cast<coordinateSystem>(drone)->get_centroid();
  double drone_width = 0.5 * drone->get_width();
  double drone_length = 0.5 * drone->get_length();

  double bxmin = drone_centroid[0] - drone_width;  // E
  double bxmax = drone_centroid[0] + drone_width;  // F
  double bymin = drone_centroid[1] - drone_length; // E
  double bymax = drone_centroid[1] + drone_length; // H

  if ((axmin < bxmax && bxmin < axmax) && (aymin < bymax && bymin < aymax) && (drone_centroid[2]  - 0.5 * drone->get_height() >= centroid[2]))
    return true;
  return false;
}

bool basicDrone::can_land(std::shared_ptr<droneInterface> drone) {
  // nigdy nie mozna ladowac na innym dronie
  return false;
}
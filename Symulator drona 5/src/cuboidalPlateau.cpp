#include "cuboidalPlateau.hpp"
#include <random>

cuboidalPlateau::cuboidalPlateau(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double _min,
    double _max,
    double _height,
    std::string color)
    : cuboid(api, centroid, orientation, nullptr, width, length, _height, color) {
  // standaryzacja
  min = fabs(_min);
  max = fabs(_max);
  height = fabs(_height);
  if (min <= std::numeric_limits<double>::epsilon()) {
    min = 1;
    std::cerr << "# Warning: the lower bound has been changed to 1 in order to create a valid cuboidalPlateau." << std::endl;
  }
  if (max < min) {
    max = min;
    std::cerr << "# Warning: the upper bound has been changed to " << min << " in order to create a valid cuboidalPlateau." << std::endl;
  }
  // randomizacja
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_real_distribution<double> width_distribution(min, max);
  std::uniform_real_distribution<double> length_distribution(min, max);
  width = width_distribution(gen);
  length = length_distribution(gen);
  // rysowanie
  draw();
  api->redraw();
}

bool cuboidalPlateau::is_above(std::shared_ptr<droneInterface> drone) {

  // a - plaskowyz prostopadloscienny, b - dron, dla ktorego wykonywane jest sprawdzenie

  //          H --------- G
  //          |     b     |
  //          |           |
  //   D -----|----- C    |
  //   |      E --------- F
  //   |   a         |
  //   A ----------- B

  double axmin = centroid[0] - 0.5 * get_width();  // A
  double axmax = centroid[0] + 0.5 * get_width();  // B
  double aymin = centroid[1] - 0.5 * get_length(); // A
  double aymax = centroid[1] + 0.5 * get_length(); // D

  vectorMD<3> drone_centroid = std::dynamic_pointer_cast<coordinateSystem>(drone)->get_centroid();
  double drone_width = 0.5 * drone->get_width();
  double drone_length = 0.5 * drone->get_length();

  double bxmin = drone_centroid[0] - drone_width;  // E
  double bxmax = drone_centroid[0] + drone_width;  // F
  double bymin = drone_centroid[1] - drone_length; // E
  double bymax = drone_centroid[1] + drone_length; // H

  if ((axmin < bxmax && bxmin < axmax) && (aymin < bymax && bymin < aymax))
    return true;
  return false;
}

bool cuboidalPlateau::can_land(std::shared_ptr<droneInterface> drone) {

  // srodek drona musi byc w obszarze plaszczyzny XY przecinajacej prostopadloscian

  //       |
  //   A --|---------.
  //   |   |         |
  // ------x--------------dy
  //   B --|-------- C
  //       |
  //       dx

  double xmin = centroid[0] - 0.5 * get_width();  // B
  double xmax = centroid[0] + 0.5 * get_width();  // C
  double ymin = centroid[1] - 0.5 * get_length(); // B
  double ymax = centroid[1] + 0.5 * get_length(); // A

  // srodek drona
  vectorMD<3> drone_centroid = std::dynamic_pointer_cast<coordinateSystem>(drone)->get_centroid();
  double dx = drone_centroid[0];
  double dy = drone_centroid[1];

  if ((dx > xmin && dx < xmax) && (dy > ymin && dy < ymax) && (drone_centroid[2] - 0.5 * drone->get_height() >= height))
    return true;
  return false;
}

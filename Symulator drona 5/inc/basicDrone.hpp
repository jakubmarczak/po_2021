#ifndef DRONE_HPP
#define DRONE_HPP

#include "coordinateSystem.hpp"
#include "cuboid.hpp"
#include "drawingInterface.hpp"
#include "droneInterface.hpp"
#include "hexagonalPrism.hpp"
#include "landmarkInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie drona bazowego - zlozonego z korpusu prostopadlosciennego i czterech wirnikow w postaci graniastoslupow prawidlowych szesciokatnych.
*/
class basicDrone : public coordinateSystem, public drawingInterface, public droneInterface, public landmarkInterface {
protected:
  cuboid body;                              // Prostopadloscian reprezentujacy korpus drona.
  std::array<hexagonalPrism, 4> propellers; // Array przechowujacy graniastoslupy prawidlowe szesciokatne reprezentujace wirniki drona.

public:
  /**
   * @brief Konstruktor parametryczny inicjalizujacy uklad wspolrzednych, interfejs rysowania, kolor drona oraz jego korpus i wirniki.
   * @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
   * @param centroid Wektor trojwymiarowy reprezentujacy srodek drona (srodek korpusu).
   * @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje drona.
   * @param width Szerokosc korpusu drona (w osi X).
   * @param length Dlugosc korpusu drona (w osi Y).
   * @param height Wysokosc korpusu drona (w osi Z).
   * @param propeller_radius Promien okregu wpisanego w graniastoslup prawidlowy szesciokatny reprezentujacy wirnik drona.
   * @param color Kolor drona.
	*/
  basicDrone(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double width,
      double length,
      double height,
      double radius,
      std::string color = "black");

  /**
   * @brief Destruktor umozliwiajacy zmazanie drona wraz z jego usunieciem.
  */
  ~basicDrone() { erase(); api->redraw(); }
 
  // Metody wirtualne droneInterface

  vectorMD<3> get_centroid() override { return body.get_centroid(); }
  double get_width() override { return body.get_width(); }
  double get_length() override { return body.get_length(); }
  double get_height() override { return body.get_height(); }
  void shortDescription(std::ostream &stream) override;
  void fullDescription(std::ostream &stream) override;
  void move_horizontally(const double &distance) override { translate(orientation * vectorMD<3>({0, distance, 0})); }
  void move_vertically(const double &distance) override { translate(orientation * vectorMD<3>({0, 0, distance})); }
  void tilt(const double &angle) override { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({1, 0, 0}))); }
  void spin(const double &angle) override { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({0, 0, 1}))); }
  void spin_propellers(const double &angle) override;
  void animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed) override;

  // Metody wirtualne drawingInterface

  void draw() override;
  void erase() override;
  void rescale(const double &scale) override;

  // Metody wirtualne landmarkInterface
  
  bool is_above(std::shared_ptr<droneInterface> drone) override;
  bool can_land(std::shared_ptr<droneInterface> drone) override;
};

#endif

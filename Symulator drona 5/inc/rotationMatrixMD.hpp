#ifndef ROTATIONMATRIXMD_HPP
#define ROTATIONMATRIXMD_HPP

#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>
#include <array>

#include "vectorMD.hpp"

/**
 * @brief Szablon klasy modelujacej pojecie wielowymiarowej macierzy obrotu/rotacji (MultiDimensional rotation matrix).
*/
template <uint8_t dimension>
class rotationMatrixMD {
 public:
  /**
   * @brief Array przechowujacy wiersze macierzy rotacji.
   * @param dimension wymiar/stopien macierzy.
	*/
  std::array<vectorMD<dimension>, dimension> rows;
  
  /**
		@brief Konstruktor domyslny tworzacy macierz identycznosciowa (jednostkowa).
	*/
  rotationMatrixMD();

  /**
   * @brief Konstruktor tworzacy wielowymiarowa macierz obrotu/rotacji o zadany kat.
   * @param angle kat obrotu podany w stopniach.
   * @param axis wektor reprezentujacy os rotacji.
	*/
  explicit rotationMatrixMD(double angle, vectorMD<dimension> axis);

  /**
   * @brief Operator dostepowy dla wierszy macierzy rotacji.
   * @param n numer wiersza macierzy rotacji (n>=0).
   * @return Wektor bedacy n-tym wierszem macierzy rotacji.
	*/
  const vectorMD<dimension> &operator[](uint8_t n) const;

  /**
   * @brief Operator iloczynu macierzy rotacji i wektora.
   * @param arg wektor, dla ktorego wykonujemy iloczyn.
   * @return Wektor bedacy wynikiem iloczynu.
	*/
  vectorMD<dimension> operator*(const vectorMD<dimension> &arg) const;

  /**
   * @brief Operator iloczynu dwoch macierzy rotacji.
   * @param arg druga z macierzy rotacji, dla ktorych wykonujemy iloczyn.
   * @return Macierz rotacji bedaca wynikiem iloczynu.
	*/
  rotationMatrixMD<dimension> operator*(const rotationMatrixMD<dimension> &arg) const;

  /**
   * @brief Metoda pozwalajaca na transpozycje macierzy rotacji.
   * @return Transponowana macierz rotacji.
	*/
  rotationMatrixMD<dimension> transpose() const;
};

/**
 * @brief Operator przesuniecia bitowego w lewo (wypisanie) dla macierzy rotacji.
 * @param stream obslugiwany strumien.
 * @param arg wektor wielowymiarowy, ktorego wspolrzedne chcemy wypisac.
 * @return Obslugiwany strumien.
*/
template <uint8_t dimension>
std::ostream &operator<<(std::ostream &stream, const rotationMatrixMD<dimension> &arg) {
  for (uint8_t i = 0; i < dimension; ++i) {
    for (uint8_t j = 0; j < dimension; ++j)
      stream << ": [" << i + 1 << "][" << j + 1 << "] = " << arg[i][j] << std::endl;
    stream << "|" << std::endl;
  }
  return stream;
}

template <uint8_t dimension>
rotationMatrixMD<dimension>::rotationMatrixMD() {
  for (uint8_t i = 0; i < dimension; ++i)
    rows[i][i] = 1;
}

template <uint8_t dimension>
rotationMatrixMD<dimension>::rotationMatrixMD(double angle, vectorMD<dimension> axis) {
  throw std::logic_error("obecnie klasa rotationMatrixMD obsluguje wylacznie obroty w 3 wymiarach (obroty w 2 wymiarach mozna zrealizowac posrednio w 3 wymiarach)");
}

template <>
inline rotationMatrixMD<3>::rotationMatrixMD(double angle, vectorMD<3> axis) {
  if (axis[0] == 0 && axis[1] == 0 && axis[2] == 0)
    throw std::logic_error("os obrotu nie moze byc wektorem zerowym");
  angle *= M_PIf64 / 180;   // zamiana stopni na radiany
  axis = axis.normalized(); // unormowanie (przeksztalcenie w wersor)
  double x = axis[0], y = axis[1], z = axis[2], s = sin(angle), c = cos(angle);
  // wyprowadzenie z rozdzialu 2.3.3 https://en.wikipedia.org/wiki/Rotation_matrix
  vectorMD<3> row1({(c + pow(x, 2) * (1 - c)), (x * y * (1 - c) - z * s), (x * z * (1 - c) + y * s)});
  vectorMD<3> row2({(y * x * (1 - c) + z * s), (c + pow(y, 2) * (1 - c)), (y * z * (1 - c) - x * s)});
  vectorMD<3> row3({(z * x * (1 - c) - y * s), (z * y * (1 - c) + x * s), (c + pow(z, 2) * (1 - c))});
  rows = {row1, row2, row3};
}

template <uint8_t dimension>
const vectorMD<dimension> &rotationMatrixMD<dimension>::operator[](uint8_t n) const {
  if (n > dimension - 1)
    throw std::logic_error("proba dostepu do nieistniejacego wiersza macierzy");
  return rows[n];
}

template <uint8_t dimension>
vectorMD<dimension> rotationMatrixMD<dimension>::operator*(const vectorMD<dimension> &arg) const {
  vectorMD<dimension> result;
  for (uint8_t i = 0; i < dimension; ++i)
    result[i] = rows[i] * arg;
  return result;
}

template <uint8_t dimension>
rotationMatrixMD<dimension> rotationMatrixMD<dimension>::operator*(const rotationMatrixMD<dimension> &arg) const {
  rotationMatrixMD result, temp;
  temp = arg.transpose();
  for (uint8_t i = 0; i < dimension; ++i)
    for (uint8_t j = 0; j < dimension; ++j)
      result.rows[i][j] = rows[i] * temp[j];
  return result;
}

template <uint8_t dimension>
rotationMatrixMD<dimension> rotationMatrixMD<dimension>::transpose() const {
  rotationMatrixMD result;
  for (uint8_t i = 0; i < dimension; ++i)
    for (uint8_t j = 0; j < dimension; ++j)
      result.rows[i][j] = rows[j][i];
  return result;
}

#endif

#ifndef PLATEAU_HPP
#define PLATEAU_HPP

#include "coordinateSystem.hpp"
#include "drawingInterface.hpp"
#include "landmarkInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie plaskowyzu.
*/
class plateau : public coordinateSystem, public drawingInterface, public landmarkInterface {

protected:
  std::vector<vectorMD<3>> vertices; // Wierzcholki plaskowyzu.
  double min;                        // Minimalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
  double max;                        // Maksymalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
  double height;                     // Wysokosc wzgledem poziomu zerowego (w osi Z).

public:
  /**
   * @brief Konstruktor inicjalizujacy interfejs rysowania, poszczegolne wymiary i kolor powierzchni plaskiej.
   * @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
   * @param centroid Wektor trojwymiarowy reprezentujacy srodek plaskowyzu.
   * @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje plaskowyzu.
   * @param min Minimalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
   * @param max Maksymalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
   * @param height Wysokosc wzgledem poziomu zerowego (w osi Z).
   * @param color Kolor plaskowyzu. 
	*/
  plateau(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double min,
      double max,
      double height,
      std::string color = "black");

  /**
   * @brief Destruktor umozliwiajacy zmazanie plaskowyzu wraz z jego usunieciem.
  */
  ~plateau() { erase(); api->redraw(); }

  // Metody wirtualne drawingInterface

  void draw() override;
  void erase() override { api->erase_shape(id); }
  void rescale(const double &scale) override {}

  // Metody wirtualne landmarkInterface

  bool is_above(std::shared_ptr<droneInterface> drone) override;
  bool can_land(std::shared_ptr<droneInterface> drone) override;
};

#endif
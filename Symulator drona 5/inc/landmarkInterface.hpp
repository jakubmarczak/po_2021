#ifndef LANDMARKINTERFACE_HPP
#define LANDMARKINTERFACE_HPP

#include "droneInterface.hpp"

/**
 * @brief Klasa modelujaca interfejs elementu krajobrazu.
*/
class landmarkInterface {

public:

  /**
   * @brief Domyslny destruktor wirtualny.
  */
  virtual ~landmarkInterface() {}

  /**
   * @brief Metoda wirtualna realizujaca sprawdzenie, czy dron znajduje sie nad elementem krajobrazu.
   * @param drone Wskaznik wspoldzielony na interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
   * @return Wartosci 'true' lub 'false' odpowiednio, gdy dron znajduje lub nie znajduje sie nad elementem krajobrazu.
  */
  virtual bool is_above(std::shared_ptr<droneInterface> drone) = 0;

  /**
   * @brief Metoda wirtualna realizujaca sprawdzenie, czy dron moze wyladowac/opasc na element krajobrazu.
   * @param drone Wskaznik wspoldzielony na interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
   * @return Wartosci 'true' lub 'false' odpowiednio, gdy dron moze lub nie moze opasc na dana wysokosc.
  */
  virtual bool can_land(std::shared_ptr<droneInterface> drone) = 0;
};

#endif

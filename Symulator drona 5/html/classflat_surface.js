var classflat_surface =
[
    [ "flatSurface", "classflat_surface.html#a0a6c71ef14ebc738409fee0b0b379e27", null ],
    [ "~flatSurface", "classflat_surface.html#af5f725c65487493f8f0308654370ac08", null ],
    [ "draw", "classflat_surface.html#a80cf805d99209a8bcf050926825f46a9", null ],
    [ "erase", "classflat_surface.html#a4e4e5ea9e2e0142bdd6ab020a9b1811e", null ],
    [ "rescale", "classflat_surface.html#aa1ecf0c171d740150d3eebc284212dd3", null ],
    [ "height", "classflat_surface.html#ab708fbb7e6896977e52c87781657ddfd", null ],
    [ "length", "classflat_surface.html#aaa72d5a240ff174392e3811d7e25491a", null ],
    [ "scale", "classflat_surface.html#aa95f9678ed8ac3e1011fc30063c99ed6", null ],
    [ "width", "classflat_surface.html#a0bda17da686154b3f49096ac6baf696e", null ]
];
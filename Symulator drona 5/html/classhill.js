var classhill =
[
    [ "hill", "classhill.html#a1fd4f63a53376bbda90fc40ef1d89fed", null ],
    [ "~hill", "classhill.html#aac157baa162b12f28900ff88ed6e04c2", null ],
    [ "can_land", "classhill.html#a8e6fabbfb2391a236d4a5a98e8e666db", null ],
    [ "draw", "classhill.html#a951d8569d1e85787c726899077761b3b", null ],
    [ "erase", "classhill.html#aaff34060f661febbeb761e981e014c58", null ],
    [ "is_above", "classhill.html#aefa38b166ecebb7c8e0a47cb18fcaa20", null ],
    [ "rescale", "classhill.html#ad240131dc679ca706c4f4ed319bafc9d", null ],
    [ "height", "classhill.html#a31b4a11babb4c276ac5c5bbbc4d2d200", null ],
    [ "max", "classhill.html#a668c434f141ce58b3eed97f83f8fc22a", null ],
    [ "min", "classhill.html#a6efb2a9c3781f4a431cf82ad7f95024c", null ],
    [ "vertices", "classhill.html#adae2f182bff33bfa7cf5a69d628abefd", null ]
];
var classdrawing_interface =
[
    [ "drawingInterface", "classdrawing_interface.html#a15c59d1ee28f27d642280d0570409ade", null ],
    [ "~drawingInterface", "classdrawing_interface.html#a95f5d9f823fcb33e8c937e9c198a9cfc", null ],
    [ "draw", "classdrawing_interface.html#ae7e21e9fdfa4b661f845548305743268", null ],
    [ "erase", "classdrawing_interface.html#a3a762591b45c766ce1764661e67d993d", null ],
    [ "get_id", "classdrawing_interface.html#ae3d6d6e223fe949a321cb7de8bac88e1", null ],
    [ "refresh", "classdrawing_interface.html#ad097f2ca9cfcf8bceb5489e75e3f754e", null ],
    [ "rescale", "classdrawing_interface.html#ac65466a05e2cf655be8dad2b28204cd8", null ],
    [ "api", "classdrawing_interface.html#a9681d29015cd4330373a34a591821aaa", null ],
    [ "color", "classdrawing_interface.html#ac8a368b4201dd9ee107ef688794f5f88", null ],
    [ "id", "classdrawing_interface.html#a6656e6eff6190a547b3b500f35ad4922", null ]
];
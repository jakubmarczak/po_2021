var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstvw~",
  1: "abcdfhlmprsv",
  2: "d",
  3: "bcdfhlmprsv",
  4: "abcdefghilmnopqrstv~",
  5: "_abcdghiklmoprstvw",
  6: "s",
  7: "_"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Macros"
};


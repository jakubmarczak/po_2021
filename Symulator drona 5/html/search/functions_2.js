var searchData=
[
  ['can_5fland_239',['can_land',['../classbasic_drone.html#a4ac4f43390831bb7066950ed70dee6d9',1,'basicDrone::can_land()'],['../classcuboidal_plateau.html#a2588eb0e279272d49b3d44646736a3c4',1,'cuboidalPlateau::can_land()'],['../classhexagonal_drone.html#aae4381b562f88e1451f4d4ace3741c68',1,'hexagonalDrone::can_land()'],['../classhill.html#a8e6fabbfb2391a236d4a5a98e8e666db',1,'hill::can_land()'],['../classlandmark_interface.html#a165967069481550c1edcb4916b24d092',1,'landmarkInterface::can_land()'],['../classplateau.html#aae7b070854ff083fa6279b8c41b9b6e3',1,'plateau::can_land()']]],
  ['change_5fref_5ftime_5fms_240',['change_ref_time_ms',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#aa7da7ed9eaaea392a5710143fda0da67',1,'drawNS::APIGnuPlot3D::change_ref_time_ms()'],['../classdraw_n_s_1_1_draw3_d_a_p_i.html#a260616064efb475e27c24c1b0ffa307e',1,'drawNS::Draw3DAPI::change_ref_time_ms()']]],
  ['change_5fshape_5fcolor_241',['change_shape_color',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#ac33b91c7e171909c6ae2fcfbf7b915b1',1,'drawNS::APIGnuPlot3D::change_shape_color()'],['../classdraw_n_s_1_1_draw3_d_a_p_i.html#a8caeca726076c2479a2505742ecc7b1e',1,'drawNS::Draw3DAPI::change_shape_color()']]],
  ['check_242',['check',['../main_8cpp.html#a221523bba1fa7a62c86d10db66658069',1,'main.cpp']]],
  ['check_5fcollision_243',['check_collision',['../classscene.html#a1e196a12c8c36c69011da6c0d433d18e',1,'scene']]],
  ['converttoglobal_244',['convertToGlobal',['../classcoordinate_system.html#a9d9e3640b9bacb26d8c0f41b8e492cdd',1,'coordinateSystem']]],
  ['converttoglobalpoint3d_245',['convertToGlobalPoint3D',['../classcoordinate_system.html#a1e0b0449493f309511fd2f30b8b05a1a',1,'coordinateSystem']]],
  ['converttoparent_246',['convertToParent',['../classcoordinate_system.html#a37bd57d5cedd3a8b465642b3c18a4ae6',1,'coordinateSystem']]],
  ['coordinatesystem_247',['coordinateSystem',['../classcoordinate_system.html#ab78b484084905cbca77d173ec1502899',1,'coordinateSystem::coordinateSystem()'],['../classcoordinate_system.html#ae1ba8ab402889472c4117378fa5ed36b',1,'coordinateSystem::coordinateSystem(vectorMD&lt; 3 &gt; centroid, rotationMatrixMD&lt; 3 &gt; orientation, coordinateSystem *parent=nullptr)']]],
  ['created_248',['created',['../classvector_m_d.html#a95d321914c4a0bd94cf1c3a4866a278a',1,'vectorMD']]],
  ['cuboid_249',['cuboid',['../classcuboid.html#af14793cc08af1cc90f92da6c7c827164',1,'cuboid::cuboid()'],['../classcuboid.html#a5754e547358a0ae49020e7633c77c230',1,'cuboid::cuboid(std::shared_ptr&lt; drawNS::Draw3DAPI &gt; api, vectorMD&lt; 3 &gt; centroid, rotationMatrixMD&lt; 3 &gt; orientation, coordinateSystem *parent, double width, double length, double height, std::string color=&quot;black&quot;)']]],
  ['cuboidalplateau_250',['cuboidalPlateau',['../classcuboidal_plateau.html#aa81f97a99a1cf753136fd160ddbcb3e1',1,'cuboidalPlateau']]]
];

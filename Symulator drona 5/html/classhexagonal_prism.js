var classhexagonal_prism =
[
    [ "hexagonalPrism", "classhexagonal_prism.html#ad1cf2ec44f0d0e75d2683116402ed6c7", null ],
    [ "~hexagonalPrism", "classhexagonal_prism.html#a862ceb452b089a6a981433527c6b4fdd", null ],
    [ "describe", "classhexagonal_prism.html#aaf3820a9353cde54769cf94058cedb7f", null ],
    [ "draw", "classhexagonal_prism.html#ae1fbca5491bfd9a606abfbf099f3385e", null ],
    [ "erase", "classhexagonal_prism.html#ac0c3725242258db0415e40231fc705d3", null ],
    [ "get_height", "classhexagonal_prism.html#a3b8ca7205eaa4399d5cfa840a5bd738f", null ],
    [ "get_radius", "classhexagonal_prism.html#a578c89411647ceec041e33c8de400066", null ],
    [ "rescale", "classhexagonal_prism.html#a9389cc6a3728ce078ebf86436777c919", null ],
    [ "height", "classhexagonal_prism.html#a0c362c4e3bba24693396bf5c103fac70", null ],
    [ "radius", "classhexagonal_prism.html#a16aef1ce9a2504383a70efeb180b7633", null ]
];
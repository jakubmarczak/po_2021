#ifndef HEXAGONALPRISM_HPP
#define HEXAGONALPRISM_HPP

#include <string>

#include "coordinateSystem.hpp"
#include "Dr3D_gnuplot_api.hpp"
#include "rotationMatrixMD.hpp"

/**
  @brief Klasa modelujaca pojecie graniastoslupa prawidlowego szesciokatnego.
*/
class hexagonalPrism : public coordinateSystem {
  uint id;           // ID graniastoslupa.
  std::string color; // Kolor graniastoslupa.
  double radius;     // Promien okregu opisanego na podstawie graniastoslupa - szesciokacie foremnym.
  double height;     // Wysokosc graniastoslupa (w osi Y).

public:
  /**
    @brief Konstruktor inicjalizujacy uklad wspolrzednych i poszczegolne wymiary graniastoslupa prawidlowego szesciokatnego.
    @param centroid Wektor trojwymiarowy reprezentujacy srodek graniastoslupa.
    @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje graniastoslupa.
    @param parent Poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
    @param radius Promien okregu opisanego na podstawie graniastoslupa - szesciokacie foremnym.
    @param height Wysokosc graniastoslupa (w osi Y).
	*/
  hexagonalPrism(vectorMD<3> centroid, rotationMatrixMD<3> orientation, coordinateSystem *parent, double radius, double height, std::string color="black")
      : coordinateSystem(centroid, orientation, parent), color(color), radius(radius), height(height){};

  /**
    @brief Metoda realizujaca rysowanie graniastoslupa prawidlowego szesciokatnego w programie gnuplot za pomoca dostarczonego api.
    @param api wskaznik na obslugiwane api do gnuplota.
	*/
  uint draw(std::shared_ptr<drawNS::Draw3DAPI> api);
};

#endif

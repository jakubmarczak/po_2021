#include "surface.hpp"

uint surface::draw(std::shared_ptr<drawNS::Draw3DAPI> api) {
  std::vector<std::vector<drawNS::Point3D>> major; // powierzchnia
  std::vector<drawNS::Point3D> minor;              // kolejne "paski" z punktami

  for (uint8_t i = 0; i < width + 1; ++i) {
    minor.clear();
    for (uint8_t j = 0; j < length + 1; ++j)
      minor.push_back(drawNS::Point3D(scale * (i - 0.5 * width), 0, scale * (j - 0.5 * length)));
    major.push_back(minor);
  }

  // rysowanie powierzchni
  id = api->draw_surface(major);
  api->redraw();
  return id;
}
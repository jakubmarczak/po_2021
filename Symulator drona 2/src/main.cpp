#include <array>
#include <cmath>
#include <iomanip>
#include <iostream>

#include <limits>
#include <string>
#include <vector>

// do menu
#include <ncurses.h>

// do funkcji pokroju sleep()
#include <chrono>
#include <thread>

#include "Dr3D_gnuplot_api.hpp"
#include "coordinateSystem.hpp"
#include "drone.hpp"
#include "menu.hpp"
#include "surface.hpp"

using drawNS::APIGnuPlot3D;
using drawNS::Point3D;
using std::cin;
using std::cout;
using std::endl;
using std::numeric_limits;
using std::streamsize;

/**
  @brief Funkcja realizujaca czyszczenie sceny gnuplota z ksztaltow zapisanych w wektorze.
  @param api api do gnuplota.
  @param shapes wektor przechowujacy id ksztaltow do usuniecia.
*/
void clean(std::shared_ptr<drawNS::Draw3DAPI> api, std::vector<uint> shapes) {
  for (size_t i = 0; i < shapes.size(); ++i)
    api->erase_shape(shapes[i]);
}

/**
  @brief Funkcja realizujaca "zatrzymanie" programu na okres czasu przekazany w argumencie.
  @param period Okres czasu, podany w milisekundach, na ktory "zatrzymywany" jest program.
*/
void wait(int period) {
  std::this_thread::sleep_for(std::chrono::milliseconds(period));
}

/**
  @brief Funkcja realizujaca wyswietlanie ilosci stworzonych i istniejacych wektorow.
*/
void vectorCounters() {
  
}

int main() {
  // menu
  std::vector<std::string> menu_options, menu_border;                         // vectory przechowujace: nazwy pozycji i tekst ramki menu
  attr_t menu_attr = A_REVERSE;                                               // atrybut ncurses przypisywany do zaznaczonej pozycji
  uint8_t menu_choice;                                                        // numer pozycji wybranej przyciskiem enter
  menu_options.push_back("Test animacji dronow");                             // pozycja 1
  menu_options.push_back("Test rysowania powierzchni plaskich");              // pozycja 2
  menu_options.push_back("Zakoncz");                                          // pozycja 2
  menu_border.push_back(" Symulator drona ");                                 // tytul projektu
  menu_border.push_back(" Jakub Marczak 259319 ");                            // autor
  menu_border.push_back(" sterowanie: strzalki / klawisze W,A,S,D,Q,ENTER "); // sterowanie

  // gnuplot
  std::vector<uint> shapes, temp; // wektory przechowujacy id ksztaltow do usuniecia
  uint id;                        // ID ksztaltu do usuniecia

  try {
    // inicjalizacja menu
    menu _menu(menu_options, menu_border, menu_attr);

    // scena [-10,10]x[0,20]x[-10,10] odswiezana za pomoca metody redraw
    std::shared_ptr<drawNS::Draw3DAPI> api(new APIGnuPlot3D(-10, 10, 0, 20, -10, 10, -1));

    while (menu_choice != menu_options.size()) // ostatnia pozycja - zakoncz
    {
      menu_choice = _menu.run();

      switch (menu_choice) {
        case 1: { // test animacji dronow
          // definicje dronow
          std::array<drone, 4> drones{
              drone(vectorMD<3>({-3, 0, 3}), rotationMatrixMD<3>(), 1.5, 0.5, 2.5, 0.5, "red"),
              drone(vectorMD<3>({-3, 0, -3}), rotationMatrixMD<3>(), 2, 1, 3, 0.7, "orange"),
              drone(vectorMD<3>({3, 0, 3}), rotationMatrixMD<3>(), 1, 0.5, 4.5, 0.4, "green"),
              drone(vectorMD<3>({3, 0, -3}), rotationMatrixMD<3>(), 1.5, 0.5, 1.5, 0.5, "blue")};

          clean(api, shapes);

          // rysowanie powierzchni
          id = surface(5, 0, 5, 4).draw(api);

          // rysowanie dronow
          for (drone &i : drones) {
            temp = i.draw(api);
            shapes.insert(shapes.end(), temp.begin(), temp.end());
          }
          wait(2000);
          clean(api, shapes);

          // animacja ruchow drona 1
          drones[0].animate(api, 0, 1, 0, 85);
          drones[0].animate(api, -5, 0, 0, 85);
          drones[0].animate(api, 0, 10, 180, 60);
          drones[0].doABarellRoll(api, -20, 20, 70);
          drones[0].animate(api, 0, -10, 0, 30);
          drones[0].animate(api, -5, 0, 0, 85);
          drones[0].animate(api, 0, -1, 0, 85);
          temp = drones[0].draw(api);
          shapes.insert(shapes.end(), temp.begin(), temp.end());

          // animacja ruchow drona 2
          drones[1].animate(api, 0, 5, 180, 70);
          drones[1].animate(api, 0, -5, 180, 85);
          temp = drones[1].draw(api);
          shapes.insert(shapes.end(), temp.begin(), temp.end());

          // animacja ruchow drona 3
          drones[2].animate(api, 0, 5, 90, 80);
          drones[2].animate(api, 0, 0, 90, 80);
          drones[2].animate(api, 3, 0, 0, 80);
          drones[2].animate(api, 0, 0, 180, 80);
          drones[2].animate(api, 3, 0, 0, 80);
          drones[2].animate(api, 0, -5, -360, 80);
          temp = drones[2].draw(api);
          shapes.insert(shapes.end(), temp.begin(), temp.end());

          // animacja ruchow drona 4
          drones[3].animate(api, 4, 4, 90, 80);
          drones[3].animate(api, 4, -4, 90, 65);
          drones[3].animate(api, 4, 4, 90, 80);
          drones[3].animate(api, 4, -4, 90, 65);
          temp = drones[3].draw(api);
          shapes.insert(shapes.end(), temp.begin(), temp.end());

          shapes.push_back(id);
          clean(api, shapes);
          _menu.pressEnter();
          break;
        }
        case 2: { // test rysowania powierzchni plaskich
          std::vector<uint> surfaces;
          for (int i = 0; i < 100; ++i) {
            shapes.push_back(surface(0.1 * i , 0, 10, 1).draw(api));
            clean(api, shapes);
            wait(25);
          }
          for (int i = 0; i < 100; ++i) {
            shapes.push_back(surface(10 , 0, 0.1 * i, 1).draw(api));
            clean(api, shapes);
            wait(25);
          }
          for (int i = 0; i < 10; ++i) {
            shapes.push_back(surface(10 , 0, 10, 0.2*i+1).draw(api));
            clean(api, shapes);
            wait(250);
          }
          _menu.pressEnter();
          break;
        }
        default:
          break;
      }
    }
  } catch (std::logic_error &error) {
    cout << "# Blad: " << error.what() << "." << endl;
  }
}
#include <vector>

#include "cuboid.hpp"

uint cuboid::draw(std::shared_ptr<drawNS::Draw3DAPI> api) {
  std::vector<drawNS::Point3D> base1, base2; // podstawy

  // 1 podstawa
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, -0.5 * height, -0.5 * length})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, -0.5 * height, 0.5 * length})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, -0.5 * height, 0.5 * length})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, -0.5 * height, -0.5 * length})));

  // 2 podstawa
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, 0.5 * height, -0.5 * length})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, 0.5 * height, 0.5 * length})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, 0.5 * height, 0.5 * length})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, 0.5 * height, -0.5 * length})));

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base1, base2}, color);
  return id;
}
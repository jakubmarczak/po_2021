#include <chrono>
#include <thread>
#include <vector>

#include "Dr3D_gnuplot_api.hpp"
#include "cuboid.hpp"
#include "drone.hpp"
#include "hexagonalPrism.hpp"

drone::drone(vectorMD<3> centeroid, rotationMatrixMD<3> orientation, double width, double height, double length, double radius, std::string color)
    : coordinateSystem(centeroid, orientation, nullptr),
      body(vectorMD<3>({0, 0.5 * height, 0}), rotationMatrixMD<3>(), this, width, height, length, color),
      propellers{hexagonalPrism(vectorMD<3>({0.5 * width, height + 0.2 * radius, -0.5 * length}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
                 hexagonalPrism(vectorMD<3>({0.5 * width, height + 0.2 * radius, 0.5 * length}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
                 hexagonalPrism(vectorMD<3>({-0.5 * width, height + 0.2 * radius, 0.5 * length}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
                 hexagonalPrism(vectorMD<3>({-0.5 * width, height + 0.2 * radius, -0.5 * length}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color)} {}

void drone::move_horizontally(const double &distance) { translate(orientation * vectorMD<3>({0, 0, distance})); }

void drone::move_vertically(const double &height) { translate(orientation * vectorMD<3>({0, height, 0})); }

void drone::spin(const double &angle) { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({0, 1, 0}))); }

void drone::spin_propellers(const double &angle) {
  rotationMatrixMD<3> right(angle, vectorMD<3>({0, 1, 0}));
  rotationMatrixMD<3> left = right.transpose();

  propellers[0].rotate(right); // obrot w prawo
  propellers[1].rotate(left);  // obrot w lewo
  propellers[2].rotate(right);
  propellers[3].rotate(left);
}

void drone::animate(std::shared_ptr<drawNS::Draw3DAPI> api, const double &distance, const double &height, const double &angle, uint speed) {
  std::vector<uint> shapes, temp;

  // normalizacja predkosci
  if (speed > 100)
    speed = 100;

  for (uint8_t i = 0; i < 100 - speed; ++i) {
    // przesuniecia drona w pionie i poziomie
    move_vertically(height / (100 - speed));
    move_horizontally(distance / (100 - speed));

    // rotacja drona wokol i wirnikow wokol osi Y
    spin(angle / (100 - speed));
    spin_propellers(360 / (100 - speed));

    // rysowanie drona
    temp = draw(api);
    shapes.insert(shapes.begin(), temp.begin(), temp.end());

    // usuwanie "klatki"
    for (size_t i = 0; i < shapes.size(); ++i)
      api->erase_shape(shapes[i]);
      
    // 50ms przed kolejna klatka
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }
}

void drone::doABarellRoll(std::shared_ptr<drawNS::Draw3DAPI> api, const double &distance, const double &height, uint speed) {
  std::vector<uint> shapes, temp;

  // normalizacja predkosci
  if (speed > 100)
    speed = 100;

  for (uint8_t i = 0; i < 100 - speed; ++i) {
    // przesuniecia drona w pionie i poziomie
    move_horizontally(distance / (100 - speed));
    move_vertically(height / (100 - speed));

    // rotacja drona wokol wokol osi X i wirnikow wokol osi Y
    rotate(rotationMatrixMD<3>(360 / (100 - speed), vectorMD<3>({1, 0, 0})));
    spin_propellers(360 / (100 - speed));

    // rysowanie drona
    temp = draw(api);
    shapes.insert(shapes.begin(), temp.begin(), temp.end());

    // usuwanie "klatki"
    for (size_t i = 0; i < shapes.size(); ++i)
      api->erase_shape(shapes[i]);

    // 50ms przed kolejna klatka
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }
}

std::vector<uint> drone::draw(std::shared_ptr<drawNS::Draw3DAPI> api) {
  std::vector<uint> shapes;

  // rysowanie korpusu
  shapes.push_back(body.draw(api));

  // rysowanie wirnikow
  for (hexagonalPrism &i : propellers)
    shapes.push_back(i.draw(api));

  api->redraw();
  return shapes;
}
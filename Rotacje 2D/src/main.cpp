#include <iostream>
#include <ncurses.h>
#include <iomanip>
#include <string>
#include <fstream>
#include <limits>
#include <cmath>

#include "Dr2D_gnuplot_api.hpp"
#include "menu.hpp"
#include "rectangle.hpp"
#include "rotationMatrix2D.hpp"
#include "vector2D.hpp"

using drawNS::APIGnuPlot2D;
using drawNS::Point2D;
using std::cin;
using std::cout;
using std::endl;

/**
  @brief Funkcja realizujaca czyszczenie sceny gnuplota z ksztaltow zapisanych w wektorze.
  @param api api do gnuplota.
  @param shapes wektor przechowujacy id ksztaltow do usuniecia.
*/
void clean(std::shared_ptr<drawNS::Draw2DAPI> api, std::vector<int> shapes)
{
  for (int &x : shapes)
    api->erase_shape(x);
}

int main()
{
  // menu
  std::vector<std::string> options, borderText; // wektory przechowujace: nazwy pozycji i tekst ramki menu
  attr_t attr = A_REVERSE;                      // atrybut ncurses przypisywany do zaznaczonej pozycji
  uint8_t choice;                               // numer pozycji wybranej przyciskiem enter
  // pozycje menu
  options.push_back("Obrot prostokata");                        // 1
  options.push_back("Translacja prostokata");                   // 2
  options.push_back("Wyswietlenie prostokata");                 // 3
  options.push_back("Wyswietlenie wspolrzednych wierzcholkow"); // 4
  options.push_back("Zakoncz");                                 // 5
  // tekst ramki
  borderText.push_back(" Rotacje 2D ");                                      // tytul projektu
  borderText.push_back(" Jakub Marczak 259319 ");                            // autor
  borderText.push_back(" sterowanie: strzalki / klawisze W,A,S,D,Q,ENTER "); // sterowanie

  // gnuplot
  std::vector<int> shapes;     // wektor przechowujacy id ksztaltow do usuniecia
  std::array<double, 4> sides; // array przechowujacy dlugosci poszczegolnych bokow prostokata

  // prostokat
  int n;                                                   // ilosc powtorzen operacji obrotu
  double temp;                                             // zmienna tymczasowa przechowujaca kat obrotu, wspolrzedne wektora translacji
  double epsilon = std::numeric_limits<double>::epsilon(); // epsilon do porownywania dlugosci bokow

  try
  {
    // odswiezanie z pojawieniem sie nowego ksztaltu (0) generowalo komunikaty o bledzie po wywolywaniu funkcji clean
    std::shared_ptr<drawNS::Draw2DAPI> api(new APIGnuPlot2D(-25, 25, -25, 25, -1)); // scena [-25,25]x[-25,25] odswiezana za pomoca metody redraw
    menu _menu(options, borderText, attr);
    rectangle _rectangle(vector2D(0, 0), vector2D(10, 0), vector2D(10, 5), vector2D(0, 5)); // prostokat bazowy
    cout << std::fixed << std::setprecision(30);                                            // ustawienie precyzji wyswietlania
    while (choice != options.size())                                                        // ostatnia pozycja - zakoncz
    {
      choice = _menu.run();
      switch (choice)
      {
      case 1: // rotacja prostokata o zadany kat zadana ilosc razy
      {
        clean(api, shapes); // wyczyszczenie sceny
        cout << endl
             << "| Kat obrotu: ";
        cin >> temp;
        cout << "| Ilosc powtorzen: ";
        cin >> n;
        _rectangle.rotate(n * temp); // rotacja

        shapes.push_back(_rectangle.draw(api)); // rysowanie obroconego prostokata i zapis id ksztaltu do wektora
        api->redraw();                          // odswiezenie sceny
        cout << endl;

        // obliczenie dlugosci bokow prostokata (numeracja bokow od punktu (0,0) w kierunku przeciwnym do ruchu wskazowek zegara)
        for (uint8_t i = 0; i < 3; ++i)
          sides[i] = (_rectangle[i + 1] - _rectangle[i]).length();
        sides[3] = (_rectangle[0] - _rectangle[3]).length();

        // sprawdzenie dlugosci przeciwleglych bokow
        if (sides[0] > sides[1])
        {
          // dluzsze boki
          if (fabs(sides[0] - sides[2]) > epsilon)
            cout << "| Dluzsze przeciwlegle boki nie sa rowne!" << endl;
          else
            cout << "| Dluzsze przeciwlegle boki sa rowne." << endl;
          cout << "| > dlugosc pierwszego boku: " << sides[0] << endl
               << "| > dlugosc drugiego boku: " << sides[2] << endl
               << endl;
          // krotsze boki
          if (fabs(sides[1] - sides[3]) > epsilon)
            cout << "| Krotsze przeciwlegle boki nie sa rowne!" << endl;
          else
            cout << "| Krotsze przeciwlegle boki sa rowne." << endl;
          cout << "| > dlugosc pierwszego boku: " << sides[1] << endl
               << "| > dlugosc drugiego boku: " << sides[3] << endl;
        }
        else
        {
          // dluzsze boki
          if (fabs(sides[1] - sides[3]) > epsilon)
            cout << "| Dluzsze przeciwlegle boki nie sa rowne!" << endl;
          else
            cout << "| Dluzsze przeciwlegle boki sa rowne." << endl;
          cout << "| > dlugosc pierwszego boku: " << sides[1] << endl
               << "| > dlugosc drugiego boku: " << sides[3] << endl
               << endl;
          // krotsze boki
          if (fabs(sides[0] - sides[2]) > epsilon)
            cout << "| Krotsze przeciwlegle boki nie sa rowne!" << endl;
          else
            cout << "| Krotsze przeciwlegle boki sa rowne." << endl;
          cout << "| > dlugosc pierwszego boku: " << sides[0] << endl
               << "| > dlugosc drugiego boku: " << sides[2] << endl;
        }
        _menu.pressEnter();
        break;
      }
      case 2: // translacja prostokata o zadany wektor
      {
        clean(api, shapes);

        cout << endl
             << "| Wektor translacji = [x, y]" << endl
             << "| x = ";
        cin >> temp;
        // podzielenie translacji na 2 etapy w celu redukcji ilosci zmiennych (nie zmienia to wyniku)
        _rectangle.translate(vector2D(temp, 0));

        cout << "| y = ";
        cin >> temp;
        _rectangle.translate(vector2D(0, temp));

        shapes.push_back(_rectangle.draw(api));
        api->redraw();
        _menu.pressEnter();
        break;
      }
      case 3: // wyswietlenie prostokata
      {
        clean(api, shapes);
        shapes.push_back(_rectangle.draw(api));
        api->redraw();
        _menu.pressEnter();
        break;
      }
      case 4: // wyswietlenie wspolrzednych wierzcholkow
      {
        cout << endl
             << "| Wspolrzedne wierzcholkow prostokata: " << endl;
        cout << "| W1 = [" << _rectangle[0][0] << ", " << _rectangle[0][1] << "]" << endl;
        cout << "| W2 = [" << _rectangle[1][0] << ", " << _rectangle[1][1] << "]" << endl;
        cout << "| W3 = [" << _rectangle[2][0] << ", " << _rectangle[2][1] << "]" << endl;
        cout << "| W4 = [" << _rectangle[3][0] << ", " << _rectangle[3][1] << "]" << endl;
        _menu.pressEnter();
        break;
      }
      default:
        break;
      }
    }
  }

  catch (std::logic_error &error)
  {
    cout << error.what() << endl;
  }
}

#include <iostream>
#include <vector>
#include <cmath>
#include <limits>
#include "rectangle.hpp"
#include "rotationMatrix2D.hpp"

rectangle::rectangle(vector2D v1, vector2D v2, vector2D v3, vector2D v4)
{
  double epsilon = std::numeric_limits<double>::epsilon();
  // sprawdzenie prostopadlosci wzgledem dwoch sasiednich bokow
  if (fabs((v4 - v1) * (v4 - v3)) > epsilon)
    throw std::logic_error("Blad: co najmniej dwa boki prostokata dwuwymiarowego nie sa wzgledem siebie prostopadle.");
  // sprawdzenie dlugosci dwoch przeciwleglych bokow
  if (fabs((v4 - v1).length() - (v3 - v2).length()) > epsilon)
    throw std::logic_error("Blad: co najmniej dwa przeciwlegle boki prostokata maja rozne dlugosci.");
  vertices = {v1, v2, v3, v4};
}

const vector2D &rectangle::operator[](uint8_t n) const
{
  if (n < 0 || n > vertices.size() - 1)
    throw std::logic_error("Blad: proba dostepu do nieistniejacego wierzcholka prostokata dwuwymiarowego.");
  return vertices[n];
}

vector2D &rectangle::operator[](uint8_t n)
{
  if (n < 0 || n > vertices.size() - 1)
    throw std::logic_error("Blad: proba dostepu do nieistniejacego wierzcholka prostokata dwuwymiarowego.");
  return vertices[n];
}

void rectangle::rotate(double angle)
{
  rotationMatrix2D rotation(angle);
  for (vector2D &i : vertices)
    i = rotation * i;
}

void rectangle::translate(vector2D arg)
{
  for (vector2D &i : vertices)
    i = i + arg;
}

int rectangle::draw(std::shared_ptr<drawNS::Draw2DAPI> api)
{
  std::vector<drawNS::Point2D> polygonal_chain;
  for (vector2D &i : vertices)
    polygonal_chain.push_back(drawNS::Point2D(i[0], i[1])); // z jednoczesna konwersja vector2D na Point2D
  polygonal_chain.push_back(drawNS::Point2D(vertices[0][0], vertices[0][1]));
  return api->draw_polygonal_chain(polygonal_chain);
}
#define _USE_MATH_DEFINES
#include <cmath>
#include "rotationMatrix2D.hpp"

rotationMatrix2D::rotationMatrix2D()
{
  vector2D row1(1, 0), row2(0, 1);
  rows = {row1, row2};
}

rotationMatrix2D::rotationMatrix2D(double angle)
{
  angle *= M_PIf32 / 180;
  vector2D row1(std::cos(angle), -std::sin(angle)), row2(std::sin(angle), std::cos(angle));
  rows = {row1, row2};
}

const vector2D &rotationMatrix2D::operator[](uint8_t n) const
{
  if (n < 0 || n > rows.size() - 1)
    throw std::logic_error("Blad: proba dostepu do nieistniejacego wiersza macierzy.");
  return rows[n];
}

rotationMatrix2D rotationMatrix2D::operator*(const rotationMatrix2D &arg) const
{
  rotationMatrix2D result, temp;
  temp = arg.transpose();
  for (uint8_t i = 0; i < rows.size(); ++i)
    for (uint8_t j = 0; j < rows.size(); ++j)
      result.rows[i][j] = rows[i] * temp[j];
  return result;
}

rotationMatrix2D rotationMatrix2D::transpose() const
{
  rotationMatrix2D result;
  for (uint8_t i = 0; i < rows.size(); ++i)
    for (uint8_t j = 0; j < rows.size(); ++j)
      result.rows[i][j] = rows[j][i];
  return result;
}

vector2D rotationMatrix2D::operator*(const vector2D &arg) const
{
  vector2D result;
  for (uint8_t i = 0; i < rows.size(); ++i)
    result[i] = rows[i] * arg;
  return result;
}

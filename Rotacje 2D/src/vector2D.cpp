#include <cmath>
#include "vector2D.hpp"

double &vector2D::operator[](uint8_t n)
{
  if (n < 0 || n > data.size()-1)
    throw std::logic_error("Blad: proba dostepu do nieistniejacego elementu wektora/macierzy.");
  return data[n];
}

const double &vector2D::operator[](uint8_t n) const
{
  if (n < 0 || n > data.size()-1)
    throw std::logic_error("Blad: proba dostepu do nieistniejacego elementu wektora/macierzy.");
  return data[n];
}

vector2D vector2D::operator+(const vector2D &arg) const
{
  vector2D result;
  for (size_t i = 0; i < data.size(); ++i)
    result.data[i] = data[i] + arg.data[i];
  return result;
}

vector2D vector2D::operator-(const vector2D &arg) const
{
  vector2D result;
  for (size_t i = 0; i < data.size(); ++i)
    result.data[i] = data[i] - arg.data[i];
  return result;
}

vector2D vector2D::operator*(double arg) const
{
  vector2D result;
  for (size_t i = 0; i < data.size(); ++i)
    result.data[i] = data[i] * arg;
  return result;
}

const double vector2D::operator*(const vector2D &arg) const
{
  double result=0;
  for (size_t i = 0; i < data.size(); ++i)
    result += (data[i] * arg.data[i]);
  return result;
}

const double vector2D::length()
{
  double result=0;
  for (size_t i = 0; i < data.size(); ++i)
    result += std::pow(data[i], 2);
  return std::sqrt(result);
}

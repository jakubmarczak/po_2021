#ifndef ROTATIONMATRIX2D_HPP
#define ROTATIONMATRIX2D_HPP

#include <iostream>
#include <array>

#include "vector2D.hpp"

/**
  @brief Klasa modelujaca pojecie macierzy obrotu (rotacji) w dwoch wymiarach.
*/
class rotationMatrix2D
{
  std::array<vector2D,2> rows;

public:
  /**
		@brief Konstruktor domyslny tworzacy macierz identycznosciowa (jednostkowa).
		@return Macierz identycznosciowa (jednostkowa).
	*/
  rotationMatrix2D();

  /**
		@brief Konstruktor tworzacy macierz obrotu (rotacji) w dwoch wymiarach o zadany kat.
    @param angle kat obrotu podany w stopniach.
		@return Macierz obrotu (rotacji) w dwoch wymiarach o zadany kat.
	*/
  explicit rotationMatrix2D(double angle);

  /**
		@brief Operator dostepowy dla wierszy macierzy obrotu.
    @param n numer wiersza macierzy obrotu (n>=0).
		@return Wektor bedacy n-tym wierszem macierzy obrotu.
	*/
  const vector2D & operator[](uint8_t n) const;

  /**
		@brief Operator iloczynu macierzy obrotu i wektora.
    @param arg wektor, dla ktorego wykonujemy iloczyn.
		@return Wektor bedacy wynikiem iloczynu.
	*/
  vector2D operator*(const vector2D & arg) const;

  /**
		@brief Operator iloczynu dwoch macierzy obrotu.
    @param arg druga z macierzy obrotu, dla ktorych wykonujemy iloczyn.
		@return Macierz obrotu bedaca wynikiem iloczynu.
	*/
  rotationMatrix2D operator*(const rotationMatrix2D & arg) const;

  /**
		@brief Metoda pozwalajaca na transpozycje macierzy obrotu.
		@return Transponowana macierz obrotu.
	*/
  rotationMatrix2D transpose() const;
};

#endif

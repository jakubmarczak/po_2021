#ifndef VECTOR2D_HPP
#define VECTOR2D_HPP

#include <iostream>
#include <array>
#include "Dr2D_gnuplot_api.hpp"

/**
  @brief Klasa modelujaca pojecie wektora dwuwymiarowego.
*/
class vector2D
{
	std::array<double, 2> data; // Array przechowujacy wspolrzedne wektora dwuwymiarowego.

public:
	/**
		@brief Konstruktor domyslny zerujacy wszystkie wspolrzedne wektora dwuwymiarowego.
	*/
	vector2D() { data.fill(0); }

	/**
		@brief Konstruktor przypisujacy wspolrzednym wektora dwuwymiarowego wartosci przekazane jako argumenty.
		@param x wartosc wspolrzednej x-owej.
		@param y wartosc wspolrzednej y-owej.
	*/
	vector2D(double x, double y) { data = {x, y}; }

	/**
		@brief Operator dostepowy dla wspolrzednych wektora dwuwymiarowego.
		@param n numer wspolrzednej wektora (n>=0, 0=x, 1=y, ...).
		@return Niemodyfikowalna wartosc n-tej wspolrzednej wektora.
	*/
	const double &operator[](uint8_t n) const;

	/**
		@brief Operator dostepowy dla wspolrzednych wektora dwuwymiarowego.
		@param n numer wspolrzednej wektora (n>=0, 0=x, 1=y, ...).
		@return Modyfikowalna wartosc n-tej wspolrzednej wektora.
	*/
	double &operator[](uint8_t n);

	/**
		@brief Operator dodawania wektorow dwuwymiarowych.
		@param arg wektor dwuwymiarowy, ktory chcemy dodac.
		@return Suma dwoch wektorow (nowy wektor).
	*/
	vector2D operator+(const vector2D &arg) const;

	/**
		@brief Operator odejmowania wektorow dwuwymiarowych.
		@param arg wektor dwuwymiarowy, ktory chcemy odjac.
		@return Roznica dwoch wektorow (nowy wektor).
	*/
	vector2D operator-(const vector2D &arg) const;

	/**
		@brief Operator iloczynu wektora dwuwymiarowego i liczby rzeczywistej.
		@param arg liczba rzeczywista, przez ktora chcemy przemnozyc wektor.
		@return Przemnozony wektor.
	*/
	vector2D operator*(double arg) const;

	/**
		@brief Operator iloczynu skalarnego dwoch wektorow dwuwymiarowych.
		@param arg wektor dwuwymiarowy bedacy drugim skladnikiem iloczynu skalarnego.
		@return Liczba rzeczywista - wartosc iloczynu skalarnego.
	*/
	const double operator*(const vector2D &arg) const;

	/**
		@brief Metoda realizujaca obliczenie dlugosci wektora.
		@return Liczba rzeczywista - dlugosc wektora.
	*/
	const double length();
};

#endif
#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

#include <iostream>
#include <vector>
#include "vector2D.hpp"

/**
  @brief Klasa modelujaca pojecie prostokata dwuwymiarowego.
*/
class rectangle
{
  std::array<vector2D, 4> vertices; // Array przechowujacy wektory dwuwymiarowe pelniace role wierzcholkow prostokata.

public:
  /**
		@brief Konstruktor sprawdzajacy, czy tworzony czworokat jest prostokatem oraz przypisujacy jego wierzcholkom wektory dwuwymiarowe przekazane jako argumenty.
		@param v1 pierwszy / "lewy dolny" wierzcholek.
		@param v2 drugi / "prawy dolny" wierzcholek.
    @param v3 trzeci / "prawy gorny" wierzcholek.
    @param v4 czwarty / "lewy gorny" wierzcholek.
	*/
  rectangle(vector2D v1, vector2D v2, vector2D v3, vector2D v4);

  /**
		@brief Operator dostepowy dla wierzcholkow prostokata dwuwymiarowego.
		@param n numer wierzcholka prostokata (n>=0).
		@return Niemodyfikowalny wierzcholek prostokata dwuwymiarowego.
	*/
  const vector2D &operator[](uint8_t n) const;

  /**
		@brief Operator dostepowy dla wierzcholkow prostokata dwuwymiarowego.
		@param n numer wierzcholka prostokata (n>=0).
		@return Modyfikowalny wierzcholek prostokata dwuwymiarowego.
	*/
  vector2D &operator[](uint8_t n);

  /**
		@brief Metoda realizujaca obrot (rotacje) prostokata o dowolny kat dowolna ilosc razy.
    @param angle kat obrotu podany w stopniach.
		@return Prostokat dwuwymiarowy obrocony o zadany kat zadana ilosc razy.
	*/
  void rotate(double angle);

  /**
		@brief Metoda realizujaca przesuniecie (translacje) prostokata o dowolny wektor.
    @param arg wektor, o ktory chcemy przesunac prostokat.
		@return Prostokat przesuniety o zadany wektor.
	*/
  void translate(vector2D arg);
	
  /**
		@brief Metoda realizujaca konwersje wektorow na Point2D i posrednie rysowanie prostokata w programie gnuplot.
    @param api wskaznik na obslugiwane api do gnuplota.
		@return ID ksztaltu wymagane w razie potrzeby pozniejszego usuniecia prostokata.
	*/
  int draw(std::shared_ptr<drawNS::Draw2DAPI> api);
};

#endif

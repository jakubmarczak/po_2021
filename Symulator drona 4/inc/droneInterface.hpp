#ifndef DRONEINTERFACE_HPP
#define DRONEINTERFACE_HPP

/**
 * @brief Klasa modelujaca interfejs drona.
*/
class droneInterface {

public:
  /**
   * @brief Domyslny destruktor wirtualny.
  */
  virtual ~droneInterface() {}

  /**
   * @brief Metoda wirtualna realizujaca wypisanie krotkiego opisu drona zawierajacego jego typ, kolor i obecne wspolrzedne.
   * @param stream Obslugiwany strumien.
  */
  virtual void shortDescription(std::ostream &stream) = 0;

  /**
   * @brief Metoda wirtualna realizujaca wypisanie pelnego opisu drona zawierajacego jego krotki opis oraz opisy korpusu i wirnikow.
   * @param stream Obslugiwany strumien.
  */
  virtual void fullDescription(std::ostream &stream) = 0;

  /**
   * @brief Metoda wirtualna realizujaca przemieszczenie drona w kierunku poziomym/horyzontalnym (domyslnie w osi Y).
   * @param distance Dystans, o jaki zostanie przemieszczony dron.
  */
  virtual void move_horizontally(const double &distance) = 0;

  /**
   * @brief Metoda wirtualna realizujaca przemieszczenie drona w kierunku pionowym/wertykalnym (domyslnie w osi Z).
   * @param distance Dystans, o jaki zostanie przemieszczony dron.
  */
  virtual void move_vertically(const double &distance) = 0;

  /**
   * @brief Metoda wirtualna realizujaca pochylenie drona (domyslnie rotacje wokol osi X).
   * @param angle Kat, o jaki zostanie pochylony dron.
  */
  virtual void tilt(const double &angle) = 0;

  /**
   * @brief Metoda wirtualna realizujaca rotacje drona (domyslnie wokol osi Z).
   * @param angle Kat, o jaki zostanie obrocony dron.
  */
  virtual void spin(const double &angle) = 0;

  /**
   * @brief Metoda wirtualna realizujaca rotacje wirnikow drona (domyslnie wokol osi Z).
   * @param angle Kat, o jaki zostana obrocone wirniki.
  */
  virtual void spin_propellers(const double &angle) = 0;

  /**
   * @brief Metoda wirtualna realizujaca animacje (wizualizacje) przemieszczen i rotacji drona z zadana predkoscia.
   * @param horizontal_distance Dystans, o jaki zostanie przemieszczony animowany dron w kierunku poziomym/horyzontalnym.
   * @param vertical_distance Dystans, o jaki zostanie przemieszczony animowany dron w kierunku pionowym/wertykalnym.
   * @param angle Kat, o jaki zostanie obrocony animowany dron.
   * @param speed Wyrazona w procentach predkosc, z jaka bedzie sie poruszal animowany dron.              
  */
  virtual void animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed) = 0;
};

#endif

#ifndef SCENE_HPP
#define SCENE_HPP

#include <memory>

#include "drawingInterface.hpp"
#include "droneInterface.hpp"
#include "landmarkInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie sceny w programie gnuplot. 
*/
class scene {

protected:
  std::vector<std::shared_ptr<droneInterface>> drones;       // Kolekcja dronow.
  std::vector<std::shared_ptr<landmarkInterface>> landmarks; // Kolekcja elementow krajobrazu.
  std::vector<std::shared_ptr<drawingInterface>> drawables;  // Kolekcja elementow rysowalnych.
  std::shared_ptr<droneInterface> selected_drone;            // Wybrany dron.

public:
  /**
   * @brief Konstruktor domyslny.
  */
  scene() = default;

  /**
   * @brief Metoda dostepowa dla rozmiaru kolekcji elementow rysowalnych.
   * @return Ilosc elementow przechowywanych w kolekcji elementow rysowalnych.
  */
  size_t quantity() { return drawables.size(); }

  /**
   * @brief Metoda realizujaca dodanie drona do sceny.
   * @param drone Wskaznik wspoldzielony na interfejs dodawanego drona.
  */
  void add_drone(std::shared_ptr<droneInterface> drone);

  /**
   * @brief Metoda realizujaca dodanie elementu krajobrazu do sceny.
   * @param drone Wskaznik wspoldzielony na interfejs dodawanego elementu krajobrazu.
  */
  void add_landmark(std::shared_ptr<landmarkInterface> landmark);

  /**
   * @brief Metoda realizujaca usuniecie obiektu sceny.
   * @param id ID obiektu sceny do usuniecia.
  */
  void remove(const uint8_t &id);

  /**
   * @brief Metoda realizujaca usuniecie wszystkich obiektow sceny.
  */
  void remove_all();

  /**
   * @brief Metoda realizujaca "podswietlenie" (mruganie) obiektu sceny.
   * @param id ID obiektu sceny do "podswietlenia".
  */
  void highlight(const uint8_t &id);

  /**
   * @brief Metoda realizujaca wybor drona.
   * @param id ID wybranego drona.
   * @return Wartosc 'true' jesli poprawnie wybrano drona oraz 'false' jesli nie wybrano.
  */
  bool select_drone(const uint8_t &id);

  /**
   * @brief Metoda realizujaca wypisanie krotkiego opisu wybranego drona.
   * @param stream Obslugiwany strumien.
  */
  void describe_drone_short(std::ostream &stream);

  /**
   * @brief Metoda realizujaca wypisanie pelnego opisu wybranego drona.
   * @param stream Obslugiwany strumien.
  */
  void describe_drone_full(std::ostream &stream);

  /**
   * @brief Metoda realizujaca sprawdzanie kolizji drona.
   * @param drone Wskaznik wspoldzielony na interfejs drona, dla ktorego zostanie wykonane sprawdzenie.
  */
  void check_collision(std::shared_ptr<droneInterface> drone);

  /**
    @brief Metoda realizujaca animacje (wizualizacje) przemieszczen i rotacji drona z zadana predkoscia.
    @param horizontal_distance Dystans, o jaki zostanie przemieszczony animowany dron w kierunku poziomym/horyzontalnym.
    @param vertical_distance Dystans, o jaki zostanie przemieszczony animowany dron w kierunku pionowym/wertykalnym.
    @param angle Kat, o jaki zostanie obrocony animowany dron.
    @param speed Wyrazona w procentach predkosc, z jaka bedzie sie poruszal animowany dron.
  */
  void animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed);
};

#endif

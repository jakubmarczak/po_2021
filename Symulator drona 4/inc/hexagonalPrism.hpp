#ifndef HEXAGONALPRISM_HPP
#define HEXAGONALPRISM_HPP

#include "coordinateSystem.hpp"
#include "drawingInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie graniastoslupa prawidlowego szesciokatnego.
*/
class hexagonalPrism : public coordinateSystem, public drawingInterface {
protected:
  double radius; // Promien okregu opisanego na podstawie graniastoslupa - szesciokacie foremnym.
  double height; // Wysokosc graniastoslupa (w osi Z).

public:
  /**
   * @brief Konstruktor inicjalizujacy interfejs rysowania, uklad wspolrzednych i poszczegolne wymiary graniastoslupa prawidlowego szesciokatnego.
   * @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
   * @param centroid Wektor trojwymiarowy reprezentujacy srodek graniastoslupa.
   * @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje graniastoslupa.
   * @param parent Poprzedni wzgledem lokalnego uklad wspolrzednych pelniacy role "rodzica".
   * @param radius Promien okregu opisanego na podstawie graniastoslupa - szesciokacie foremnym.
   * @param height Wysokosc graniastoslupa (w osi Z).
	*/
  hexagonalPrism(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      coordinateSystem *parent,
      double radius,
      double height,
      std::string color = "black")
      : coordinateSystem(centroid, orientation, parent),
        drawingInterface(api, color),
        radius(radius), height(height){};

  ~hexagonalPrism() { erase(); api->redraw(); }

  /**
   * @brief Metoda realizujaca wypisanie opisu graniastostlupa prawidlowego szesciokatnego - jego wymiarow, wspolrzednych srodka i macierzy rotacji.
  */
  void describe(std::ostream &stream);

  // Metody wirtualne drawingInterface

  void draw() override;
  void erase() override { api->erase_shape(id); }
  void rescale(const double &scale) override { centroid = centroid * scale; radius *= scale; height *= scale; }  
};

#endif

#ifndef HILL_HPP
#define HILL_HPP

#include "coordinateSystem.hpp"
#include "drawingInterface.hpp"
#include "landmarkInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie wzgorza.
*/
class hill : public coordinateSystem, public drawingInterface, public landmarkInterface{
  
protected:
  std::vector<vectorMD<3>> vertices; // Wierzcholki podstawy wzgorza.
  double min; // Minimalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
  double max; // Maksymalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
  double height; // Wysokosc wzgledem poziomu zerowego (w osi Z).

public:
  /**
   * @brief Konstruktor inicjalizujacy podstawowe parametry losowo wygenerowanego wzgorza.
   * @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
   * @param centroid Wektor trojwymiarowy reprezentujacy srodek wzgorza.
   * @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje wzgorza.
   * @param min Minimalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
   * @param max Maksymalna wartosc szerokosci i dlugosci wzgorza, jaka moze zostac wygenerowana.
   * @param height Wysokosc wzgledem poziomu zerowego (w osi Z).
   * @param color Kolor wzgorza.
	*/
  hill(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double min,
      double max,
      double height,
      std::string color = "black");

  /**
   * @brief Destruktor umozliwiajacy zmazanie wzgorza wraz z jego usunieciem.
  */
  ~hill() { erase(); api->redraw(); }

  // Metody wirtualne drawingInterface

  void draw() override;
  void erase() override { api->erase_shape(id); }
  void rescale(const double &scale) override {}

  // Metody wirtualne landmarkInterface

  bool is_above(droneInterface *drone) override;
  bool can_land(droneInterface *drone, const double &altitude) override;
};

#endif
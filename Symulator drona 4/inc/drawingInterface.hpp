#ifndef DRAWINGINTERFACE_HPP
#define DRAWINGINTERFACE_HPP

#include <chrono>
#include <thread>

#include "Dr3D_gnuplot_api.hpp"

/**
  @brief Klasa modelujaca interfejs rysowania w programie gnuplot za pomoca dostarczonego api.
*/
class drawingInterface {

protected:
  std::shared_ptr<drawNS::Draw3DAPI> api; // Wskaznik wspoldzielony na obslugiwane api do gnuplota.
  uint id;                                // ID rysowanego obiektu.
  std::string color;                      // Kolor rysowanego obiektu.

public:
  /**
   * @brief Konstruktor inicjalizujacy wskaznik na api do gnuplota i kolor rysowanego obiektu. 
  */
  drawingInterface(std::shared_ptr<drawNS::Draw3DAPI> api, std::string color) : api(api), color(color) {}

  /**
   * @brief Domyslny destruktor wirtualny.
  */
  virtual ~drawingInterface() {}

  /**
   * @brief Metoda wirtualna realizujaca rysowanie obiektu w programie gnuplot za pomoca dostarczonego api.
  */
  virtual void draw() = 0;

  /**
   * @brief Metoda wirtualna realizujaca zmazywanie obiektu w programie gnuplot za pomoca dostarczonego api.
  */
  virtual void erase() = 0;

  /**
   * @brief Metoda wirtualna realizujaca zmiane skali obiektu.
   * @param scale Skala, z jaka zostana powiekszony (scale>1) / pomniejszony (scale<1) dany obiekt.
  */
  virtual void rescale(const double &scale) = 0;

  /**
   * @brief Metoda dostepowa do metody redraw api do gnuplota.
  */
  void refresh() { api->redraw(); }
};

#endif
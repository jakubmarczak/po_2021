#ifndef HEXAGONALDRONE_HPP
#define HEXAGONALDRONE_HPP

#include "coordinateSystem.hpp"
#include "cuboid.hpp"
#include "drawingInterface.hpp"
#include "droneInterface.hpp"
#include "hexagonalPrism.hpp"
#include "landmarkInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie drona szesciokatnego - zlozonego z korpusu i szesciu wirnikow w postaci graniastoslupow prawidlowych szesciokatnych.
*/
class hexagonalDrone : public coordinateSystem, public drawingInterface, public droneInterface, public landmarkInterface {
protected:
  hexagonalPrism body;                      // Graniastoslup prawidlowy szesciokatny reprezentujacy korpus drona.
  std::array<hexagonalPrism, 6> propellers; // Array przechowujacy graniastoslupy prawidlowe szesciokatne reprezentujace wirniki drona.

public:
  /**
   * @brief Konstruktor inicjalizujacy interfejs rysowania, uklad wspolrzednych i poszczegolne wymiary drona.
   * @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
   * @param centroid Wektor trojwymiarowy reprezentujacy srodek drona (srodek korpusu).
   * @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje drona.
   * @param height Wysokosc korpusu drona (w osi Z).
   * @param body_radius Promien okregu wpisanego w graniastoslup prawidlowy szesciokatny reprezentujacy korpus drona.
   * @param propeller_radius Promien okregu wpisanego w graniastoslup prawidlowy szesciokatny reprezentujacy wirnik drona.
   * @param color Kolor drona.
	*/
  hexagonalDrone(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double height,
      double body_radius,
      double propeller_radius,
      std::string color = "black");

  /**
   * @brief Destruktor umozliwiajacy zmazanie drona wraz z jego usunieciem.
  */
  ~hexagonalDrone() { erase(); api->redraw(); }

  // Metody wirtualne droneInterface

  void shortDescription(std::ostream &stream) override;
  void fullDescription(std::ostream &stream) override;
  void move_horizontally(const double &distance) override { translate(orientation * vectorMD<3>({0, distance, 0})); }
  void move_vertically(const double &distance) override { translate(orientation * vectorMD<3>({0, 0, distance})); }
  void tilt(const double &angle) override { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({1, 0, 0}))); }
  void spin(const double &angle) override { rotate(rotationMatrixMD<3>(angle, vectorMD<3>({0, 0, 1}))); }
  void spin_propellers(const double &angle) override;
  void animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed) override;
  
  // Metody wirtualne drawingInterface

  void draw() override;
  void erase() override;
  void rescale(const double &scale) override;

  // Metody wirtualne landmarkInterface

  bool is_above(droneInterface *hexagonalDrone) override;
  bool can_land(droneInterface *hexagonalDrone, const double &altitude) override;
};

#endif

#ifndef CUBOIDALPLATEAU_HPP
#define CUBOIDALPLATEAU_HPP

#include "cuboid.hpp"
#include "landmarkInterface.hpp"

/**
 * @brief Klasa modelujaca pojecie plaskowyzu prostopadlosciennego.
*/
class cuboidalPlateau : public cuboid, public landmarkInterface {

protected:
  double min;     // Minimalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
  double max;     // Maksymalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
  double _height; // Wysokosc wzgledem poziomu zerowego (w osi Z).

public:
  /**
   * @brief Konstruktor inicjalizujacy podstawowe parametry losowo wygenerowanego plaskowyzu prostopadlosciennego.
   * @param api Wskaznik wspoldzielony na obslugiwane api do gnuplota.
   * @param centroid Wektor trojwymiarowy reprezentujacy srodek plaskowyzu prostopadlosciennego.
   * @param orientation Trojwymiarowa macierz rotacji reprezentujaca orientacje plaskowyzu prostopadlosciennego. 
   * @param min Minimalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
   * @param max Maksymalna wartosc szerokosci i dlugosci plaskowyzu, jaka moze zostac wygenerowana.
   * @param height Wysokosc wzgledem poziomu zerowego (w osi Z).
   * @param color Kolor plaskowyzu prostopadlosciennego.
	*/
  cuboidalPlateau(
      std::shared_ptr<drawNS::Draw3DAPI> api,
      vectorMD<3> centroid,
      rotationMatrixMD<3> orientation,
      double min,
      double max,
      double height,
      std::string color = "black");
  
  /**
   * @brief Destruktor umozliwiajacy zmazanie plaskowyzu prostopadlosciennego wraz z jego usunieciem.
  */
  ~cuboidalPlateau() { erase(); api->redraw(); }
  
  // Metody wirtualne landmarkInterface
  
  bool is_above(droneInterface *drone) override;
  bool can_land(droneInterface *drone, const double &altitude) override;
};

#endif
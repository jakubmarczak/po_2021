#include "coordinateSystem.hpp"

void coordinateSystem::translate(const vectorMD<3> &translation) { centroid = centroid + translation; }

void coordinateSystem::rotate(const rotationMatrixMD<3> &rotation) { orientation = orientation * rotation; }

coordinateSystem coordinateSystem::convertToParent() {

  if (parent != nullptr)
    return coordinateSystem((*parent).orientation * centroid + (*parent).centroid, (*parent).orientation * orientation, (*parent).parent);
  else
    return coordinateSystem();
}

coordinateSystem coordinateSystem::convertToGlobal() {

  coordinateSystem result = (*this);

  while (result.parent != nullptr)
    result = convertToParent();
  return result;
}

drawNS::Point3D coordinateSystem::convertToGlobalPoint3D(const vectorMD<3> &arg) {

  vectorMD<3> result = convertToGlobal().orientation * arg + convertToGlobal().centroid;
  return drawNS::Point3D(result[0], result[1], result[2]);
}
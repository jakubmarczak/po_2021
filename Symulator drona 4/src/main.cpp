#include <algorithm>
#include <iostream>
#include <limits>
#include <memory>
#include <ncurses.h> // do menu
#include <string>

#include "Dr3D_gnuplot_api.hpp"
#include "basicDrone.hpp"
#include "cuboidalPlateau.hpp"
#include "droneInterface.hpp"
#include "flatSurface.hpp"
#include "hexagonalDrone.hpp"
#include "hill.hpp"
#include "menu.hpp"
#include "plateau.hpp"
#include "scene.hpp"

using drawNS::APIGnuPlot3D;
using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::numeric_limits;
using std::streamsize;

int main() {

  // definicja menu
  std::vector<std::string> menu_options, menu_border;                   // vectory przechowujace: nazwy pozycji i tekst ramki menu
  attr_t menu_attr = A_REVERSE;                                         // atrybut ncurses przypisywany do zaznaczonej pozycji
  uint8_t menu_choice;                                                  // numer pozycji wybranej przyciskiem enter
  menu_options.push_back("Generate a simple scene");                    // pozycja 1 - wygeneruj prosta scene
  menu_options.push_back("Maneuver a selected drone");                  // pozycja 2 - manewruj wybranym dronem
  menu_options.push_back("Select a drone");                             // pozycja 3 - wybierz drona
  menu_options.push_back("Highlight a selected object");                // pozycja 4 - "podswietl" wybrany obiekt
  menu_options.push_back("Add a drone to the scene");                   // pozycja 5 - dodaj drona do sceny
  menu_options.push_back("Add a landmark to the scene");                // pozycja 6 - dodaj element krajobrazu do sceny
  menu_options.push_back("Remove an object from the scene");            // pozycja 7 - usun obiekt ze sceny
  menu_options.push_back("Remove all objects from the scene");          // pozycja 8 - usun wszystkie obiekty ze sceny
  menu_options.push_back("Show short description of a selected drone"); // pozycja 9 - wypisz krotki opis wybranego drona
  menu_options.push_back("Show full description of a selected drone");  // pozycja 10 - wypisz pelny opis wybranego drona
  menu_options.push_back("Show current 3D vector statistics");          // pozycja 11 - wyswietl obecne statystyki wektorow trojwymiarowych
  menu_options.push_back("Exit");                                       // pozycja 12 - zakoncz
  menu_border.push_back(" Drone simulator ");                           // tytul projektu
  menu_border.push_back(" wrote by Jakub Marczak (259319) ");           // autor
  menu_border.push_back(" controls: arrows / W,A,S,D,Q,ENTER ");        // sterowanie

  // zmienne pomocnicze
  uint16_t id, n;
  uint8_t speed;
  std::string type, color;
  bool selected = false;
  double horizontal_distance, vertical_distance, angle;
  double x, y, z, width, length, height, body_radius, propeller_radius;
  double rotation, min, max;
  vectorMD<3> base({0, 0, 0}); // wektor do statystyk

  try {

    menu menu(menu_options, menu_border, menu_attr); // inicjalizacja menu

    // scena X[-10,10] x Y[-10,10] x Z[0,20] odswiezana za pomoca metody redraw
    std::shared_ptr<drawNS::Draw3DAPI> api(new APIGnuPlot3D(-10, 10, -10, 10, 0, 20, -1));

    scene scene;                        // scena z dronami i elementami krajobrazu
    flatSurface floor(api, 4, 5, 5, 0); // powierzchnia plaska, "podloga"

    while (menu_choice != menu_options.size()) // ostatnia pozycja - zakoncz
    {
      menu_choice = menu.run();

      switch (menu_choice) {
        case 1: { // pozycja 1 - wygeneruj prosta scene

          // dodanie elementow krajobrazu do sceny

          scene.add_landmark(std::shared_ptr<landmarkInterface>(new plateau(api, vectorMD<3>({-5, 5, 0}), rotationMatrixMD<3>(), 3, 6, 3, "black")));
          scene.add_landmark(std::shared_ptr<landmarkInterface>(new hill(api, vectorMD<3>({-5, -5, 0}), rotationMatrixMD<3>(), 3, 6, 4, "black")));
          scene.add_landmark(std::shared_ptr<landmarkInterface>(new cuboidalPlateau(api, vectorMD<3>({5, 0, 1}), rotationMatrixMD<3>(), 2, 4, 2, "black")));

          // dodanie dronow do sceny
          scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({-3, 3, 0}), rotationMatrixMD<3>(), 0.7, 1, 0.4, "red")));
          scene.add_drone(std::shared_ptr<droneInterface>(new basicDrone(api, vectorMD<3>({-3, -3, 0}), rotationMatrixMD<3>(), 1.5, 1.5, 1, 0.5, "orange")));
          scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({3, 3, 0}), rotationMatrixMD<3>(), 0.8, 1.4, 0.5, "green")));
          //scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({3, -3, 0}), rotationMatrixMD<3>(), 0.7, 0.8, 0.3, "blue")));

          /* SPORY BUG
            scene.select_drone() nie dziala przy roznych kombinacjach typow dronow
            np gdy dron 4 to:
            > scene.add_drone(std::shared_ptr<droneInterface>(new basicDrone(api, vectorMD<3>({3, -3, 0}), rotationMatrixMD<3>(), 0.8, 3, 0.5, 0.3, "blue")));
            scene.select_drone(4) wyrzuca segmentation fault (gdb nie pokazuje niczego pozytecznego)
            ale jesli dron 4 zamieni sie na hexagonalDrone, to wszystko dziala poprawnie 
          */

          cout << "@ Successfully generated a simple scene." << endl;

          menu.pressEnter();
          break;
        }
        case 2: { // pozycja 2 - manewruj wybranym dronem

          if (!selected) {
            cout << "# Warning: no drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          cout << "| Enter horizontal distance > ";
          cin >> horizontal_distance;
          cout << "| Enter vertical distance > ";
          cin >> vertical_distance;
          cout << "| Enter rotation angle > ";
          cin >> angle;
          cout << "| Enter speed value > ";
          cin >> speed;

          scene.animate(5, 5, 0, 50);

          cout << "@ Successfully manoeuvred a selected drone." << endl;

          menu.pressEnter();
          break;
        }
        case 3: { // pozycja 3 - wybierz drona

          id = 0;
          n = scene.quantity();

          if (n == 0) {
            cerr << endl
                 << "# Warning: the scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          do {
            cout << "| Enter object ID (1-" << n << ") > ";
            cin >> id;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (id < 1 || id > n);

          if (scene.select_drone(id)) { // wybor drona
            cout << "@ Successfully selected a drone." << endl;
            selected = true;
          } else {
            cout << "# Warning: selected object is not of a type drone." << endl;
            selected = false;
          }

          menu.pressEnter();
          break;
        }
        case 4: { // pozycja 4 - "podswietl" wybrany obiekt

          id = 0;
          n = scene.quantity();

          if (n == 0) {
            cerr << endl
                 << "# Warning: the scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          do {
            cout << "| Enter object ID (1-" << n << ") > ";
            cin >> id;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (id < 1 || id > n);

          scene.highlight(id); // podswietlenie wybranego obiektu
          cout << "@ Successfully highlighted a selected object." << endl;

          menu.pressEnter();
          break;
        }
        case 5: { // pozycja 5 - dodaj drona do sceny

          do {
            cout << "| Enter drone type (basic / hexagonal) > ";
            cin >> type;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (type != "basic" && type != "hexagonal");

          cout << "| Enter centeroid coordinates in subsequent axes" << endl
               << "| X > ";
          cin >> x;
          cout << "| Y > ";
          cin >> y;
          cout << "| Z > ";
          cin >> z;

          // dla drona bazowego
          if (type == "basic") {
            cout << "| Enter body width (X axis) > ";
            cin >> width;
            cout << "| Enter body length (Y axis) > ";
            cin >> length;
          }

          cout << "| Enter body height (Z axis) > ";
          cin >> height;

          // dla drona heksagonalnego
          if (type == "hexagonal") {
            cout << "| Enter body radius (XY plane) > ";
            cin >> body_radius;
          }

          cout << "| Enter propeller radius (XY plane) > ";
          cin >> propeller_radius;

          cout << "| Enter drone color " << endl
               << "| (red/orange/yellow/green/blue/light-blue/purple/white/grey/black)" << endl
               << "| > ";
          cin >> color;

          if (type == "basic") {
            scene.add_drone(std::shared_ptr<droneInterface>(new basicDrone(api, vectorMD<3>({x, y, z}), rotationMatrixMD<3>(), width, length, height, propeller_radius, color)));
            cout << "@ Successfully added a basic drone with ID " << scene.quantity() << " to the scene." << endl;
          } else if (type == "hexagonal") {
            scene.add_drone(std::shared_ptr<droneInterface>(new hexagonalDrone(api, vectorMD<3>({x, y, z}), rotationMatrixMD<3>(), height, body_radius, propeller_radius, color)));
            cout << "@ Successfully added a hexagonal drone with ID " << scene.quantity() << " to the scene." << endl;
          }

          menu.pressEnter();
          break;
        }
        case 6: { // pozycja 6 - dodaj element krajobrazu do sceny

          do {
            cout << "| Enter landmark type (hill/plateau/cuboidalplateau) > ";
            cin >> type;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (type != "hill" && type != "plateau" && type != "cuboidalplateau");

          cout << "| Enter centeroid coordinates in subsequent axes" << endl
               << "| X > ";
          cin >> x;
          cout << "| Y > ";
          cin >> y;
          cout << "| Z > ";
          cin >> z;

          cout << "| Enter rotation angle (in Z axis) > ";
          cin >> rotation;

          cout << "| Enter minimal value of width & length to be generated > ";
          cin >> min;

          cout << "| Enter maximal value of width & length to be generated > ";
          cin >> max;

          cout << "| Enter height > ";
          cin >> height;

          cout << "| Enter landmark color " << endl
               << "| (red/orange/yellow/green/blue/light-blue/purple/white/grey/black)" << endl
               << "| > ";
          cin >> color;

          if (type == "hill") {
            scene.add_landmark(std::shared_ptr<landmarkInterface>(new hill(api, vectorMD<3>({x, y, z}), rotationMatrixMD<3>(rotation, vectorMD<3>({0, 0, 1})), min, max, height, color)));
            cout << "@ Successfully added a hill with ID " << scene.quantity() << " to the scene." << endl;
          } else if (type == "plateau") {
            scene.add_landmark(std::shared_ptr<landmarkInterface>(new plateau(api, vectorMD<3>({x, y, z}), rotationMatrixMD<3>(rotation, vectorMD<3>({0, 0, 1})), min, max, height, color)));
            cout << "@ Successfully added a plateau with ID " << scene.quantity() << " to the scene." << endl;
          } else if (type == "cuboidalplateau") {
            scene.add_landmark(std::shared_ptr<landmarkInterface>(new cuboidalPlateau(api, vectorMD<3>({x, y, z}), rotationMatrixMD<3>(rotation, vectorMD<3>({0, 0, 1})), min, max, height, color)));
            cout << "@ Successfully added a cuboidal plateau with ID " << scene.quantity() << " to the scene." << endl;
          }

          menu.pressEnter();
          break;
        }
        case 7: { // pozycja 7 - usun obiekt ze sceny

          uint16_t id = 0, n = scene.quantity();

          if (n == 0) {
            cerr << endl
                 << "# Warning: the scene is empty." << endl;
            menu.pressEnter();
            break;
          }

          do {
            cout << "| Enter object ID (1-" << n << ") > ";
            cin >> id;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
          } while (id < 1 || id > n);

          scene.remove(id);
          cout << "@ Successfully removed an object from the scene." << endl;

          menu.pressEnter();
          break;
        }
        case 8: { // pozycja 8 - usun wszystkie obiekty ze sceny

          scene.remove_all();
          cout << "@ Successfully removed all objects from the scene." << endl;

          menu.pressEnter();
          break;
        }
        case 9: { // pozycja 9 - wypisz krotki opis wybranego drona

          if (!selected) {
            cout << "# Warning: no drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          scene.describe_drone_short(cout);
          menu.pressEnter();
          break;
        }
        case 10: { // pozycja 10 - wypisz pelny opis wybranego drona

          if (!selected) {
            cout << "# Warning: no drone has been selected." << endl;
            menu.pressEnter();
            break;
          }

          scene.describe_drone_full(cout);
          menu.pressEnter();
          break;
        }
        case 11: { // pozycja 11 - wyswietl obecne statystyki wektorow trojwymiarowych

          cout << "| Number of created vectorMD's: " << base.created() << endl;
          cout << "| Number of existing vectorMD's: " << base.existing() << endl;

          menu.pressEnter();
          break;
        }
      }
    }
  } catch (std::logic_error &error) {

    cout << "# Error: " << error.what() << "." << endl;
  }
}
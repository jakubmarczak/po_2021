#include "plateau.hpp"
#include "rotationMatrixMD.hpp"
#include <random>

plateau::plateau(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double min,
    double max,
    double height,
    std::string color)
    : coordinateSystem(centroid, orientation, nullptr),
      drawingInterface(api, color),
      min(min), max(max), height(height) {
  // do randomizacji
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_int_distribution<int> vertices_distribution(min, max);
  std::uniform_real_distribution<double> radius_distribution(0.5*min, 0.5*max);
  int vertices_number = vertices_distribution(gen); // randomizacja ilosci wierzcholkow
  for (int i = 0; i < vertices_number; ++i)
    vertices.push_back(rotationMatrixMD<3>(360 * i / vertices_number, vectorMD<3>({0, 0, 1})) * vectorMD<3>({1, 1, 0}) * radius_distribution(gen));
  draw();
  api->redraw();
}

void plateau::draw() {

  std::vector<drawNS::Point3D> lower_base, upper_base; // podstawy

  // 1 podstawa
  for (vectorMD<3> i : vertices)
    lower_base.push_back(convertToGlobalPoint3D(i));

  // 2 podstawa
  for (vectorMD<3> i : vertices)
    upper_base.push_back(convertToGlobalPoint3D(i + vectorMD<3>({0, 0, height})));

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{lower_base, upper_base}, color);
}

bool plateau::is_above(droneInterface *basicDrone) { 
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}

bool plateau::can_land(droneInterface *basicDrone, const double &altitude) { 
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}
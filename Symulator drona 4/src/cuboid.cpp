#include "cuboid.hpp"

void cuboid::describe(std::ostream &stream) {

  stream << "| Color: " << color << std::endl
         << "|" << std::endl
         << "| Dimensions:" << std::endl
         << ": Width: " << width << std::endl
         << ": Length: " << length << std::endl
         << ": Height: " << height << std::endl
         << "|" << std::endl
         << "| Current position:" << std::endl
         << centroid;

  stream << "|" << std::endl
         << "| Current orientation:" << std::endl
         << orientation;
}

void cuboid::draw() {

  std::vector<drawNS::Point3D> base1, base2; // podstawy

  // 1 podstawa
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, -0.5 * length, -0.5 * height})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, -0.5 * length, 0.5 * height})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, -0.5 * length, 0.5 * height})));
  base1.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, -0.5 * length, -0.5 * height})));

  // 2 podstawa
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, 0.5 * length, -0.5 * height})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({0.5 * width, 0.5 * length, 0.5 * height})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, 0.5 * length, 0.5 * height})));
  base2.push_back(convertToGlobalPoint3D(vectorMD<3>({-0.5 * width, 0.5 * length, -0.5 * height})));

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base1, base2}, color);
}

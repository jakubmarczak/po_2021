#include <chrono>
#include <cmath>
#include <thread>

#include "Dr3D_gnuplot_api.hpp"
#include "basicDrone.hpp"

basicDrone::basicDrone(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double width,
    double length,
    double height,
    double radius,
    std::string color)
    : coordinateSystem(centroid, orientation, nullptr),
      drawingInterface(api, color),
      body(api, vectorMD<3>({0, 0, 0.5 * height}), rotationMatrixMD<3>(), this, width, length, height, color),
      propellers{
          hexagonalPrism(api, vectorMD<3>({0.5 * width, -0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
          hexagonalPrism(api, vectorMD<3>({0.5 * width, 0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
          hexagonalPrism(api, vectorMD<3>({-0.5 * width, 0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color),
          hexagonalPrism(api, vectorMD<3>({-0.5 * width, -0.5 * length, height + 0.2 * radius}), rotationMatrixMD<3>(), this, radius, 0.4 * radius, color)} {
  draw();
  api->redraw();
}

void basicDrone::shortDescription(std::ostream &stream) {
  stream << "@ SHORT DRONE DESCRIPTION" << std::endl
         << "|" << std::endl
         << "| Type: basicDrone" << std::endl
         << "| Color: " << color << std::endl
         << "|" << std::endl
         << "| Current position:" << std::endl
         << centroid;
}

void basicDrone::fullDescription(std::ostream &stream) {
  stream << "@ FULL DRONE DESCRIPTION" << std::endl
         << "|" << std::endl
         << "| Type: basicDrone" << std::endl
         << "| Color: " << color << std::endl
         << "|" << std::endl
         << "| Current position:" << std::endl
         << centroid
         << "|" << std::endl
         << "> Body's description" << std::endl
         << "|" << std::endl
         << "| Type: cuboidal" << std::endl;

  body.describe(stream);

  stream << "|" << std::endl
         << "> Propellers' description" << std::endl
         << "|" << std::endl
         << "| Type: 4 x hexagonal" << std::endl
         << "|" << std::endl;

  propellers[0].describe(stream);
}

void basicDrone::spin_propellers(const double &angle) {
  rotationMatrixMD<3> right(angle, vectorMD<3>({0, 0, 1}));
  rotationMatrixMD<3> left(angle, vectorMD<3>({0, 0, -1}));

  propellers[0].rotate(right); // obrot w prawo
  propellers[1].rotate(left);  // obrot w lewo
  propellers[3].rotate(right);
  propellers[4].rotate(left);
}

void basicDrone::animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed) {
  int8_t direction = 0; // kierunek przesuniecia poziomego (+/-)

  if (horizontal_distance != 0)
    direction = std::copysign(1, horizontal_distance);

  // normalizacja predkosci
  if (speed > 100)
    speed = 100;

  // zmazywanie "domyslnego" drona (z konstruktora)
  erase();

  for (uint8_t i = 0; i < 100 - speed; ++i) {

    // przesuniecia drona w pionie i poziomie
    move_vertically(vertical_distance / (100 - speed));
    move_horizontally(horizontal_distance / (100 - speed));

    // rotacja drona i wirnikow wokol osi Z
    spin(angle / (100 - speed));
    spin_propellers(360 / (100 - speed));

    // przechylenie drona
    if (i <= 0.5 * (100 - speed))
      tilt(-direction * speed / (i + 4)); // do przodu
    else
      tilt(direction * speed / (i + 4)); // do tylu

    // rysowanie drona i odswiezenie sceny
    draw();
    api->redraw();

    // poprawka dla kolejnych przesuniec
    if (i <= 0.5 * (100 - speed))
      tilt(direction * speed / (i + 4));
    else
      tilt(-direction * speed / (i + 4));

    // zmazywanie drona (pojedynczej "klatki")
    erase();

    // przestoj 50ms przed kolejna klatka
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }

  // rysowanie "domyslnego" drona
  draw();
}

void basicDrone::draw() {
  body.draw();
  for (hexagonalPrism &i : propellers)
    i.draw();
}

void basicDrone::erase() {
  body.erase();
  for (hexagonalPrism &i : propellers)
    i.erase();
}

void basicDrone::rescale(const double &scale) {
  body.rescale(scale);
  for (hexagonalPrism &i : propellers)
    i.rescale(scale);
}

bool basicDrone::is_above(droneInterface *basicDrone) {
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}

bool basicDrone::can_land(droneInterface *basicDrone, const double &altitude) {
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}
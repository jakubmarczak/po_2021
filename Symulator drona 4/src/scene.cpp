#include <algorithm>

#include "scene.hpp"

void scene::add_drone(std::shared_ptr<droneInterface> drone) {

  drones.push_back(drone);
  drawables.push_back(std::dynamic_pointer_cast<drawingInterface>(drone));
}

void scene::add_landmark(std::shared_ptr<landmarkInterface> landmark) {

  landmarks.push_back(landmark);
  drawables.push_back(std::dynamic_pointer_cast<drawingInterface>(landmark));
}

void scene::remove(const uint8_t &id) {

  if (id > drawables.size() || drawables.empty())
    throw std::logic_error("failed access to a non-existing object of a scene");

  // definicje wskaznikow wspoldzielonych
  std::shared_ptr<drawingInterface> drawables_ptr = drawables[id - 1];
  std::shared_ptr<droneInterface> drones_ptr = std::dynamic_pointer_cast<droneInterface>(drawables_ptr);
  std::shared_ptr<landmarkInterface> landmarks_ptr = std::dynamic_pointer_cast<landmarkInterface>(drawables_ptr);

  // usuwanie dronow
  if (drones_ptr != nullptr)
    drones.erase(std::remove(drones.begin(), drones.end(), drones_ptr), drones.end());

  // usuwanie elementow krajobrazu
  if (landmarks_ptr != nullptr)
    landmarks.erase(std::remove(landmarks.begin(), landmarks.end(), landmarks_ptr), landmarks.end());

  // usuwanie elementow rysowalnych
  drawables.erase(std::remove(drawables.begin(), drawables.end(), drawables_ptr), drawables.end());
}

void scene::remove_all() {

  uint8_t n = drawables.size(); // zapamietanie pierwotnej ilosci obiektow sceny

  for (uint8_t i = 0; i < n; ++i)
    remove(1);
}

void scene::highlight(const uint8_t &id) {

  // (mruganie)
  for (uint8_t i = 0; i < 3; ++i) {

    drawables[id - 1]->erase();
    drawables[id - 1]->refresh();

    std::this_thread::sleep_for(std::chrono::milliseconds(250));

    drawables[id - 1]->draw();
    drawables[id - 1]->refresh();

    std::this_thread::sleep_for(std::chrono::milliseconds(250));
  }
}

bool scene::select_drone(const uint8_t &id) {

  if (id > drawables.size() || drawables.empty())
    throw std::logic_error("failed access to a non-existing object of a scene");

  selected_drone = drones[std::distance(drones.begin(), find(drones.begin(), drones.end(), std::dynamic_pointer_cast<droneInterface>(drawables[id - 1])))];

  if (selected_drone == nullptr) {
    return false;
  } else
    return true;
}

void scene::describe_drone_short(std::ostream &stream) {

  if (selected_drone != nullptr)
    selected_drone->shortDescription(stream);
  else
    std::cout << "# Warning: no drone has been selected before." << std::endl;
}

void scene::describe_drone_full(std::ostream &stream) {

  if (selected_drone != nullptr)
    selected_drone->shortDescription(stream);
  else
    std::cout << "# Warning: no drone has been selected before." << std::endl;
}

void scene::check_collision(std::shared_ptr<droneInterface> drone) {
}

void scene::animate(const double &horizontal_distance, const double &vertical_distance, const double &angle, uint8_t speed) {

  if (selected_drone != nullptr)
    selected_drone->animate(horizontal_distance, vertical_distance, angle, speed);
  else
    std::cout << "# Warning: no drone has been selected before." << std::endl;
}

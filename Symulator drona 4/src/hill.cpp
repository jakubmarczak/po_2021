#include "hill.hpp"

#include <random>

hill::hill(
    std::shared_ptr<drawNS::Draw3DAPI> api,
    vectorMD<3> centroid,
    rotationMatrixMD<3> orientation,
    double min,
    double max,
    double height,
    std::string color)
    : coordinateSystem(centroid, orientation, nullptr),
      drawingInterface(api, color),
      min(min), max(max), height(height) {
  // do randomizacji
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_int_distribution<int> vertices_distribution(min, max);
  std::uniform_real_distribution<double> radius_distribution(0.5*min, 0.5*max);
  int vertices_number = vertices_distribution(gen); // randomizacja ilosci wierzcholkow
  for (int i = 0; i < vertices_number; ++i)
    vertices.push_back(rotationMatrixMD<3>(360 * i / vertices_number, vectorMD<3>({0, 0, 1})) * vectorMD<3>({1, 1, 0}) * radius_distribution(gen));
  draw();
  api->redraw();
}

void hill::draw() {

  std::vector<drawNS::Point3D> base, summit; // podstawa i gorny wierzcholek

  for (vectorMD<3> i : vertices) {
    base.push_back(convertToGlobalPoint3D(i));
    summit.push_back(convertToGlobalPoint3D(vectorMD<3>({0, 0, height})));
  }

  id = api->draw_polyhedron(std::vector<std::vector<drawNS::Point3D>>{base, summit}, color);
}

bool hill::is_above(droneInterface *basicDrone) { 
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}

bool hill::can_land(droneInterface *basicDrone, const double &altitude) { 
  // nie wiem jak z poziomu droneInterface dostac sie do drona
  // return () ? true : false;
  return false;
}
#include <iomanip>
#include <iostream>
#include <ncurses.h>
#include <string>
#include <vector>

#include "menu.hpp"

void menu::init() {

  initscr();                                     // inicjacja ncurses
  noecho();                                      // wylaczenie echo z getch
  curs_set(0);                                   // usuniecie kursora
  getmaxyx(stdscr, height, width);               // pobranie wymiarow terminala
  border = newwin(height, width, 0, 0);          // dodanie okna o wymiarach terminala zawierajace ramke
  content = newwin(height - 4, width - 4, 2, 3); // dodanie okna w wiekszym oknie z ramka zawierajace wlasciwe menu
  keypad(content, true);                         // wlaczenie obslugi strzalek
  box(border, 0, 0);                             // utworzenie ramki (0, 0 - domyslne znaki w pionie i poziomie)

  // dodanie tytulu okna
  if (borderText.size() > 0)
    mvwprintw(border, 0, 2, borderText[0].c_str());
  
  // dodanie autora
  if (borderText.size() > 1)
    mvwprintw(border, 0, width - borderText[1].length() - 1, borderText[1].c_str());
  
  // dodanie sterowania
  if (borderText.size() > 2)
    mvwprintw(border, height - 1, 2, borderText[2].c_str());

  wrefresh(border); // odswiezenie okna z ramka (inaczej sie nie pojawia)
}

void menu::printOptions(attr_t attr) {

  werase(content); // usuniecie zawartosci okna

  for (uint8_t i = 0; i < options.size(); i++) {
    if (i == selected - 1)
      // (wattron, wattroff - window attribute on/off)
      wattron(content, attr); // przypisanie atrybutu (np. A_REVERSE - zamiana koloru tla i czcionki)
    // wyswietlanie kolejnych pozycji menu z wektora (od 0) z pionowym odstepem 1
    // c_str() - konwersja ciagu znakow zapisanego w zmiennej string na ciag, ktory moze byc zapisany w tablicy znakow
    mvwprintw(content, 2 * i, 0, options[i].c_str());
    wattroff(content, attr);
  }
  wrefresh(content); // odswiezenie zawartosci okna
}

void menu::scrollUp() {

  selected--;
  if (selected < 1)
    selected = options.size();
}

void menu::scrollDown() {

  selected++;
  if (selected > options.size())
    selected = 1;
}

void menu::pressEnter() {

  std::string text = "> Press ENTER to continue ...";

  std::cout << std::endl
            << " " << std::setfill('-') << std::setw(text.length() + 2) << "" << std::endl
            << "| " << text << " |" << std::endl
            << " " << std::setfill('-') << std::setw(text.length() + 2) << "" << std::endl
            << std::setfill(' ');

  wgetch(content);
}

uint8_t menu::run() {

  init(); // inicjacja menu

  // poruszanie sie po menu
  while (1) {
    printOptions(attr);           // wypisanie pozycji menu
    keyPressed = wgetch(content); // rejestracja wcisnietego klawisza
    switch (keyPressed) {
      // przewijanie do gory
      case KEY_UP: case KEY_LEFT: case 'A': case 'a': case 'W': case 'w':
        scrollUp(); break;

      // przewijanie w dol
      case KEY_DOWN: case KEY_RIGHT: case 'D': case 'd': case 'S': case 's':
        scrollDown(); break;

      // wywolanie funkcji danej pozycji przyciskiem enter
      case 10: 
        endwin(); return selected; break;

      // "zakoncz" przyciskiem 'q'
      case 'Q': case 'q': 
        endwin(); return options.size(); break;

      default: 
        break;
    }
  }

  return 0;
}

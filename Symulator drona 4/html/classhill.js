var classhill =
[
    [ "hill", "classhill.html#a1fd4f63a53376bbda90fc40ef1d89fed", null ],
    [ "~hill", "classhill.html#aac157baa162b12f28900ff88ed6e04c2", null ],
    [ "can_land", "classhill.html#af474cfc6f29fb2e0b9c0e2a367be1e7f", null ],
    [ "draw", "classhill.html#a951d8569d1e85787c726899077761b3b", null ],
    [ "erase", "classhill.html#aaff34060f661febbeb761e981e014c58", null ],
    [ "is_above", "classhill.html#ae2d81b032ab57550fa45289b43df53ad", null ],
    [ "rescale", "classhill.html#ad240131dc679ca706c4f4ed319bafc9d", null ],
    [ "height", "classhill.html#a31b4a11babb4c276ac5c5bbbc4d2d200", null ],
    [ "max", "classhill.html#a668c434f141ce58b3eed97f83f8fc22a", null ],
    [ "min", "classhill.html#a6efb2a9c3781f4a431cf82ad7f95024c", null ],
    [ "vertices", "classhill.html#adae2f182bff33bfa7cf5a69d628abefd", null ]
];
var classbasic_drone =
[
    [ "basicDrone", "classbasic_drone.html#a0fa6975f54111bc1c166104fc597e491", null ],
    [ "~basicDrone", "classbasic_drone.html#a61e69a7b0183b3a4ab70de8bc7e703fe", null ],
    [ "animate", "classbasic_drone.html#a1494182d1f2e07173b5acf8723724b6a", null ],
    [ "can_land", "classbasic_drone.html#a773ea942c38dd4602cac59d6fabda734", null ],
    [ "draw", "classbasic_drone.html#a5f26619a4600f5943ec8db0264266c96", null ],
    [ "erase", "classbasic_drone.html#a8ab53f910ac83b6f8d819b6f07593a81", null ],
    [ "fullDescription", "classbasic_drone.html#aef736d8ed0adf71ca909e0b1b0211a05", null ],
    [ "is_above", "classbasic_drone.html#a0b9ac8f66b4b885b2db631e2aad354be", null ],
    [ "move_horizontally", "classbasic_drone.html#a916b84f3c01ad7fb75b8cccf11f6b28a", null ],
    [ "move_vertically", "classbasic_drone.html#ab79b2b59bef37290233d4344b0f28c7f", null ],
    [ "rescale", "classbasic_drone.html#ad36b09e138ae21ce82cf37299b503a6f", null ],
    [ "shortDescription", "classbasic_drone.html#a5c604b869554d88b90cdd4be952d8f9e", null ],
    [ "spin", "classbasic_drone.html#a771d97049917d3e4635256ebc04d0d64", null ],
    [ "spin_propellers", "classbasic_drone.html#a784b4b3b5c5e681ca72ef499b5d3dacf", null ],
    [ "tilt", "classbasic_drone.html#a6da3613910fbdc9b6c71a380e98f4d7c", null ],
    [ "body", "classbasic_drone.html#aa61d3c5bdf9f1ce359789626821df45a", null ],
    [ "propellers", "classbasic_drone.html#a2a4177dcaa15d73b574b0593b9d1191d", null ]
];
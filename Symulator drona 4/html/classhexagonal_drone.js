var classhexagonal_drone =
[
    [ "hexagonalDrone", "classhexagonal_drone.html#a1ce5621ab1e2d0a1301e0f8cb23c452a", null ],
    [ "~hexagonalDrone", "classhexagonal_drone.html#a631c827ba56bc84c09b07865c26b21ff", null ],
    [ "animate", "classhexagonal_drone.html#abd2efdea6bc146494079fa47b01a578b", null ],
    [ "can_land", "classhexagonal_drone.html#a001682bc37ce94a6df3d4e5d4e845b51", null ],
    [ "draw", "classhexagonal_drone.html#a500ef2f882295b27bb40fe835d617fe9", null ],
    [ "erase", "classhexagonal_drone.html#a6a199ce976c3a05be2308e2715934d97", null ],
    [ "fullDescription", "classhexagonal_drone.html#a11b5fb38c4a185ea7c75224b55bed2c4", null ],
    [ "is_above", "classhexagonal_drone.html#a4befaafaddebec3ceff8aad836efed49", null ],
    [ "move_horizontally", "classhexagonal_drone.html#ae9103b6fb372c2d28093198a84a08479", null ],
    [ "move_vertically", "classhexagonal_drone.html#a67b67e47d83b4e90851721c83d8ac5e9", null ],
    [ "rescale", "classhexagonal_drone.html#a3e8eba4c6355c5fe9d2ae56de1693f6e", null ],
    [ "shortDescription", "classhexagonal_drone.html#a3abf158459fa0b66c3dfa7c81278d6d1", null ],
    [ "spin", "classhexagonal_drone.html#a16d3eb162e1955d4726c169f43772bdb", null ],
    [ "spin_propellers", "classhexagonal_drone.html#aa39a617f97b53aa9bf6f44135ee003c0", null ],
    [ "tilt", "classhexagonal_drone.html#a8fdb066edac8e51cd254818db95655ab", null ],
    [ "body", "classhexagonal_drone.html#a6e42a4820b333123f4ce5f3807a80f7c", null ],
    [ "propellers", "classhexagonal_drone.html#a2f1f7c531b62b6180a87ebfd4c2930d9", null ]
];
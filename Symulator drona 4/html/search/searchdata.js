var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstvw~",
  1: "abcdfhlmprsv",
  2: "d",
  3: "bcdfhlmprsv",
  4: "abcdefhilmnopqrstv~",
  5: "_abcdghiklmoprstvw",
  6: "s",
  7: "_"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines"
};

var indexSectionLabels =
{
  0: "Wszystko",
  1: "Klasy",
  2: "Przestrzenie nazw",
  3: "Pliki",
  4: "Funkcje",
  5: "Zmienne",
  6: "Definicje typów",
  7: "Definicje"
};


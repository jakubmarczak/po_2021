var searchData=
[
  ['tilt_297',['tilt',['../classbasic_drone.html#a6da3613910fbdc9b6c71a380e98f4d7c',1,'basicDrone::tilt()'],['../classdrone_interface.html#a36cbe4f668f16a57954723b13601a6b9',1,'droneInterface::tilt()'],['../classhexagonal_drone.html#a8fdb066edac8e51cd254818db95655ab',1,'hexagonalDrone::tilt()']]],
  ['translate_298',['translate',['../classcoordinate_system.html#aa4630b9ee4e8166ed15f997ce7140221',1,'coordinateSystem']]],
  ['translate_5fcolor_299',['translate_color',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#ad9c71531a2f5c76ecdb65fa913962961',1,'drawNS::APIGnuPlot3D']]],
  ['transpose_300',['transpose',['../classrotation_matrix_m_d.html#acf8c133af2869c223207fc957959f8d7',1,'rotationMatrixMD']]]
];

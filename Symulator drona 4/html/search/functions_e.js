var searchData=
[
  ['redraw_279',['redraw',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#a27a110521a511f0c75e5c867f247a3f6',1,'drawNS::APIGnuPlot3D::redraw()'],['../classdraw_n_s_1_1_draw3_d_a_p_i.html#ae88121104d2eeb8936f8dc1b68fc3bbf',1,'drawNS::Draw3DAPI::redraw()']]],
  ['refresh_280',['refresh',['../classdrawing_interface.html#ad097f2ca9cfcf8bceb5489e75e3f754e',1,'drawingInterface']]],
  ['remove_281',['remove',['../classscene.html#ad38d462842fc17100d25eaeb4118dedc',1,'scene']]],
  ['remove_5fall_282',['remove_all',['../classscene.html#afcf69fec77046da38be82ac06d7ba02e',1,'scene']]],
  ['replot_5floop_283',['replot_loop',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#afa364d7d8a0a74d51b680a9560e1f1ed',1,'drawNS::APIGnuPlot3D']]],
  ['rescale_284',['rescale',['../classbasic_drone.html#ad36b09e138ae21ce82cf37299b503a6f',1,'basicDrone::rescale()'],['../classcuboid.html#a5501edb2e9606e678482c9bb6bb2945e',1,'cuboid::rescale()'],['../classdrawing_interface.html#ac65466a05e2cf655be8dad2b28204cd8',1,'drawingInterface::rescale()'],['../classflat_surface.html#aa1ecf0c171d740150d3eebc284212dd3',1,'flatSurface::rescale()'],['../classhexagonal_drone.html#a3e8eba4c6355c5fe9d2ae56de1693f6e',1,'hexagonalDrone::rescale()'],['../classhexagonal_prism.html#a9389cc6a3728ce078ebf86436777c919',1,'hexagonalPrism::rescale()'],['../classhill.html#ad240131dc679ca706c4f4ed319bafc9d',1,'hill::rescale()'],['../classplateau.html#a8c371f03882c0e96272f1558deb7b934',1,'plateau::rescale()']]],
  ['rotate_285',['rotate',['../classcoordinate_system.html#aaa81628f4c536c5478f6032993ab1087',1,'coordinateSystem']]],
  ['rotationmatrixmd_286',['rotationMatrixMD',['../classrotation_matrix_m_d.html#ae6542a39add90c162c0f03a155009b1a',1,'rotationMatrixMD::rotationMatrixMD()'],['../classrotation_matrix_m_d.html#add5eb8450790910c8c24da50cb675e18',1,'rotationMatrixMD::rotationMatrixMD(double angle, vectorMD&lt; dimension &gt; axis)']]],
  ['run_287',['run',['../classmenu.html#a8d1ce798dd304e75c24b3c48ac76d2f4',1,'menu']]]
];

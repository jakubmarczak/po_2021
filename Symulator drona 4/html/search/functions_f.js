var searchData=
[
  ['scene_288',['scene',['../classscene.html#a2e694bbcb89d7e17749120c8002b1285',1,'scene']]],
  ['scrolldown_289',['scrollDown',['../classmenu.html#a7482f9275fece5fb816ea2cbb4ea58b3',1,'menu']]],
  ['scrollup_290',['scrollUp',['../classmenu.html#a72fe7dda1094f64526dd3763e405aa4e',1,'menu']]],
  ['select_5fdrone_291',['select_drone',['../classscene.html#ab710cf92679b6bb9e12fa3665d845404',1,'scene']]],
  ['send2gnuplot_292',['send2gnuplot',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#a6a8a5e074e337ed9be9cfb45b6738b89',1,'drawNS::APIGnuPlot3D']]],
  ['shortdescription_293',['shortDescription',['../classbasic_drone.html#a5c604b869554d88b90cdd4be952d8f9e',1,'basicDrone::shortDescription()'],['../classdrone_interface.html#ace12402c3592000e8e8ef5f776506aba',1,'droneInterface::shortDescription()'],['../classhexagonal_drone.html#a3abf158459fa0b66c3dfa7c81278d6d1',1,'hexagonalDrone::shortDescription()']]],
  ['spin_294',['spin',['../classbasic_drone.html#a771d97049917d3e4635256ebc04d0d64',1,'basicDrone::spin()'],['../classdrone_interface.html#a06b32ee5127bb712130c95cb8b4174c9',1,'droneInterface::spin()'],['../classhexagonal_drone.html#a16d3eb162e1955d4726c169f43772bdb',1,'hexagonalDrone::spin()']]],
  ['spin_5fpropellers_295',['spin_propellers',['../classbasic_drone.html#a784b4b3b5c5e681ca72ef499b5d3dacf',1,'basicDrone::spin_propellers()'],['../classdrone_interface.html#a2a16eda2e52c8ec067fe4174a8e1703e',1,'droneInterface::spin_propellers()'],['../classhexagonal_drone.html#aa39a617f97b53aa9bf6f44135ee003c0',1,'hexagonalDrone::spin_propellers()']]],
  ['stop_5fdrawing_5fproces_296',['stop_drawing_proces',['../classdraw_n_s_1_1_a_p_i_gnu_plot3_d.html#af9507e24c3fd5bad9e75b00f30cc9f80',1,'drawNS::APIGnuPlot3D']]]
];

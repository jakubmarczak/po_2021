var classrotation_matrix_m_d =
[
    [ "rotationMatrixMD", "classrotation_matrix_m_d.html#ae6542a39add90c162c0f03a155009b1a", null ],
    [ "rotationMatrixMD", "classrotation_matrix_m_d.html#add5eb8450790910c8c24da50cb675e18", null ],
    [ "operator*", "classrotation_matrix_m_d.html#af09350c89e78cf88e851940729bd9cfd", null ],
    [ "operator*", "classrotation_matrix_m_d.html#ac88d66689bb80e247f2d1e40ac57568e", null ],
    [ "operator[]", "classrotation_matrix_m_d.html#a882023edbe95a8a2b067dbe0ccafa223", null ],
    [ "transpose", "classrotation_matrix_m_d.html#acf8c133af2869c223207fc957959f8d7", null ],
    [ "rows", "classrotation_matrix_m_d.html#a39709c3014a49d5039225561f000ac8b", null ]
];
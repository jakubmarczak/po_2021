var classplateau =
[
    [ "plateau", "classplateau.html#ab058bb6f8558a76a16a855d5d2809a23", null ],
    [ "~plateau", "classplateau.html#a336a9f14762c3821cf717cca91f6ca86", null ],
    [ "can_land", "classplateau.html#a576ba0a3af12bd1f4e1dfd7ec2e70dc9", null ],
    [ "draw", "classplateau.html#a5b3802dd049b465fb10775d909076454", null ],
    [ "erase", "classplateau.html#a74bab67ef98b7c1094f2fa4f2c0bde1e", null ],
    [ "is_above", "classplateau.html#aabfd8955760185d298b337baef90718c", null ],
    [ "rescale", "classplateau.html#a8c371f03882c0e96272f1558deb7b934", null ],
    [ "height", "classplateau.html#a0170696a3be931ab673a0f0954832203", null ],
    [ "max", "classplateau.html#a87a794f86bcbf95997a5545816c7a7d0", null ],
    [ "min", "classplateau.html#ad5cd15b6cffe5a7f24d3ff8dff921658", null ],
    [ "vertices", "classplateau.html#a8a0a8731d00d35c3b881ba7b0494abbe", null ]
];
var classdrone_interface =
[
    [ "~droneInterface", "classdrone_interface.html#a469aeece561141165f9ed02f82cda60e", null ],
    [ "animate", "classdrone_interface.html#adf17b408aef35cc95fb96c25782baeba", null ],
    [ "fullDescription", "classdrone_interface.html#ae4ae938a9f3fec08c5a651297e3f23fa", null ],
    [ "move_horizontally", "classdrone_interface.html#a1bc77a459850b983cfc7cd7774c387c4", null ],
    [ "move_vertically", "classdrone_interface.html#ad9d057c58f70c5043801ca7d00eb9b28", null ],
    [ "shortDescription", "classdrone_interface.html#ace12402c3592000e8e8ef5f776506aba", null ],
    [ "spin", "classdrone_interface.html#a06b32ee5127bb712130c95cb8b4174c9", null ],
    [ "spin_propellers", "classdrone_interface.html#a2a16eda2e52c8ec067fe4174a8e1703e", null ],
    [ "tilt", "classdrone_interface.html#a36cbe4f668f16a57954723b13601a6b9", null ]
];
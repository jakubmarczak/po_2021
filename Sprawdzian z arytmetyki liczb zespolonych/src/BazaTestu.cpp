#include <iostream>
#include <fstream>
#include <limits>
#include "BazaTestu.hpp"

database_t::database_t(std::string test_name)
{
  std::ifstream file; // utworzenie strumieniu plikowego dla bazy testu
  complex_expression_t expression;
  uint16_t n=1; // zmienna do numerowania pytan

  // np. latwy.dat
  test_name += ".dat";
  file.open(test_name);

  // blad otwarcia pliku
  if (file.fail())
    throw std::logic_error("Blad: Otwarcie pliku '" + test_name + "' z baza pytan nie powiodlo sie.");
  
  // pusty plik (w sensie braku pytan)
  file >> std::ws;
  if (file.eof())
    throw std::logic_error("Blad: Plik '" + test_name + "' jest pusty.");

  // czytanie do konca pliku
  // pominieto wymieniony w instrukcji do zadania znak kropki z braku pomyslu - kropka jest bowiem takze separatorem dziesietnym 
  while (!file.eof())
  {
    try 
    {
      // wczytanie wyrazenia zespolonego z pliku
      file >> expression;
      // zaladowanie wyrazenia zespolonego do wektora z zestawem pytan
      question_set.push_back(expression);
    }
    // obsluga bledow zwiazanych z zapisem pytan
    catch (std::logic_error &error)
    { 
      if (!file.eof())
        std::cerr << "Uwaga: Pominieto pytanie " << n << " (" << error.what() << ")." << std::endl;
      file.clear();
      file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    n++;
  }

  file.close();
}

complex_expression_t database_t::operator[](uint8_t i)
{
  return question_set[i];
}

uint8_t database_t::size()
{
  return question_set.size();
}
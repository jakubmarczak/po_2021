#include <cmath>
#include <limits>
#include "LZespolona.hpp"

// Przeciazenia operatorow arytmetycznych dla liczb zespolonych

complex_t complex_t::operator+(const complex_t &complex) const
{
	// zwraca konstruktor (nowa LZ jako wynik dodawania)
	return complex_t(re + complex.re, im + complex.im);
}

complex_t complex_t::operator-(const complex_t &complex) const
{
	// zwraca konstruktor (nowa LZ jako wynik odejmowania)
	return complex_t(re - complex.re, im - complex.im);
}

complex_t complex_t::operator*(const complex_t &complex) const
{
	// zwraca konstruktor (nowa LZ jako wynik iloczynu)
	return complex_t((re * complex.re - im * complex.im), (re * complex.im + im * complex.re));
}

complex_t complex_t::operator/(const complex_t &complex) const
{
	double m = complex.modulus();
	// blad w przypadku proby dzielenia przez zero
	if (m == 0)
		throw std::logic_error("operacja dzielenia przez zero nie posiada sensu liczbowego");
	// zwraca LZ (jako wynik ilorazu LZ-LZ)
	// *this - dereferencja wskaznika na adres complex_t
	return ((*this * complex.conjugate()) / pow(m, 2));
}

complex_t complex_t::operator/(const double &real) const
{
	// blad w przypadku proby dzielenia przez zero
	if (real == 0)
		throw std::logic_error("operacja dzielenia przez zero nie posiada sensu liczbowego");
	// zwraca konstruktor (nowa LZ jako wynik ilorazu LZ-LR)
	return complex_t(re / real, im / real);
}

// Przeciazenia operatorow porownania dla liczb zespolonych

bool complex_t::operator==(const complex_t &complex) const
{
	return ((fabs(re - complex.re) < std::numeric_limits<double>::epsilon()) && (fabs(im - complex.im) < std::numeric_limits<double>::epsilon()));
}

bool complex_t::operator!=(const complex_t &complex) const
{
	// *this - dereferencja wskaznika na adres complex_t
	return !(*this == complex);
}

// Funkcje pomocnicze - sprzezenie i modul liczby zespolonej

complex_t complex_t::conjugate() const
{
	// zwraca konstruktor (nowa LZ jako wynik sprzezenia)
	return complex_t(re, -im);
}

double complex_t::modulus() const
{
	return sqrt(pow(re, 2) + pow(im, 2));
}

// Przeciazenia operatorow przesuniec bitowych dla liczb zespolonych (obsluga skroconej notacji)

std::istream &operator>>(std::istream &stream, complex_t &complex)
{
	char input, op1, op2; // zmienna pomocnicza, 1 i 2 operator (+ lub -)
	double arg1, arg2;		// czesc rzeczywista i urojona liczby zespolonej

	// ( check
	stream >> std::ws >> input;
	if (input != '(')
		throw std::logic_error("brak nawiasu otwierajacego");

	stream >> std::ws >> input;

	// sprawdzenie czy 1 znak to + lub -
	if (input == '+' || input == '-')
	{
		op1 = input;
		stream >> std::ws >> input;
	}

	// (+-i)
	if (input == 'i')
	{
		// ) check
		stream >> std::ws >> input;
		if (input != ')')
			throw std::logic_error("brak nawiasu zamykajacego");
		complex.set_re(0);
		complex.set_im(1);
		if (op1 == '-')
			complex.set_im(-1);
		return stream;
	}
	// (+-a...
	else if (isdigit(input))
	{
		// char -> double
		stream.putback(input);
		stream >> std::ws >> arg1;
		stream >> std::ws >> input;
		// (+-a)
		if (input == ')')
		{
			complex.set_im(0);
			complex.set_re(arg1);
			if (op1 == '-')
				complex.set_re(-arg1);
			return stream;
		}
		// (+-ai)
		else if (input == 'i')
		{
			// ) check
			stream >> std::ws >> input;
			if (input != ')')
				throw std::logic_error("brak nawiasu zamykajacego");
			complex.set_re(0);
			// (+-ai)
			complex.set_im(arg1);
			if (op1 == '-')
				complex.set_im(-arg1);
			return stream;
		}
		// (+-a+-...
		else if (input == '+' || input == '-')
		{
			op2 = input;
			stream >> std::ws >> input;
			// (+-a+-i)
			if (input == 'i')
			{
				// ) check
				stream >> std::ws >> input;
				if (input != ')')
					throw std::logic_error("brak nawiasu zamykajacego");
				// (+-a...
				complex.set_re(arg1);
				if (op1 == '-')
					complex.set_re(-arg1);
				// (+-a+-i)
				complex.set_im(1);
				if (op2 == '-')
					complex.set_im(-1);
				return stream;
			}
			// (+-a+-bi)
			else if (isdigit(input))
			{
				// char -> double
				stream.putback(input);
				stream >> std::ws >> arg2;
				// i check
				stream >> std::ws >> input;
				if (input != 'i')
					throw std::logic_error("nie podano jednostki urojonej 'i'");
				// ) check
				stream >> std::ws >> input;
				if (input != ')')
					throw std::logic_error("brak nawiasu zamykajacego");
				// (+-a...
				complex.set_re(arg1);
				if (op1 == '-')
					complex.set_re(-arg1);
				// (+-a+-bi)
				complex.set_im(arg2);
				if (op2 == '-')
					complex.set_im(-arg2);
				return stream;
			}
			else 
				throw std::logic_error("blad zapisu liczby zespolonej");
		}
		else
			throw std::logic_error("blad zapisu liczby zespolonej");
	}
	else
		throw std::logic_error("blad zapisu liczby zespolonej");
	return stream;
}

std::ostream &operator<<(std::ostream &stream, const complex_t &complex)
{
	// zawsze wymagane sa nawiasy otwierajacy i zamykajacy
	stream << '(';
	// (0+0i) -> 0
	if (complex.get_re() == 0 && complex.get_im() == 0)
		stream << 0;
	// brak urojonej -> wypisz tylko rzeczywista
	else if (complex.get_im() == 0)
		stream << complex.get_re();
	else
	{
		// jest rzeczywista -> wypisz ja
		if (complex.get_re() != 0)
		{
			stream << complex.get_re();
			// usuwanie 1 z 1i
			if (complex.get_im() == 1)
				stream << '+';
			// usuwanie 1 z -1i
			else if (complex.get_im() == -1)
				stream << '-';
			// zwykly zapis typu (3-2i)
			else
				stream << std::showpos << complex.get_im() << std::noshowpos;
		}
		// sama urojona
		else
		{
			// usuwanie 1 z -1i
			if (complex.get_im() == -1)
				stream << '-';
			// usuwanie 1 z 1i
			else if (complex.get_im() != 1)
				stream << complex.get_im();
		}
		stream << 'i';
	}
	stream << ')';
	return stream;
}
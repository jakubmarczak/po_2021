#include <iomanip>
#include "Statystyka.hpp"

void stats_t::counter(bool answer, stats_t &stats)
{
	if (answer == true)
		stats.correct++; // poprawna odpowiedz++ (dla false posrednio dodajemy bledna odpowiedz)
	stats.all++;			 // wszystkie odpowiedzi++
}

std::ostream &operator<<(std::ostream &stream, const stats_t &stats)
{
	float score; // procentowy udzial poprawnych odpowiedzi (wynik)
	std::string k = "Koniec testu", wp = "Wynik pozytywny!", wn = "Wynik negatywny!";
	std::string w = "wynik", p = "poprawne", b = "bledne", r = "razem";
	uint8_t width = w.length() + p.length() + b.length() + r.length() + 9; // dlugosc 'tabeli'

	if (stats.get_all() == 0) // wszystkie pytania w pliku sa niepoprawne lub nie ma pytan
		score = 0;
	else
		score = static_cast<float>(stats.get_correct() * 100) / static_cast<float>(stats.get_all());

	// 1 linia
	stream << std::setw(width) << std::setfill('=') << ' ' << std::setfill(' ') << std::endl;

	// koniec testu !
	stream << std::setw(static_cast<int>((width - k.length()) / 2)) << ' ' << k << std::endl;

	// 2 linia
	stream << std::setw(width) << std::setfill('-') << ' ' << std::setfill(' ') << std::endl;

	// nazwy wynikow
	stream << std::setw(w.length() + 1) << w
				 << std::setw(p.length() + 2) << p
				 << std::setw(b.length() + 2) << b
				 << std::setw(r.length() + 2) << r << std::endl;

	// wynik, ilosc odpowiedzi: poprawnych, blednych, wszystkich
	stream << std::right << std::setw(w.length()) << std::setprecision(3) << score << "%"
				 << std::right << std::setw(p.length() + 2) << +stats.get_correct() // '+' do wyswietlania wartosci numerycznej (uint8_t = unsigned char)
				 << std::right << std::setw(b.length() + 2) << stats.get_all() - stats.get_correct()
				 << std::right << std::setw(r.length() + 2) << +stats.get_all() << std::endl;
	
	// 3 linia
	stream << std::setw(width) << std::setfill('-') << ' ' << std::setfill(' ') << std::endl;

	// wynik pozytywny
	if (score > 50)
		stream << std::setw(static_cast<int>((width - wp.length()) / 2)) << ' ' << wp << std::endl;

	// wynik negatywny
	else 
		stream << std::setw(static_cast<int>((width - wn.length()) / 2)) << ' ' << wn << std::endl;

	// 4 linia
	stream << std::setw(width) << std::setfill('=') << ' ' << std::setfill(' ') << std::endl << std::endl;

	return stream;
}

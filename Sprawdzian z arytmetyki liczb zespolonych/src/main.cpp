#include <iostream>
#include <limits>
#include "BazaTestu.hpp"
#include "Statystyka.hpp"

using namespace std;

int main(int argc, char **argv)
{ 
  complex_t answer, solved; // odpowiedz, prawidlowa odpowiedz
  stats_t stats;            // statystyki testu
  uint8_t attempts = 1;     // liczba prob (max 3)

  try
  {
    // w try, poniewaz kontruktor moze wysylac bledy
    database_t database(argv[1]); // wczytanie wyrazen zespolonych z pliku o nazwie argv[1].dat do wektora

    // sprawdzenie, czy podano rodzaj testu (1 argument wywolania programu)
    if (argc < 2)
      throw std::runtime_error("Blad: nie podano opcji okreslajacej rodzaj testu. Dopuszczalne opcje: latwy, trudny.");

    cout << endl;

    for (uint8_t i = 0; i < database.size(); i++) // size() odpowiada liczbie pytan
    {
      try
      {
        // pierwsze podejscie/proba
        solved = database[i].solve(); // oblicz i-te wyrazenie z bazy
        if (attempts == 1)
          cout << "| Podaj wynik wyrazenia: " << database[i] << endl
               << "|>";
        cin >> answer;
        cin.ignore(numeric_limits<streamsize>::max(), '\n'); // ignorowanie znakow po nawiasie zamykajacym

        // prawidlowa odpowiedz
        if (answer == solved)
        {
          cout << "| Prawidlowa odpowiedz!" << endl
               << endl;
          attempts = 1;               // reset prob (maksymalnie sa 3)
          stats.counter(true, stats); // zwiekszenie liczby poprawnych i wszystkich odpowiedzi
        }
        // zla odpowiedz
        else
        {
          cout << "| Zle! Prawidlowa odpowiedz: " << solved << endl
               << endl;
          attempts = 1;                // reset prob (maksymalnie sa 3)
          stats.counter(false, stats); // zwiekszenie liczby wszystkich odpowiedzi (posrednio tez niepoprawnych)
        }
      }
      // obsluga bledow zwiazanych z zapisem odpowiedzi
      catch (std::logic_error &logic)
      {
        // pozostala co najmniej 1 proba
        if (attempts < 4)
        {
          cerr << "| Blad zapisu. (pozostalo podejsc: " << 4 - attempts << ")" << endl
               << "|>";
          attempts++;
          i--; // zapobieganie wczytywaniu nastepnego pytania
        }
        // brak pozostalych prob - zaliczenie odpowiedzi jako blednej
        else
        {
          cerr << "| Blad zapisu! Prawidlowa odpowiedz: " << solved << endl
               << endl;
          attempts = 1;
          stats.counter(false, stats);
        }

        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
      }
    }
    // wyswietlenie statystyk
    cout << stats;
  }

  // obsluga bledow zwiazanych z wywolaniem programu
  catch (std::runtime_error &runtime)
  {
    cerr << runtime.what();
    return 1;
  }

  // obsluga pozostalych bledow
  catch (std::logic_error &logic)
  {
    cerr << logic.what();
    return 1;
  }

  return 0;
}

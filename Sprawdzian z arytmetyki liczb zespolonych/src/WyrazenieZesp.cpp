#include "WyrazenieZesp.hpp"

complex_t complex_expression_t::solve()
{
	complex_t complex;
	switch (op)
	{
	// dodawanie
	case operation::add:
		complex = arg1 + arg2;
		break;
	// odejmowanie
	case operation::subtract:
		complex = arg1 - arg2;
		break;
	// mnozenie
	case operation::multiply:
		complex = arg1 * arg2;
		break;
	// dzielenie
	case operation::divide:
		complex = arg1 / arg2;
		break;
	}
	return complex;
}

std::istream &operator>>(std::istream &stream, complex_expression_t &expression)
{
	// wczytywanie kolejno: 1 argument, operator, 2 argument wyrazenia zespolonego
	stream >> std::ws >> expression.arg1; // 1 argument
	stream >> std::ws >> expression.op;	 // operator
	stream >> std::ws >> expression.arg2; // 2 argument
	return stream;
}

std::ostream &operator<<(std::ostream &stream, const complex_expression_t &expression)
{
	// wypisywanie kolejno: 1 argument, operator, 2 argument wyrazenia zespolonego
	stream << expression.get_arg1() << expression.get_op() << expression.get_arg2();
	return stream;
}

std::istream &operator>>(std::istream &stream, complex_expression_t::operation &op)
{
	char Op;
	stream >> Op;

	switch (Op)
	{
	case '+':
		op = complex_expression_t::operation::add;
		break;
	case '-':
		op = complex_expression_t::operation::subtract;
		break;
	case '*':
		op = complex_expression_t::operation::multiply;
		break;
	case '/':
		op = complex_expression_t::operation::divide;
		break;
	default:
		throw std::logic_error("niewlasciwy operator arytmetyczny");
		break;
	}
	return stream;
}

std::ostream &operator<<(std::ostream &stream, const complex_expression_t::operation &op)
{
	stream << (char)op;
	return stream;
}

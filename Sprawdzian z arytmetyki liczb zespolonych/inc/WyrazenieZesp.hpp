#ifndef WYRAZENIEZESP_HPP
#define WYRAZENIEZESP_HPP

#include "LZespolona.hpp"

/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
class complex_expression_t
{
  /**
    @brief Wyliczenie modelujace zbior operatorow arytmetycznych.    
  */
  enum operation
  {
    add = '+',      // znak dodawania +
    subtract = '-', // znak odejmowania  -
    multiply = '*', // znak mnozenia *
    divide = '/'    // znak dzielenia /
  };

  operation op;   // operator wyrazenia zespolonego
  complex_t arg1; // pierwszy argument wyrazenia zespolonego
  complex_t arg2; // drugi argument wyrazenia zespolonego

public:
  /**
		@brief Funkcja realizujaca obliczenia dla wyrazen zespolonych. 
		@return Wynik wyrazenia zespolonego (liczba zespolona).
	*/
  complex_t solve();

  /**
		@brief Funkcja dostepowa pozwalajaca na odczyt pierwszego argumentu wyrazenia zespolonego.
		@return Pierwszy argument wyrazenia zespolonego.
	*/
  complex_t get_arg1() const { return arg1; }

  /**
		@brief Funkcja dostepowa pozwalajaca na odczyt drugiego argumentu wyrazenia zespolonego.
		@return Drugi argument wyrazenia zespolonego.
	*/
  complex_t get_arg2() const { return arg2; }

  /**
		@brief Funkcja dostepowa pozwalajaca na odczyt operatora wyrazenia zespolonego.
		@return Operator wyrazenia zespolonego.
	*/
  operation get_op() const { return op; }

  // dostep do complex_expression_t::arg1, complex_expression_t::op i complex_expression_t::arg2 (w celu modyfikacji arg1, op, arg2)
  friend std::istream &operator>>(std::istream &stream, complex_expression_t &expression);
  // dostep do complex_expression_t::op i complex_expression_t::operation (w celu modyfikacji op)
  friend std::istream &operator>>(std::istream &stream, complex_expression_t::operation &op);
  // dostep do complex_expression_t::op (w celu wypisania op)
  friend std::ostream &operator<<(std::ostream &stream, const complex_expression_t::operation &op);
};

/**
	@brief  Operator realizujacy przesuniecie bitowe w prawo (wczytywanie) dla wyrazenia zespolonego.
	@param  stream Obslugiwany strumien.
	@param  expression Wyrazenie zespolone, ktore zostanie wczytane ze strumienia.
  @return Obslugiwany strumien.
*/
std::istream &operator>>(std::istream &stream, complex_expression_t &expression);

/**
	@brief  Operator realizujacy przesuniecie bitowe w lewo (wypisywanie) dla wyrazenia zespolonego.
	@param  stream Obslugiwany strumien.
	@param  expression Wyrazenie zespolone, ktore zostanie wypisane na strumieniu.
	@return Obslugiwany strumien.
*/
std::ostream &operator<<(std::ostream &strumien, const complex_expression_t &expression);

/**
	@brief  Operator realizujacy przesuniecie bitowe w prawo (wczytywanie) dla operatora wyrazenia zespolonego.
	@param  stream Obslugiwany strumien.
	@param  operation Operator arytmetyczny wyrazenia zespolonego, ktory zostanie wczytany ze strumienia.
	@return Obslugiwany strumien.
*/
std::istream &operator>>(std::istream &strumien, complex_expression_t::operation &op);

/**
	@brief  Operator realizujacy przesuniecie bitowe w lewo (wypisywanie) dla operatora wyrazenia zespolonego.
	@param  stream Obslugiwany strumien.
	@param  operation Operator arytmetyczny wyrazenia zespolonego, ktory zostanie wypisany na strumieniu.
	@return Obslugiwany strumien.
*/
std::ostream &operator<<(std::ostream &strumien, const complex_expression_t::operation &op);

#endif

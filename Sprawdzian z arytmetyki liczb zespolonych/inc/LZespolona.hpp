#ifndef LZESPOLONA_HPP
#define LZESPOLONA_HPP

#include <iostream>

/**
  @brief Klasa modelujaca pojecie liczby zespolonej.
*/
class complex_t
{
	double re; // Pole reprezentujace czesc rzeczywista.
	double im; // Pole reprezentujace czesc urojona.

public:
	/**
		@brief Konstruktor przypisujacy czesci rzeczywistej i urojonej wartosc 0 (domyslny).
		@return Nowa liczba zespolona (0,0).
	*/
	complex_t() : re{0}, im{0} {}

	/**
		@brief Konstruktor przypisujacy czesci rzeczywistej i urojonej wartosci przekazane jako argumenty.
		@param RE wartosc czesci rzeczywistej.
		@param IM wartosc czesci urojonej.
		@return Nowa liczba zespolona (RE,IM).
	*/
	complex_t(double RE, double IM) : re{RE}, im{IM} {}

	/**
		@brief Funkcja dostepowa pozwalajaca na odczyt wartosci czesci rzeczywistej liczby zespolonej.
		@return Czesc rzeczywista liczby zespolonej.
	*/
	double get_re() const { return re; }

	/**
		@brief Funkcja dostepowa pozwalajaca na odczyt wartosci czesci urojonej liczby zespolonej.
		@return Czesc urojona liczby zespolonej.
	*/
	double get_im() const { return im; }

	/**
		@brief Funkcja pozwalajaca na modyfikacje wartosci czesci rzeczywistej liczby zespolonej.
	*/
	void set_re(double RE) { re = RE; }

	/**
		@brief Funkcja pozwalajaca na modyfikacje wartosci czesci urojonej liczby zespolonej.
	*/
	void set_im(double IM) { im = IM; }

	/**
    @brief Operator realizujacy dodawanie dwoch liczb zespolonych.
    @param complex liczba zespolona, ktora chcemy dodac.
    @return Suma dwoch liczb zespolonych.
	*/
	complex_t operator+(const complex_t &complex) const;

	/**
		@brief Operator realizujacy odejmowanie dwoch liczb zespolonych.
		@param complex liczba zespolona, ktora chcemy odjac.
		@return Roznica dwoch liczb zespolonych.
	*/
	complex_t operator-(const complex_t &complex) const;

	/**
		@brief Operator realizujacy iloczyn dwoch liczb zespolonych.
		@param complex liczba zespolona, ktora chcemy pomnozyc.
		@return Iloczyn dwoch liczb zespolonych.
	*/
	complex_t operator*(const complex_t &complex) const;

	/**
		@brief Operator realizujacy iloraz dwoch liczb zespolonych.
		@param complex liczba zespolona, przez ktora chcemy dzielic.
		@return Iloraz dwoch liczb zespolonych.
	*/
	complex_t operator/(const complex_t &complex) const;

	/**
		@brief Operator realizujacy iloraz liczby zespolonej i rzeczywistej.
		@param real liczba rzeczywista, przez ktora chcemy dzielic.
		@return Iloraz liczby zespolonej i rzeczywistej.
	*/
	complex_t operator/(const double &real) const;

	/**
		@brief Operator realizujacy porownanie ze soba dwoch liczb zespolonych.
		@param complex liczba zespolona, ktora chcemy porownac.
		@return Wartosc 'true', jesli liczby sa sobie rowne i 'false', jesli sa sobie rozne.
	*/
	bool operator==(const complex_t &complex) const;

	/**
		@brief Operator realizujacy negacje porownania ze soba dwoch liczb zespolonych.
		@param complex liczba zespolona, dla ktorej chcemy zrealizowac negacje porownania.
		@return Wartosc 'true', jesli liczby sa sobie rozne i 'false', jesli sa sobie rowne.
	*/
	bool operator!=(const complex_t &complex) const;

	/**
	@brief Funkcja realizujaca sprzezenie liczby zespolonej.
	@return Sprzezenie liczby zespolonej.
	*/
	complex_t conjugate() const;

	/**
		@brief Funkcja realizujaca modul (rowny "dlugosci") liczby zespolonej.
		@return Modul liczby zespolonej.
	*/
	double modulus() const;
};

/**
	@brief Operator realizujacy przesuniecie bitowe w prawo (wczytywanie) dla liczby zespolonej.
	@param stream obslugiwany strumien.
	@param complex liczba zespolona, ktora zostanie wczytana ze strumienia.
	@return Obslugiwany strumien.
*/
std::istream &operator>>(std::istream &stream, complex_t &complex);

/**
	@brief Operator realizujacy przesuniecie bitowe w lewo (wypisywanie) dla liczby zespolonej.
	@param stream obslugiwany strumien.
	@param complex liczba zespolona, ktora zostanie wypisana na strumieniu.
	@return Obslugiwany strumien.
*/
std::ostream &operator<<(std::ostream &stream, const complex_t &complex);

#endif

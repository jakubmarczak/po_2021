#ifndef STATYSTYKA_HPP
#define STATYSTYKA_HPP

#include <iostream>

/**
  @brief Klasa odpowiedzialna za konstrukcje statystyk testu.
*/
class stats_t
{
	uint8_t correct; // ilosc poprawnych odpowiedzi (bledne = wszystkie - poprawne)
	uint8_t all;			// ilosc wszystkich odpowiedzi

public:
	/**
	  @brief Konstruktor realizujacy inicjacje liczby odpowiedzi, w tym poprawnych, zerami (domyslny).
	*/
	stats_t() : correct{0}, all{0} {};

	/**
		@brief Funkcja dostepowa pozwalajaca na odczyt liczby poprawnych odpowiedzi.
		@return Ilosc poprawnych odpowiedzi.
	*/
	uint8_t get_correct() const { return correct; }

	/**
		@brief Funkcja dostepowa pozwalajaca na odczyt liczby wszystkich odpowiedzi.
		@return Ilosc poprawnych odpowiedzi.
	*/
	uint8_t get_all() const { return all; }

	/**
	  @brief Funkcja realizujaca zliczanie odpowiedzi, w tym bezposrednio poprawnych i posrednio blednych.
	  @param answer Odpowiedz poprawna lub bledna - true lub false.
	  @param stats Klasa statystyk.
	*/
	void counter(bool answer, stats_t &stats);
};

/**
	@brief Operator realizujacy wypisanie statystyk.
	@param stream Obslugiwany strumien.
	@param stats Klasa statystyk.
	@return Obslugiwany strumien.
*/
std::ostream &operator<<(std::ostream &stream, const stats_t &stats);

#endif

#ifndef BAZATESTU_HPP
#define BAZATESTU_HPP

#include <vector>
#include "WyrazenieZesp.hpp"

/**
  @brief Klasa modelujaca pojecie bazy testu.
*/
class database_t
{
	/**
	  @brief Wektor przechowujacy pytania (wyrazenia zespolone) wczytane z pliku. 
	*/
	std::vector<complex_expression_t> question_set;

public:
	/**
		@brief Konstruktor realizujacy wczytywanie wyrazen zespolonych z pliku do wektora.
		@param test_name Nazwa pliku, z ktorego wczytywane sa wyrazenia zespolone.
	*/
	database_t(std::string test_name);

	/**
		@brief Operator realizujacy zwracanie wyrazen zespolonych umieszczonych w wektorze.
		@param i Numer wyrazenia zespolonego, ktore chcemy zwrocic.
		@return Wyrazenie zespolone o danym numerze.
	*/
	complex_expression_t operator[](uint8_t i);

	/**
		@brief Funkcja zwracajaca ilosc pytan w wektorze.
		@return Ilosc pytan w wektorze.
	*/
	uint8_t size();
};

#endif
